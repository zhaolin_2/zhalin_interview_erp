package com.ruoyi.common.utils;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

/**
 * 操作信息记录类
 * 负责库存 销售 采购 对操作：增删改 内容的记录操作
 *
 * @Author Lin
 * @Date 2023 05 15 10
 **/
public class HandleMessageRecordUtil {

    /**
     *
     * @param handleUid
     * @param father 父类
     * @param son 子类
     * @param handleClass 业务类别
     * @return
     */
    public int addHandleMessage(String handleClass,Long handleUid,Object father,Object son) {
        /**
         * 共有三个模块需要处理：库存 销售订单 采购订单
         * 库存添加
         * 销售订单添加
         * 采购订单添加
         */

        /**
         *库存部分：
         *  库存产品本体添加
         *  1.库存产品重订货规则添加
         *  2.库存产品供应商报价添加
         *  3.库存产品物料清单组件添加
         */

        /**
         * 销售订单部分
         * 1.销售订单本体添加
         * 2.销售订单组件添加
         */

        /**
         * 销售订单部分
         * 1.采购订单本体添加
         * 2.采购订单组件添加
         */


        return 0;
    }


    /**
     * 删除：
     */
    public int deleteHandleMessage() {
        return 0;
    }


    /**
     * 修改：
     */
    public int updateHandleMessage() {
        return 0;
    }
}
