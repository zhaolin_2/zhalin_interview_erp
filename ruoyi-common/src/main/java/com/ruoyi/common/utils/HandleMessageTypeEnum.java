package com.ruoyi.common.utils;

/**
 * @Author Lin
 * @Date 2023 05 15 11
 **/
public enum HandleMessageTypeEnum {
    INSERT,DELETE,UPDATE
}
