package com.ruoyi.web.controller.system.purchase;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.system.domain.purchase.PurchaseOrderMessage;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.StockProductTemplate;
import com.ruoyi.system.domain.stock.StockTemplateMessage;
import com.ruoyi.system.service.IPurchaseOrderMessageService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.service.IPurchaseOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购Controller
 * @author Lin
 * @date 2023-04-06
 */
@RestController
@RequestMapping("/system/purchaseOrder")
public class PurchaseOrderController extends BaseController
{
    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Autowired
    private IPurchaseOrderMessageService purchaseOrderMessageService;

    /**
     * 查询采购列表
     */
    @GetMapping("/list")
    public TableDataInfo list(PurchaseOrder purchaseOrder)
    {
        startPage();
        List<PurchaseOrder> list = purchaseOrderService.selectPurchaseOrderList(purchaseOrder);
        return getDataTable(list);
    }

    /**
     * 导出采购列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, PurchaseOrder purchaseOrder)
    {
        List<PurchaseOrder> list = purchaseOrderService.selectPurchaseOrderList(purchaseOrder);
        ExcelUtil<PurchaseOrder> util = new ExcelUtil<PurchaseOrder>(PurchaseOrder.class);
        util.exportExcel(response, list, "采购数据");
    }

    /**
     * 获取采购详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        PurchaseOrder purchaseOrder = purchaseOrderService.selectPurchaseOrderById(id);
        return success(purchaseOrderService.selectPurchaseOrderById(id));
    }

    /**
     * 新增采购
     */
    @PostMapping
    public AjaxResult add(@RequestBody PurchaseOrder purchaseOrder)
    {
        return toAjax(purchaseOrderService.insertPurchaseOrder(purchaseOrder));
    }

    /**
     * 插入订单行以及编辑订单销售订单
     */
    @PostMapping("/purchaseOrder_write")
    public AjaxResult writeIn(@RequestBody PurchaseOrder purchaseOrder) throws IllegalAccessException {
        return AjaxResult.success(purchaseOrderService.writePurchaseOrder(purchaseOrder));
    }

    /**
     * 修改采购
     */
    @PutMapping
    public AjaxResult edit(@RequestBody PurchaseOrder purchaseOrder)
    {
        return toAjax(purchaseOrderService.updatePurchaseOrder(purchaseOrder));
    }

    /**
     * 删除采购
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(purchaseOrderService.deletePurchaseOrderByIds(ids));
    }


    /**
     * 采购订单详情：采购订单组件部分
     */
    @Anonymous
    @GetMapping("/partMsg/{orderId}")
    public AjaxResult orderPartMsg(@PathVariable Long orderId){
        List<PurchaseOrderPart> purcharOrderParts = purchaseOrderService.GetOrderPartMsg(orderId);
        return AjaxResult.success(purcharOrderParts);
    }

    /**
     * 根据订单id 查询采购订单操作信息
     * @param purchaseOrderId
     * @return
     */
    @Anonymous
    @GetMapping("/orderMsg_get/{purchaseOrderId}")
    public AjaxResult PurchaseOrderMessageGet(@PathVariable("purchaseOrderId") Long purchaseOrderId) {
        List<PurchaseOrderMessage> purchaseOrderMessages = purchaseOrderMessageService.getOrderMessageByOrderId(purchaseOrderId);
        return AjaxResult.success(purchaseOrderMessages);
    }

    /**
     * 方法说明：
     * 传入库存产品id 返回对应的待编辑采购订单子组件
     * @param StockId
     * @return
     */
    @GetMapping("/purchaseOrderPart_get/{stockId}")
    @Anonymous
    public AjaxResult GetPurchaseOrderPartInfoByStockId(@PathVariable("stockId")Long StockId){
        List list = new ArrayList();
        list.add(purchaseOrderService.getPurchaseOrderPartFromStockProductTemplateId(StockId));
        return AjaxResult.success(list);
    }

    /**
     * 采购订单详情：供应商信息返回
     */
    @Anonymous
    @GetMapping("/partnerMsg/{purchaseOrderId}")
    public AjaxResult PartnerMsgGet(@PathVariable Long purchaseOrderId){
        List list = purchaseOrderService.getOrderPartnerMsg(purchaseOrderId);
        return AjaxResult.success(list);
    }



    /**
     * 销售订单-编辑中：销售订单的 订单组件 更新
     */
    @PutMapping("/purchaseOrderPart_update")
    public AjaxResult purchaseOrderPartEdit(@RequestBody PurchaseOrderPart purchaseOrderPart) {
        return AjaxResult.success(purchaseOrderService.purchaseOrderPartUpdate(purchaseOrderPart));
    }



    /**
     * 采购订单组件删除
     * @param sequenceId
     * @return
     */
    @GetMapping("/purchaseOrderPart_delete/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult PurchaseOrderPartBySequenceId(@PathVariable("sequenceId") Long sequenceId,@PathVariable("isInsert") String isInsert){
        return AjaxResult.success(purchaseOrderService.purchaseOrderPartDelete(sequenceId,isInsert));
    }

    /**
     * 用户选定的供应商 ：可选择的供应商列表
     */
    @GetMapping("/purchaseOrderPart_stocks/{inInsert}/{name}")
    @Anonymous
    public AjaxResult getStockListAvailable(@PathVariable("inInsert") String isInsert,@PathVariable("name") String name){
        System.out.println(name+"**********************************");
        List<StockProductTemplate> stockProductTemplates = purchaseOrderService.selectPurchaseOrderStockListAvailable(isInsert,name);
        return AjaxResult.success(stockProductTemplates);
    }


    /**
     * 销售报价单：放弃编辑
     * @param
     * @return
     */
    @GetMapping("/orderUpdate_abandon")
    @Anonymous
    public AjaxResult PurchaseOrderUpdateAbandon(){
        return AjaxResult.success(purchaseOrderService.purchaseOrderPartUpdateAbandon());
    }

    /**
     * 订单组件修改-订单组件修改前的信息获取
     * @param sequenceId 修改订单行的序列id
     * @param isInsert 编辑时修改/写入后修改
     * @return
     */
    @GetMapping("/purchaseOrderPart_info/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult GetPurchaseOrderPartInfoBySequenceId(@PathVariable("sequenceId") Long sequenceId,@PathVariable("isInsert") String isInsert){
        return AjaxResult.success(purchaseOrderService.purchaseOrderPartInfo(sequenceId,isInsert));
    }

    /**
     * 询价单 确认为采购订单
     * @param id
     * @return
     */
    @GetMapping("/orderConfirm/{id}")
    @Anonymous
    public AjaxResult DraftPurchaseOrderConfirmToPurchase(@PathVariable("id") Long id){
        purchaseOrderService.draftPurchaseOrderConfirmToPurchaseOrder(id);
        return AjaxResult.success("确认成功");
    }

    /**
     * 询价单取消
     * @param id
     * @return
     */
    @GetMapping("/orderCancel/{id}")
    @Anonymous
    public AjaxResult PurchaseOrderCancel(@PathVariable("id") Long id){
        purchaseOrderService.purchaseOrderCancel(id);
        return AjaxResult.success("订单已废弃");
    }


}
