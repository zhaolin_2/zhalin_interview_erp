package com.ruoyi.web.controller.system.sale;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.system.domain.sale.SaleOrderMessage;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.service.IPurchaseOrderPartService;
import com.ruoyi.system.service.ISaleOrderMessageService;
import com.ruoyi.system.service.ISaleOrderPartService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.service.ISaleOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 销售订单Controller
 *
 * @author Lin
 * @date 2023-04-04
 */
@RestController
@RequestMapping("/system/saleOrder")
public class SaleOrderController extends BaseController {

    @Autowired
    private ISaleOrderService saleOrderService;

    @Autowired
    private ISaleOrderMessageService saleOrderMessageService;

    @Autowired
    private ISaleOrderPartService saleOrderPartService;

    @Autowired
    private IPurchaseOrderPartService purchaseOrderPartService;

    /**
     * 查询销售订单列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SaleOrder saleOrder) {
        startPage();
        List<SaleOrder> list = saleOrderService.selectSaleOrderList(saleOrder);
        return getDataTable(list);
    }

    /**
     * 导出销售订单列表
     */
    @Log(title = "销售订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SaleOrder saleOrder) {
        List<SaleOrder> list = saleOrderService.selectSaleOrderList(saleOrder);
        ExcelUtil<SaleOrder> util = new ExcelUtil<SaleOrder>(SaleOrder.class);
        util.exportExcel(response, list, "销售订单数据");
    }

    /**
     * 获取销售订单详细信息
     */
    @GetMapping(value = "/{id}")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(saleOrderService.selectSaleOrderById(id));
    }

    /**
     * 插入订单行以及编辑订单销售订单
     */
    @PostMapping("/saleOrder_write")
    public AjaxResult writeIn(@RequestBody SaleOrder saleOrder) throws IllegalAccessException {
        return AjaxResult.success(saleOrderService.writeSaleOrder(saleOrder));
    }

    /**
     * 新增销售订单
     */
    @Log(title = "销售订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SaleOrder saleOrder) {
        return toAjax(saleOrderService.insertSaleOrder(saleOrder));
    }

    /**
     * 修改销售订单
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SaleOrder saleOrder) {
        return toAjax(saleOrderService.updateSaleOrder(saleOrder));
    }

    /**
     * 删除销售订单
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("@ss.hasPermi('admin')")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(saleOrderService.deleteSaleOrderByIds(ids));
    }

    /**
     * 销售订单详情：销售订单组件部分
     */
    @Anonymous
    @GetMapping("/partMsg/{orderId}")
    public AjaxResult orderPartMsg(@PathVariable Long orderId) {
        List<SaleOrderPart> saleOrderParts = saleOrderService.GetOrderPartMsg(orderId);
        return AjaxResult.success(saleOrderParts);
    }

    /**
     * 库存信息返回
     */
    @Anonymous
    @GetMapping("/orderMsg_get/{saleOrderId}")
    public AjaxResult PurchaseOrderMessageGet(@PathVariable("saleOrderId") Long saleOrderId) {
        List<SaleOrderMessage> saleOrderMessages = saleOrderMessageService.getSaleOrderMessageByOrderId(saleOrderId);
        return AjaxResult.success(saleOrderMessages);
    }

    /**
     * 测试
     */
    @Anonymous
    @GetMapping("/test")
    public AjaxResult SaleOrderTest() {
        SaleOrder saleOrder = saleOrderService.selectSaleOrderById(26l);
        List test = new ArrayList();
        SaleOrderPart saleOrderPart1 = saleOrderPartService.selectSaleOrderPartById(60l);
        test.add(saleOrderPart1);
        test.add(saleOrderPart1);
        saleOrder.setSaleOrderParts(test);
        List test2 = new ArrayList();
        test2.add(saleOrder);
        return AjaxResult.success(test2);
    }


    /**
     * 方法说明：方法用于销售报价单编辑中，插入订单组件（销售的子组件）时使用
     * 该订单需要参数：用户选择的库存产品的id,传入后端后，后端处理后，作为一个订单组件对象返回前端，回填入表格内
     * 用户进行编辑后，则请求销售订单writeIn 接口，将销售订单组件 写入订单内
     *
     * @param StockId
     * @return
     */
    @GetMapping("/saleOrderPart_get/{stockId}")
    @Anonymous
    public AjaxResult GetSaleOrderPartInfoByStockId(@PathVariable("stockId") Long StockId) {
        List list = new ArrayList();
        list.add(saleOrderService.getSaleOrderPartFromStockProductTemplateId(StockId));
        return AjaxResult.success(list);
    }

    /**
     * 订单组件删除
     *
     * @param sequenceId
     * @return
     */
    @GetMapping("/saleOrderPart_delete/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult SaleOrderPartBySequenceId(@PathVariable("sequenceId") Long sequenceId, @PathVariable("isInsert") String isInsert) {
        return AjaxResult.success(saleOrderService.saleOrderPartDelete(sequenceId, isInsert));
    }

    /**
     * 销售订单-编辑中：销售订单的 订单组件 信息
     */
    @PutMapping("/saleOrderPart_update")
    public AjaxResult saleOrderPartEdit(@RequestBody SaleOrderPart saleOrderPart) {
        return AjaxResult.success(saleOrderService.saleOrderPartUpdate(saleOrderPart));
    }

    /**
     * 订单组件修改-订单组件修改前的信息获取
     *
     * @param sequenceId 修改订单行的序列id
     * @param isInsert   编辑时修改/写入后修改
     * @return
     */
    @GetMapping("/saleOrderPart_info/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult GetSaleOrderPartInfoBySequenceId(@PathVariable("sequenceId") Long sequenceId, @PathVariable("isInsert") String isInsert) {
        return AjaxResult.success(saleOrderService.saleOrderPartInfo(sequenceId, isInsert));
    }


    /**
     * 销售报价单：放弃编辑
     *
     * @return
     */
    @GetMapping("/orderUpdate_abandon")
    @Anonymous
    public AjaxResult SaleOrderUpdateAbandon() {
        return AjaxResult.success(saleOrderService.saleOrderWriteAbandon());
    }


    /**
     * 报价单确认 转换为销售订单
     *
     * @param id
     * @return
     */
    @GetMapping("/orderConfirm/{id}")
    @Anonymous
    public AjaxResult DraftSaleOrderConfirmToSale(@PathVariable("id") Long id) {
        saleOrderService.draftSaleOrderConfirmToSaleOrder(id);
        return AjaxResult.success("确认成功");
    }

    /**
     * 报价单取消
     *
     * @param id
     * @return
     */
    @GetMapping("/orderCancel/{id}")
    @Anonymous
    public AjaxResult SaleOrderCancel(@PathVariable("id") Long id) {
        saleOrderService.saleOrderCancel(id);
        return AjaxResult.success("订单已废弃");
    }

    /**
     * 发货记录查询
     *
     * @param
     * @return
     */
    @GetMapping("/saleOrderMoveLine/{orderId}}")
    @Anonymous
    public AjaxResult getOrderMoveLineRecord(@PathVariable("orderId") Long orderId) {
        return AjaxResult.success();
    }

    /**
     * 收货记录查询
     *
     * @param
     * @return
     */
    @GetMapping("/purchaseOrderMoveLine/{orderId}}")
    @Anonymous
    public AjaxResult getPurchaseMoveLineRecord(@PathVariable("orderId") Long orderId) {
        return AjaxResult.success();
    }

    /**
     * 收货记录查询
     *
     * @param
     * @return
     */
    @GetMapping("/lastName/{partnerId}")
    @Anonymous
    public AjaxResult getOrderLastNameTest(@PathVariable("partnerId") Long partnerId) {
        return AjaxResult.success(saleOrderService.orderNameGenerator(partnerId));
    }


    /**
     * @param
     * @return TODO
     */
    @GetMapping("/partName/ok")
    @Anonymous
    public AjaxResult setSalePartNameRight() {
        return AjaxResult.success(saleOrderPartService.saleOrderPartNamePut());
    }


    /**
     *
     */

    /**
     * TODO
     *
     * @param
     * @return
     */
    @GetMapping("/partName/okk")
    @Anonymous
    public AjaxResult setPurchasePartNameRight() {
        return AjaxResult.success(purchaseOrderPartService.purchaseOrderPartNamePut());
    }


    /**
     * TODO
     *
     * @param
     * @return
     */
    @GetMapping("/partName/str")
    @Anonymous
    public AjaxResult str() {
        String url ="/profile/ruoyi/2023/07/19/photo.png";
        String substring = url.substring(url.lastIndexOf("/")+1);
        System.out.println(substring);

        return AjaxResult.success(substring);
    }


}
