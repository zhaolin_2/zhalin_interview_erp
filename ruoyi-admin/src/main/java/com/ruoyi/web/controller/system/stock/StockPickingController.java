package com.ruoyi.web.controller.system.stock;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.StockMove;
import com.ruoyi.system.domain.stock.vo.StockDeliveryOrReceiveVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.StockPicking;
import com.ruoyi.system.service.IStockPickingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库存拣货Controller
 * 
 * @author Lin
 * @date 2023-05-14
 */
@RestController
@RequestMapping("/system/picking")
public class StockPickingController extends BaseController
{
    @Autowired
    private IStockPickingService stockPickingService;

    /**
     * 查询库存拣货列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockPicking stockPicking)
    {
        startPage();
        List<StockPicking> list = stockPickingService.selectStockPickingList(stockPicking);
        return getDataTable(list);
    }

    /**
     * 导出库存拣货列表
     */
    @Log(title = "库存拣货", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockPicking stockPicking)
    {
        List<StockPicking> list = stockPickingService.selectStockPickingList(stockPicking);
        ExcelUtil<StockPicking> util = new ExcelUtil<StockPicking>(StockPicking.class);
        util.exportExcel(response, list, "库存拣货数据");
    }

    /**
     * 获取库存拣货详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockPickingService.selectStockPickingById(id));
    }

    /**
     * 新增出入库操作（非确认操作，该接口只是单独新增记录，确认操作库存需要额外接口）
     */
    @PostMapping
    public AjaxResult add(@RequestBody StockPicking stockPicking)
    {
        return AjaxResult.success(stockPickingService.insertStockPicking(stockPicking));
    }

    /**
     * 修改出入库操作
     */
    @PutMapping
    public AjaxResult edit(@RequestBody StockPicking stockPicking)
    {
        return toAjax(stockPickingService.updateStockPicking(stockPicking));
    }

    /**
     * 删除库存拣货
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockPickingService.deleteStockPickingByIds(ids));
    }

    @GetMapping("/movePartsInfo/{id}")
    @Anonymous
    public AjaxResult GetMovePartsInfoById(@PathVariable("id") Long id){
        return AjaxResult.success(stockPickingService.getMovePartsInfoByPickingId(id));
    }

//p



    /**
     * 出入库管理 -操作编辑
     */
    @PostMapping("/picking_write")
    public AjaxResult writeIn(@RequestBody StockPicking stockPicking) throws IllegalAccessException {
        return AjaxResult.success(stockPickingService.writePicking(stockPicking));
    }


    /**
     * 传入库存模板id
     * @param StockId
     * @return
     */
    @Anonymous
    @GetMapping("/movePart_get/{stockId}")
    public AjaxResult GetSaleOrderPartInfoByStockId(@PathVariable("stockId")Long StockId){
        List list = new ArrayList();
        list.add(stockPickingService.getMovePartFromStockProductTemplateId(StockId));
        return AjaxResult.success(list);
    }


    /**
     * 订单组件删除
     * @param sequenceId
     * @return
     */
    @GetMapping("/movePart_delete/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult movePartBySequenceId(@PathVariable("sequenceId") Long sequenceId,@PathVariable("isInsert") String isInsert){
        return AjaxResult.success(stockPickingService.stockMovePartDelete(sequenceId,isInsert));
    }


    /**
     * 组件修改-订单组件修改前的信息获取
     * @param sequenceId 修改订单行的序列id
     * @param isInsert 编辑时修改/写入后修改
     * @return
     */
    @GetMapping("/movePart_info/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult GetSaleOrderPartInfoBySequenceId(@PathVariable("sequenceId") Long sequenceId,@PathVariable("isInsert") String isInsert){
        return AjaxResult.success(stockPickingService.movePartInfo(sequenceId,isInsert));
    }


    /**
     * 出入库管理 出入库组件编辑
     */
    @PutMapping("/movePart_update")
    public AjaxResult stockMovePartEdit(@RequestBody StockMove stockMove) {
        return AjaxResult.success(stockPickingService.stockMovePartUpdate(stockMove));
    }


    /**
     * 出入库管理：放弃编辑
     * @return
     */
    @GetMapping("/pickingUpdate_abandon")
    @Anonymous
    public AjaxResult stockPickingUpdateAbandon(){
        return AjaxResult.success(stockPickingService.stockPickingUpdateAbandon());
    }

    /**
     * 出入库管理：
     * @return
     */
    @GetMapping("/picking_confirm/{id}")
    @Anonymous
    public AjaxResult stockPickingConfirm(@PathVariable("id") Long id){
        return AjaxResult.success(stockPickingService.stockPickingConfirm(id));
    }


    @GetMapping("/ok")
    @Anonymous
    public AjaxResult setNameOk(){
        stockPickingService.setPartnerNameByPid();
        return AjaxResult.success("ok");
    }


    /**
     * 发货
     */
    @PostMapping("/delivery")
    @Anonymous
    public AjaxResult pickDeliver(@RequestBody StockPicking stockPicking){
        return AjaxResult.success(stockPickingService.stockDelivery(stockPicking));
    }


    /**
     * 收货
     */
    @PostMapping("/receive")
    @Anonymous
    public AjaxResult pickReceive(@RequestBody StockPicking stockPicking){
        return AjaxResult.success(stockPickingService.stockReceive(stockPicking));
    }

}
