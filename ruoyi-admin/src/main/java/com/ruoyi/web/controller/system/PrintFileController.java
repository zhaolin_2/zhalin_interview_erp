package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.IPrintFileService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * @Author Lin
 * @Date 2023 06 07 14
 **/
@RestController
@RequestMapping("/system/print")
public class PrintFileController {

    @Autowired
    private IPrintFileService printFileService;

    @GetMapping(value = "/ok")
    @Anonymous
    public void getInfo(HttpServletResponse httpServletResponse,Long orderId) throws IOException {
        System.out.println(orderId);
        Workbook xwb = printFileService.writeSaleOrderConfirmFormExcel(orderId);
        httpServletResponse.reset();
//        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        httpServletResponse.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("订单审核测试.xlsx", "UTF-8"));
        OutputStream osOut = httpServletResponse.getOutputStream();
        // 将指定的字节写入此输出流
        xwb.write(osOut);
        // 刷新此输出流并强制将所有缓冲的输出字节被写出
        osOut.flush();
        // 关闭流
        osOut.close();
        return;
    }

    /**
     * 采购合同的打印方法 传递参数为采购订单的id
     * 根据订单id 查询采购订单 将订单和其相关内容 写入表内 通过文件流导出
     * @param httpServletResponse
     * @throws IOException
     */
    @GetMapping(value = "/pOrderContact")
    @Anonymous
    public void writePurchaseOrderContactFormExcel(HttpServletResponse httpServletResponse) throws IOException {
        Workbook xwb = printFileService.writePurchaseOrderContactFormExcel(1L);
        httpServletResponse.reset();
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        httpServletResponse.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("采购合同.xlsx", "UTF-8"));
        OutputStream osOut = httpServletResponse.getOutputStream();
        // 将指定的字节写入此输出流
        xwb.write(osOut);
        // 刷新此输出流并强制将所有缓冲的输出字节被写出
        osOut.flush();
        // 关闭流
        osOut.close();
        return;
    }

    public static void main(String[] args)
    {
        System.out.println(SecurityUtils.encryptPassword("admin123"));
    }


}
