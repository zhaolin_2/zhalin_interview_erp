package com.ruoyi.web.controller.system.sale;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.service.ISaleOrderPartService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单组件Controller
 *
 * @author Lin
 * @date 2023-04-06
 */
@RestController
@RequestMapping("/system/orderPart")
public class SaleOrderPartController extends BaseController
{
    @Autowired
    private ISaleOrderPartService saleOrderPartService;

    /**
     * 查询订单组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:orderPart:list')")
    @GetMapping("/list")
    public TableDataInfo list(SaleOrderPart saleOrderPart)
    {
        startPage();
        List<SaleOrderPart> list = saleOrderPartService.selectSaleOrderPartList(saleOrderPart);
        return getDataTable(list);
    }

    /**
     * 导出订单组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:orderPart:export')")
    @Log(title = "订单组件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SaleOrderPart saleOrderPart)
    {
        List<SaleOrderPart> list = saleOrderPartService.selectSaleOrderPartList(saleOrderPart);
        ExcelUtil<SaleOrderPart> util = new ExcelUtil<SaleOrderPart>(SaleOrderPart.class);
        util.exportExcel(response, list, "订单组件数据");
    }

    /**
     * 获取订单组件详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:orderPart:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(saleOrderPartService.selectSaleOrderPartById(id));
    }

    /**
     * 新增订单组件
     */
    @PreAuthorize("@ss.hasPermi('system:orderPart:add')")
    @Log(title = "订单组件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SaleOrderPart saleOrderPart)
    {
        return toAjax(saleOrderPartService.insertSaleOrderPart(saleOrderPart));
    }

    /**
     * 修改订单组件
     */
    @PreAuthorize("@ss.hasPermi('system:orderPart:edit')")
    @Log(title = "订单组件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SaleOrderPart saleOrderPart)
    {
        return toAjax(saleOrderPartService.updateSaleOrderPart(saleOrderPart));
    }

    /**
     * 删除订单组件
     */
    @PreAuthorize("@ss.hasPermi('system:orderPart:remove')")
    @Log(title = "订单组件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(saleOrderPartService.deleteSaleOrderPartByIds(ids));
    }
}
