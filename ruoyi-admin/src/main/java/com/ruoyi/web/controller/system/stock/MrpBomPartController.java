package com.ruoyi.web.controller.system.stock;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.MrpBomPart;
import com.ruoyi.system.service.IMrpBomPartService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物料清单组件Controller
 * 
 * @author Lin
 * @date 2023-04-18
 */
@RestController
@RequestMapping("/system/bom_part")
public class MrpBomPartController extends BaseController
{
    @Autowired
    private IMrpBomPartService mrpBomPartService;

    /**
     * 查询物料清单组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:bom_part:list')")
    @GetMapping("/list")
    public TableDataInfo list(MrpBomPart mrpBomPart)
    {
        startPage();
        List<MrpBomPart> list = mrpBomPartService.selectMrpBomPartList(mrpBomPart);
        return getDataTable(list);
    }

    /**
     * 导出物料清单组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:bom_part:export')")
    @Log(title = "物料清单组件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MrpBomPart mrpBomPart)
    {
        List<MrpBomPart> list = mrpBomPartService.selectMrpBomPartList(mrpBomPart);
        ExcelUtil<MrpBomPart> util = new ExcelUtil<MrpBomPart>(MrpBomPart.class);
        util.exportExcel(response, list, "物料清单组件数据");
    }

    /**
     * 获取物料清单组件详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:bom_part:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mrpBomPartService.selectMrpBomPartById(id));
    }

    /**
     * 新增物料清单组件
     */
    @PreAuthorize("@ss.hasPermi('system:bom_part:add')")
    @Log(title = "物料清单组件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MrpBomPart mrpBomPart)
    {
        return toAjax(mrpBomPartService.insertMrpBomPart(mrpBomPart));
    }

    /**
     * 修改物料清单组件
     */
    @PreAuthorize("@ss.hasPermi('system:bom_part:edit')")
    @Log(title = "物料清单组件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MrpBomPart mrpBomPart)
    {
        return toAjax(mrpBomPartService.updateMrpBomPart(mrpBomPart));
    }

    /**
     * 删除物料清单组件
     */
    @PreAuthorize("@ss.hasPermi('system:bom_part:remove')")
    @Log(title = "物料清单组件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mrpBomPartService.deleteMrpBomPartByIds(ids));
    }
}
