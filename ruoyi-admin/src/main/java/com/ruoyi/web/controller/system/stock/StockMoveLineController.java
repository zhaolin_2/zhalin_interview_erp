package com.ruoyi.web.controller.system.stock;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.StockMoveLine;
import com.ruoyi.system.service.IStockMoveLineService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库存移动记录Controller
 * 
 * @author Lin
 * @date 2023-06-05
 */
@RestController
@RequestMapping("/system/moveline")
public class StockMoveLineController extends BaseController
{
    @Autowired
    private IStockMoveLineService stockMoveLineService;

    /**
     * 查询库存移动记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:moveline:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockMoveLine stockMoveLine)
    {
        startPage();
        List<StockMoveLine> list = stockMoveLineService.selectStockMoveLineList(stockMoveLine);
        return getDataTable(list);
    }

    /**
     * 导出库存移动记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:moveline:export')")
    @Log(title = "库存移动记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockMoveLine stockMoveLine)
    {
        List<StockMoveLine> list = stockMoveLineService.selectStockMoveLineList(stockMoveLine);
        ExcelUtil<StockMoveLine> util = new ExcelUtil<StockMoveLine>(StockMoveLine.class);
        util.exportExcel(response, list, "库存移动记录数据");
    }

    /**
     * 获取库存移动记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:moveline:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockMoveLineService.selectStockMoveLineById(id));
    }

    /**
     * 新增库存移动记录
     */
    @PreAuthorize("@ss.hasPermi('system:moveline:add')")
    @Log(title = "库存移动记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockMoveLine stockMoveLine)
    {
        return toAjax(stockMoveLineService.insertStockMoveLine(stockMoveLine));
    }

    /**
     * 修改库存移动记录
     */
    @PreAuthorize("@ss.hasPermi('system:moveline:edit')")
    @Log(title = "库存移动记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockMoveLine stockMoveLine)
    {
        return toAjax(stockMoveLineService.updateStockMoveLine(stockMoveLine));
    }

    /**
     * 删除库存移动记录
     */
    @PreAuthorize("@ss.hasPermi('system:moveline:remove')")
    @Log(title = "库存移动记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockMoveLineService.deleteStockMoveLineByIds(ids));
    }
}
