package com.ruoyi.web.controller.system.stock;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.domain.stock.vo.*;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库存Controller
 * 
 * @author Lin
 * @date 2023-03-30
 */
@RestController
@RequestMapping("/system/stock")
public class StockProductTemplateController extends BaseController {

    @Autowired
    private IStockProductTemplateService stockProductService;

    @Autowired
    private IStockProductSupplierinfoService stockProductSupplierinfoService;

    @Autowired
    private IMrpBomService mrpBomService;

    @Autowired
    private IMrpBomPartService mrpBomPartService;

    @Autowired
    private IProductUomService productUomService;

    @Autowired
    private IStockQuantService stockQuantService;



    /**
     * 查询库存列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockProductTemplate stockProduct) {
        startPage();
        List<StockProductTemplate> list = stockProductService.selectStockProductList(stockProduct);
        return getDataTable(list);
    }

    /**
     * 导出库存列表
     */
    @Log(title = "库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockProductTemplate stockProduct) {
        List<StockProductTemplate> list = stockProductService.selectStockProductList(stockProduct);
        ExcelUtil<StockProductTemplate> util = new ExcelUtil<StockProductTemplate>(StockProductTemplate.class);
        util.exportExcel(response, list, "库存数据");
    }

    /**
     * 获取库存详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(stockProductService.selectStockProductById(id));
    }

    /**
     * 新增库存
     */
    @Log(title = "库存", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockProductTemplateVo stockProductVo) {
        return AjaxResult.success(stockProductService.insertStockProduct(stockProductVo));
    }

    /**
     * 修改库存
     */
    @Log(title = "库存", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockProductTemplate stockProduct) {
        return toAjax(stockProductService.updateStockProduct(stockProduct));
    }

    /**
     * 删除库存
     */
    @Log(title = "库存", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(stockProductService.deleteStockProductByIds(ids));
    }

    /**
     * 库存操作信息返回
     */
    @Anonymous
    @GetMapping("/stockMsg_get/{stockId}")
    public AjaxResult stockMessageGet(@PathVariable("stockId") Long stockId) {
        List<StockTemplateMessage> stockMessageByStockId = stockProductService.getStockMessageByStockId(stockId);
        return AjaxResult.success(stockMessageByStockId);
    }

    /**
     * 库存的重订货规则详情返回
     * @stockId 库存id
     */
    @Anonymous
    @GetMapping("/getReorderRules/{stockId}")
    public AjaxResult reorderRuleGetByid(@PathVariable("stockId") Long stockId){
        StockReorderRule stockReorderRulesByStockId = stockProductService.getStockReorderRulesByStockId(stockId);
        List list = new ArrayList();
        list.add(stockReorderRulesByStockId);
        return AjaxResult.success(list);
    }

    /**
     * 库存产品供应商信息返回
     */
    @Anonymous
    @GetMapping("/supplierinfo/{stockId}")
    public AjaxResult stockSupplierInfoGet(@PathVariable("stockId") Long stockId) {
        List<StockProductSupplierinfo> stockProductSupplierinfos = stockProductSupplierinfoService.selectStockProductSupplierinfoByStockTmplId(stockId);
        return AjaxResult.success(stockProductSupplierinfos);
    }

    /**
     * 库存产品采购记录返回
     */
    @Anonymous
    @GetMapping("/purchaseHty/{stockId}")
    public AjaxResult stockPurchaseHistoryGet(@PathVariable("stockId") Long stockId) {
        List<PurchaseOrderPart> purchaseOrderParts = stockProductService.selectStockPurchaseHistoryByStockId(stockId);
        return AjaxResult.success(purchaseOrderParts);
    }


    /**
     * 库存产品销售记录返回
     */
    @Anonymous
    @GetMapping("/saleHty/{stockId}")
    public AjaxResult stockSaleHistoryGet(@PathVariable("stockId") Long stockId) {
        List<SaleOrderPart> saleOrderParts = stockProductService.selectStockSaleHistoryByStockId(stockId);
        return AjaxResult.success(saleOrderParts);
    }


    /**
     * 库存产品物料清单获取
     * stkBomPart → stockBomPart
     */
    @Anonymous
    @GetMapping("/stkBomPart/{stockId}")
    public AjaxResult stockBomPartGet(@PathVariable("stockId") Long stockId) {
        List<MrpBomPart> mrpBomParts = new ArrayList<>();
        try {
            MrpBom mrpBom = mrpBomService.selectMrpBomByStockTemplateId(stockId);
            mrpBomParts = mrpBomService.selectMrpBomPartByBomId(mrpBom.getId());
        }catch(NullPointerException e){
            mrpBomParts = new ArrayList<>();
        }
        return AjaxResult.success(mrpBomParts);
    }


    /**
     * 库存产品用于的物料清单获取
     * stockBomBelongTo
     */
    @Anonymous
    @GetMapping("/stkBomBlt/{stockId}")
    public AjaxResult stockBomBelongToGet(@PathVariable("stockId") Long stockId) {
        List<MrpBomInStockVo> mrpBomInStockVos = stockProductService.selectStockBomBelongTo(stockId);
        return AjaxResult.success(mrpBomInStockVos);
    }


    /**
     * 库存产品 详情页内容：
     * 1.供应商信息增删改
     * 2.重订货规则增删改
     * 3.物料清单添加增删改
     */

    /**
     * 库存产品详情页 供应商信息 添加
     */
    @PostMapping("/supplier_add")
    @Anonymous
    public AjaxResult stockProductTemplateSupplierAdd(@RequestBody StockProductSupplierVo stockProductSupplierVo) {
        return toAjax(stockProductService.insertStockProductTemplateSupplier(stockProductSupplierVo));
    }



    /**
     * 删除库存产品的供应商信息
     */
    @DeleteMapping("/supplier_delete/{id}")
    @Anonymous
    public AjaxResult stockProductTemplateSupplierDelete(@PathVariable("id") Long id) {
        return toAjax(stockProductService.deleteStockProductSupplierInfoById(id));
    }


    /**
     * 重订货规则新增（库存产品详情页：用于库存产品新建时没有添加重订货规则的情况）
     */
    @PostMapping("/reorder_add")
    @Anonymous
    public AjaxResult stockProductTemplateReorderRuleAdd(@RequestBody StockReorderRuleVo stockReorderRuleVo){
        return toAjax(stockProductService.stockTemplateReorderRuleAdd(stockReorderRuleVo));
    }


    /**
     * 重订货规则修改
     */
    @PostMapping("/reorder_update")
    @Anonymous
    public AjaxResult stockProductTemplateReorderRuleUpdate(@RequestBody StockReorderRuleVo stockReorderRuleVo){
        return toAjax(stockProductService.stockTemplateReorderRuleUpdate(stockReorderRuleVo));
    }

    /**
     * 重订货规则删除
     */

    @DeleteMapping("/reorder_delete/{stockId}")
    @Anonymous
    public AjaxResult stockProductTemplateReorderRuleDelete(@PathVariable("stockId") Long stockId){
        return toAjax(stockProductService.stockTemplateReorderRuleDelete(stockId));
    }

    /**
     * 物料清单组件 增
     * 添加物料清单组件：
     * 情况一：如果该库存产品在bom中有对应数据 则直接给该bom添加组件
     * 情况二：如果该库存产品没有对应bom,则新建一个bom 再给该bom添加组件
     */
    @PostMapping("/bom_part_add")
    @Anonymous
    public AjaxResult stockProductBomPartsAdd(@RequestBody MrpBomPartVo mrpBomPartVo ){
          return toAjax(stockProductService.stockTemplateBomPartsAdd(mrpBomPartVo));
    }

    /**
     * 物料清单 删除
     */
    @DeleteMapping("/bom_part_delete/{productTemplateId}")
    @Anonymous
    public AjaxResult remove(@PathVariable Long productTemplateId)
    {
        return toAjax(mrpBomPartService.deleteMrpBomPartById(productTemplateId));
    }

//    /**
//     * 该方法用户选定的目标对象，用于生成信息的库存产品信息
//     * @param stockId 复制库存产品的源库存产品id
//     * @return 包含物料清单与重订货规则的库存产品对象
//     */
//    @GetMapping("/stockCopy/{stockId}")
//    @Anonymous
//    public AjaxResult copyStockTemplateProductToNewStock(@PathVariable("stockId") Long stockId){
//        stockProductService.copyStockTemplateProductByStockId(stockId);
//    }

    /**
     * 返回库存产品单位列表
     */
    @GetMapping("/uomList")
    @Anonymous
    public AjaxResult list() {
        List<ProductUom> productUoms = productUomService.selectProductUomList(new ProductUom());
        return AjaxResult.success(productUoms);
    }

    /**
     * 库存数量信息返回
     */
    @GetMapping("/quant/{stockId}")
    @Anonymous
    public AjaxResult stockQuantyMessageGet(@PathVariable("stockId") Long stockId) {
        return AjaxResult.success(stockProductService.selectStockQuantByStockTemplateId(stockId));
    }

    /**
     * 库存数量信息返回
     */
    @GetMapping("/quant/ok/{id}")
    @Anonymous
    public AjaxResult stockQuantyTest(@PathVariable("id") Long id) {
        stockProductService.takeSaleOrderApartProductNeeded(id);
        return AjaxResult.success();
    }


    /**
     * 库存数量信息返回
     */
    @GetMapping("/quant/init")
    @Anonymous
    public AjaxResult stockQuantyInit() {
        stockQuantService.stockQuantCalculate();
        return AjaxResult.success();
    }








}
