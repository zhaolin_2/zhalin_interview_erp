package com.ruoyi.web.controller.system.account;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.account.AccountPayment;
import com.ruoyi.system.service.IAccountPaymentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 付款Controller
 * 
 * @author Lin
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/system/payment")
public class AccountPaymentController extends BaseController
{
    @Autowired
    private IAccountPaymentService accountPaymentService;

    /**
     * 查询付款列表
     */
    @GetMapping("/list")
    public TableDataInfo list(AccountPayment accountPayment)
    {
        startPage();
        List<AccountPayment> list = accountPaymentService.selectAccountPaymentList(accountPayment);
        return getDataTable(list);
    }

    /**
     * 导出付款列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, AccountPayment accountPayment)
    {
        List<AccountPayment> list = accountPaymentService.selectAccountPaymentList(accountPayment);
        ExcelUtil<AccountPayment> util = new ExcelUtil<AccountPayment>(AccountPayment.class);
        util.exportExcel(response, list, "付款数据");
    }

    /**
     * 获取付款详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(accountPaymentService.selectAccountPaymentById(id));
    }

    /**
     * 新增付款
     */
    @PostMapping
    public AjaxResult add(@RequestBody AccountPayment accountPayment)
    {
        return toAjax(accountPaymentService.insertAccountPayment(accountPayment));
    }

    /**
     * 修改付款
     */
    @PutMapping
    public AjaxResult edit(@RequestBody AccountPayment accountPayment)
    {
        return toAjax(accountPaymentService.updateAccountPayment(accountPayment));
    }

    /**
     * 删除付款
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(accountPaymentService.deleteAccountPaymentByIds(ids));
    }
}
