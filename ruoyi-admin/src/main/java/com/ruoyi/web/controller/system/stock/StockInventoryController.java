package com.ruoyi.web.controller.system.stock;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.system.domain.sale.SaleOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.stock.StockInventory;
import com.ruoyi.system.service.IStockInventoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * InventoryController
 * 
 * @author Lin
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/system/inventory")
public class StockInventoryController extends BaseController
{
    @Autowired
    private IStockInventoryService stockInventoryService;

    /**
     * 查询Inventory列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockInventory stockInventory)
    {
        startPage();
        List<StockInventory> list = stockInventoryService.selectStockInventoryList(stockInventory);
        return getDataTable(list);
    }

    /**
     * 导出Inventory列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockInventory stockInventory)
    {
        List<StockInventory> list = stockInventoryService.selectStockInventoryList(stockInventory);
        ExcelUtil<StockInventory> util = new ExcelUtil<StockInventory>(StockInventory.class);
        util.exportExcel(response, list, "Inventory数据");
    }

    /**
     * 获取Inventory详细信息
     */
    @GetMapping(value = "/{id}")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockInventoryService.selectStockInventoryById(id));
    }

    /**
     * 新增Inventory
     */
    @PostMapping
    public AjaxResult add(@RequestBody StockInventory stockInventory)
    {
        return toAjax(stockInventoryService.insertStockInventory(stockInventory));
    }

    /**
     * 修改Inventory
     */
    @PutMapping
    public AjaxResult edit(@RequestBody StockInventory stockInventory)
    {
        return toAjax(stockInventoryService.updateStockInventory(stockInventory));
    }

    /**
     * 删除Inventory
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockInventoryService.deleteStockInventoryByIds(ids));
    }



    /**
     * 发起库存盘点：出库
     */
    @PostMapping("inventory_out")
    public AjaxResult stockInventoryOut(@RequestBody StockInventory stockInventory){
//        stockInventoryService.handleStockInventoryOut();
        return AjaxResult.success();
    }


    /**
     * 发起库存盘点：入库
     */
    @PostMapping("inventory_in")
    public AjaxResult stockInventory(){
//        stockInventoryService.handleStockInventoryIn();
        return AjaxResult.success();
    }

    /**
     * 发起库存梳理：把所有库存带有库存扣减的数据 的 lot_id 改写为t
     */
}
