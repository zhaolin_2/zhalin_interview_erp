package com.ruoyi.web.controller.system.stock;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.StockReorderRule;
import com.ruoyi.system.service.IStockReorderRuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 重订货Controller
 * 
 * @author Lin
 * @date 2023-04-17
 */
@RestController
@RequestMapping("/system/reorder")
public class StockReorderRuleController extends BaseController
{
    @Autowired
    private IStockReorderRuleService stockReorderRuleService;

    /**
     * 查询重订货列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockReorderRule stockReorderRule)
    {
        startPage();
        List<StockReorderRule> list = stockReorderRuleService.selectStockReorderRuleList(stockReorderRule);
        return getDataTable(list);
    }

    /**
     * 导出重订货列表
     */
    @PreAuthorize("@ss.hasPermi('system:reorder:export')")
    @Log(title = "重订货", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockReorderRule stockReorderRule)
    {
        List<StockReorderRule> list = stockReorderRuleService.selectStockReorderRuleList(stockReorderRule);
        ExcelUtil<StockReorderRule> util = new ExcelUtil<StockReorderRule>(StockReorderRule.class);
        util.exportExcel(response, list, "重订货数据");
    }

    /**
     * 根据id获取重订货详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockReorderRuleService.selectStockReorderRuleById(id));
    }

    /**
     * 新增重订货新增接口
     * 情况一：库存产品新建时添加重订货规则(1)
     * 情况二：库存产品详情页面添加重订货规则(2)
     * 情况三：重订货规则总览页面添加重订货规则(3)
     */
    @Log(title = "重订货", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockReorderRule stockReorderRule)
    {
        return toAjax(stockReorderRuleService.insertStockReorderRule(stockReorderRule));
    }

    /**
     * 修改重订货
     * 情况一 分支：导出表格统一重订货规则后更新(4)
     */
    @Log(title = "重订货", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockReorderRule stockReorderRule)
    {
        return toAjax(stockReorderRuleService.updateStockReorderRule(stockReorderRule));
    }

    /**
     * 删除重订货
     */
    @Log(title = "重订货", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockReorderRuleService.deleteStockReorderRuleByIds(ids));
    }
}
