package com.ruoyi.web.controller.system.stock;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.StockInventoryLine;
import com.ruoyi.system.service.IStockInventoryLineService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * InventoryLineController
 * 
 * @author Lin
 * @date 2023-05-31
 */
@RestController
@RequestMapping("/system/inventoryLine")
public class StockInventoryLineController extends BaseController
{
    @Autowired
    private IStockInventoryLineService stockInventoryLineService;

    /**
     * 查询InventoryLine列表
     */
    @PreAuthorize("@ss.hasPermi('system:inventoryLine:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockInventoryLine stockInventoryLine)
    {
        startPage();
        List<StockInventoryLine> list = stockInventoryLineService.selectStockInventoryLineList(stockInventoryLine);
        return getDataTable(list);
    }

    /**
     * 导出InventoryLine列表
     */
    @PreAuthorize("@ss.hasPermi('system:inventoryLine:export')")
    @Log(title = "InventoryLine", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockInventoryLine stockInventoryLine)
    {
        List<StockInventoryLine> list = stockInventoryLineService.selectStockInventoryLineList(stockInventoryLine);
        ExcelUtil<StockInventoryLine> util = new ExcelUtil<StockInventoryLine>(StockInventoryLine.class);
        util.exportExcel(response, list, "InventoryLine数据");
    }

    /**
     * 获取InventoryLine详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:inventoryLine:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockInventoryLineService.selectStockInventoryLineById(id));
    }

    /**
     * 新增InventoryLine
     */
    @PreAuthorize("@ss.hasPermi('system:inventoryLine:add')")
    @Log(title = "InventoryLine", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockInventoryLine stockInventoryLine)
    {
        return toAjax(stockInventoryLineService.insertStockInventoryLine(stockInventoryLine));
    }

    /**
     * 修改InventoryLine
     */
    @PreAuthorize("@ss.hasPermi('system:inventoryLine:edit')")
    @Log(title = "InventoryLine", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockInventoryLine stockInventoryLine)
    {
        return toAjax(stockInventoryLineService.updateStockInventoryLine(stockInventoryLine));
    }

    /**
     * 删除InventoryLine
     */
    @PreAuthorize("@ss.hasPermi('system:inventoryLine:remove')")
    @Log(title = "InventoryLine", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockInventoryLineService.deleteStockInventoryLineByIds(ids));
    }
}
