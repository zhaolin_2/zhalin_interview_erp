package com.ruoyi.web.controller.system.stock;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.StockMove;
import com.ruoyi.system.service.IStockMoveService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库存收发Controller
 * 
 * @author Lin
 * @date 2023-05-14
 */
@RestController
@RequestMapping("/system/move")
public class StockMoveController extends BaseController
{
    @Autowired
    private IStockMoveService stockMoveService;

    /**
     * 查询库存收发列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockMove stockMove)
    {
        startPage();
        List<StockMove> list = stockMoveService.selectStockMoveList(stockMove);
        return getDataTable(list);
    }

    /**
     * 导出库存收发列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockMove stockMove)
    {
        List<StockMove> list = stockMoveService.selectStockMoveList(stockMove);
        ExcelUtil<StockMove> util = new ExcelUtil<StockMove>(StockMove.class);
        util.exportExcel(response, list, "库存收发数据");
    }

    /**
     * 获取库存收发详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockMoveService.selectStockMoveById(id));
    }

    /**
     * 新增库存收发
     */
    @PostMapping
    public AjaxResult add(@RequestBody StockMove stockMove)
    {
        return toAjax(stockMoveService.insertStockMove(stockMove));
    }

    /**
     * 修改库存收发
     */
    @PutMapping
    public AjaxResult edit(@RequestBody StockMove stockMove)
    {
        return toAjax(stockMoveService.updateStockMove(stockMove));
    }

    /**
     * 删除库存收发
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockMoveService.deleteStockMoveByIds(ids));
    }
}
