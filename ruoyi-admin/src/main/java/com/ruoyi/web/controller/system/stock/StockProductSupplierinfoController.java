package com.ruoyi.web.controller.system.stock;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.StockProductSupplierinfo;
import com.ruoyi.system.service.IStockProductSupplierinfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库存供应商信息Controller
 * 
 * @author Lin
 * @date 2023-04-17
 */
@RestController
@RequestMapping("/system/supplierinfo")
public class StockProductSupplierinfoController extends BaseController
{
    @Autowired
    private IStockProductSupplierinfoService stockProductSupplierinfoService;

    /**
     * 查询库存供应商信息列表
     */

    @GetMapping("/list")
    public TableDataInfo list(StockProductSupplierinfo stockProductSupplierinfo)
    {
        startPage();
        List<StockProductSupplierinfo> list = stockProductSupplierinfoService.selectStockProductSupplierinfoList(stockProductSupplierinfo);
        return getDataTable(list);
    }

    /**
     * 导出库存供应商信息列表
     */
    @Log(title = "库存供应商信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockProductSupplierinfo stockProductSupplierinfo)
    {
        List<StockProductSupplierinfo> list = stockProductSupplierinfoService.selectStockProductSupplierinfoList(stockProductSupplierinfo);
        ExcelUtil<StockProductSupplierinfo> util = new ExcelUtil<StockProductSupplierinfo>(StockProductSupplierinfo.class);
        util.exportExcel(response, list, "库存供应商信息数据");
    }

    /**
     * 获取库存供应商信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockProductSupplierinfoService.selectStockProductSupplierinfoById(id));
    }

    /**
     * 新增库存供应商信息
     */
    @Log(title = "库存供应商信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockProductSupplierinfo stockProductSupplierinfo)
    {
        return toAjax(stockProductSupplierinfoService.insertStockProductSupplierinfo(stockProductSupplierinfo));
    }

    /**
     * 修改库存供应商信息
     */
    @Log(title = "库存供应商信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockProductSupplierinfo stockProductSupplierinfo)
    {
        return toAjax(stockProductSupplierinfoService.updateStockProductSupplierinfo(stockProductSupplierinfo));
    }

    /**
     * 删除库存供应商信息
     */
    @Log(title = "库存供应商信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockProductSupplierinfoService.deleteStockProductSupplierinfoByIds(ids));
    }

    /**
     * 库存数量信息返回
     * TODO
     */
    @GetMapping("/IdInit")
    @Anonymous
    public AjaxResult stockQuantyProductIdInit() {
        stockProductSupplierinfoService.setProductIdByTmplId();
        return AjaxResult.success();
    }

}
