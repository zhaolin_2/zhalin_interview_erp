package com.ruoyi.web.controller.system.stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Lin
 * @Date 2023 05 25 18
 **/
public class Component {
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Component> getComponents() {
            return components;
        }

        public void setComponents(List<Component> components) {
            this.components = components;
        }

        private String name;
        private List<Component> components;

        public Component(String name) {
            this.name = name;
            this.components = new ArrayList<>();
        }

        public void addComponent(Component component) {
            this.components.add(component);
        }

        // getter and setter methods

    private static Map<String, Component>  split(Component component, Map<String, Component> map) {
        if (component == null) {
            return null;
        }

        if (map.containsKey(component.getName())) {
            // 如果已经存在该组件，说明该组件已经被处理过了，直接返回

            return map;
        }

        // 将该组件添加到Map中

        map.put(component.getName(), component);

        // 递归处理子组件

        for (Component c : component.getComponents()) {
            split(c, map);
        }
        return map;
        }

    public static void main(String[] args) {
        Component component1 = new Component("component1");
        Component component2 = new Component("component2");
        Component component3 = new Component("component3");
        Component component4 = new Component("component4");
        Component component5 = new Component("component5");
        Component component6 = new Component("component6");
        Component component7 = new Component("component7");

        component1.addComponent(component2);
        component1.addComponent(component3);

        component2.addComponent(component4);
        component2.addComponent(component5);

        component3.addComponent(component6);
        component3.addComponent(component7);

        // 调用递归函数来拆分产品A成单一组件

        Map<String, Component> map = new HashMap<>();
        Map<String, Component> split = split(component1, map);

        // 输出结果

        for (Map.Entry<String, Component> entry : split.entrySet()) {
            System.out.println(entry.getKey());
        }
    }
}
