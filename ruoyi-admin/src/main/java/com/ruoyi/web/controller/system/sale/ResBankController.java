package com.ruoyi.web.controller.system.sale;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.sale.ResBank;
import com.ruoyi.system.service.IResBankService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 银行管理Controller
 * 
 * @author Lin
 * @date 2023-04-03
 */
@RestController
@RequestMapping("/system/bank")
public class ResBankController extends BaseController
{
    @Autowired
    private IResBankService resBankService;

    /**
     * 查询银行管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:bank:list')")
    @GetMapping("/list")
    public TableDataInfo list(ResBank resBank)
    {
        startPage();
        List<ResBank> list = resBankService.selectResBankList(resBank);
        return getDataTable(list);
    }

    /**
     * 导出银行管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:bank:export')")
    @Log(title = "银行管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResBank resBank)
    {
        List<ResBank> list = resBankService.selectResBankList(resBank);
        ExcelUtil<ResBank> util = new ExcelUtil<ResBank>(ResBank.class);
        util.exportExcel(response, list, "银行管理数据");
    }

    /**
     * 获取银行管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:bank:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resBankService.selectResBankById(id));
    }

    /**
     * 新增银行管理
     */
    @PreAuthorize("@ss.hasPermi('system:bank:add')")
    @Log(title = "银行管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResBank resBank)
    {
        return toAjax(resBankService.insertResBank(resBank));
    }

    /**
     * 修改银行管理
     */
    @PreAuthorize("@ss.hasPermi('system:bank:edit')")
    @Log(title = "银行管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResBank resBank)
    {
        return toAjax(resBankService.updateResBank(resBank));
    }

    /**
     * 删除银行管理
     */
    @PreAuthorize("@ss.hasPermi('system:bank:remove')")
    @Log(title = "银行管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resBankService.deleteResBankByIds(ids));
    }
}
