package com.ruoyi.web.controller.system.purchase;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.service.IPurchaseOrderPartService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购订单组件Controller
 * 
 * @author Lin
 * @date 2023-04-06
 */
@RestController
@RequestMapping("/system/purchasePart")
public class PurchaseOrderPartController extends BaseController
{
    @Autowired
    private IPurchaseOrderPartService purchaseOrderPartService;

    /**
     * 查询采购订单组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:purchasePart:list')")
    @GetMapping("/list")
    public TableDataInfo list(PurchaseOrderPart purchaseOrderPart)
    {
        startPage();
        List<PurchaseOrderPart> list = purchaseOrderPartService.selectPurchaseOrderPartList(purchaseOrderPart);
        return getDataTable(list);
    }

    /**
     * 导出采购订单组件列表
     */
    @PreAuthorize("@ss.hasPermi('system:purchasePart:export')")
    @Log(title = "采购订单组件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PurchaseOrderPart purchaseOrderPart)
    {
        List<PurchaseOrderPart> list = purchaseOrderPartService.selectPurchaseOrderPartList(purchaseOrderPart);
        ExcelUtil<PurchaseOrderPart> util = new ExcelUtil<PurchaseOrderPart>(PurchaseOrderPart.class);
        util.exportExcel(response, list, "采购订单组件数据");
    }

    /**
     * 获取采购订单组件详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:purchasePart:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(purchaseOrderPartService.selectPurchaseOrderPartById(id));
    }

    /**
     * 新增采购订单组件
     */
    @PreAuthorize("@ss.hasPermi('system:purchasePart:add')")
    @Log(title = "采购订单组件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PurchaseOrderPart purchaseOrderPart)
    {
        return toAjax(purchaseOrderPartService.insertPurchaseOrderPart(purchaseOrderPart));
    }

    /**
     * 修改采购订单组件
     */
    @PreAuthorize("@ss.hasPermi('system:purchasePart:edit')")
    @Log(title = "采购订单组件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PurchaseOrderPart purchaseOrderPart)
    {
        return toAjax(purchaseOrderPartService.updatePurchaseOrderPart(purchaseOrderPart));
    }

    /**
     * 删除采购订单组件
     */
    @PreAuthorize("@ss.hasPermi('system:purchasePart:remove')")
    @Log(title = "采购订单组件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(purchaseOrderPartService.deletePurchaseOrderPartByIds(ids));
    }
}
