package com.ruoyi.web.controller.system.stock;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.StockLocation;
import com.ruoyi.system.service.IStockLocationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库存位置Controller
 * 
 * @author Lin
 * @date 2023-04-14
 */
@RestController
@RequestMapping("/system/location")
public class StockLocationController extends BaseController
{
    @Autowired
    private IStockLocationService stockLocationService;

    /**
     * 查询库存位置列表
     */
    @PreAuthorize("@ss.hasPermi('system:location:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockLocation stockLocation)
    {
        startPage();
        List<StockLocation> list = stockLocationService.selectStockLocationList(stockLocation);
        return getDataTable(list);
    }

    /**
     * 导出库存位置列表
     */
    @PreAuthorize("@ss.hasPermi('system:location:export')")
    @Log(title = "库存位置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockLocation stockLocation)
    {
        List<StockLocation> list = stockLocationService.selectStockLocationList(stockLocation);
        ExcelUtil<StockLocation> util = new ExcelUtil<StockLocation>(StockLocation.class);
        util.exportExcel(response, list, "库存位置数据");
    }

    /**
     * 获取库存位置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:location:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockLocationService.selectStockLocationById(id));
    }

    /**
     * 新增库存位置
     */
    @PreAuthorize("@ss.hasPermi('system:location:add')")
    @Log(title = "库存位置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockLocation stockLocation)
    {
        return toAjax(stockLocationService.insertStockLocation(stockLocation));
    }

    /**
     * 修改库存位置
     */
    @PreAuthorize("@ss.hasPermi('system:location:edit')")
    @Log(title = "库存位置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockLocation stockLocation)
    {
        return toAjax(stockLocationService.updateStockLocation(stockLocation));
    }

    /**
     * 删除库存位置
     */
    @PreAuthorize("@ss.hasPermi('system:location:remove')")
    @Log(title = "库存位置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockLocationService.deleteStockLocationByIds(ids));
    }
}
