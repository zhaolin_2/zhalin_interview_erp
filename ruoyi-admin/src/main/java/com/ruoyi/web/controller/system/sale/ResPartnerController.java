package com.ruoyi.web.controller.system.sale;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.service.IResPartnerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 合作伙伴Controller
 * 
 * @author Lin
 * @date 2023-04-03
 */
@RestController
@RequestMapping("/system/partner")
public class ResPartnerController extends BaseController
{
    @Autowired
    private IResPartnerService resPartnerService;

    /**
     * 查询合作伙伴列表
     */
    @GetMapping("/list")
    @Anonymous
    public TableDataInfo list(ResPartner resPartner)
    {
        startPage();
        List<ResPartner> list = resPartnerService.selectResPartnerList(resPartner);
        return getDataTable(list);
    }

    /**
     * 导出合作伙伴列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResPartner resPartner)
    {
        List<ResPartner> list = resPartnerService.selectResPartnerList(resPartner);
        ExcelUtil<ResPartner> util = new ExcelUtil<ResPartner>(ResPartner.class);
        util.exportExcel(response, list, "合作伙伴数据");
    }

    /**
     * 获取合作伙伴详细信息
     */
    @Anonymous
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resPartnerService.selectResPartnerById(id));
    }

    /**
     * 新增合作伙伴
     */
    @PostMapping
    public AjaxResult add(@RequestBody ResPartner resPartner)
    {
        return toAjax(resPartnerService.insertResPartner(resPartner));
    }

    /**
     * 修改合作伙伴
     */
    @PutMapping
    public AjaxResult edit(@RequestBody ResPartner resPartner)
    {
        return toAjax(resPartnerService.updateResPartner(resPartner));
    }

    /**
     * 删除合作伙伴
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resPartnerService.deleteResPartnerByIds(ids));
    }


    /**
     * 根据伙伴id查询详细信息包括账户（关联银行）信息
     */
    @Anonymous
    @GetMapping("/getPtnActMsg/{partnerId}")
    public AjaxResult list(@PathVariable Long partnerId)
    {
        List list = resPartnerService.selectPartnerMessageByPartnerId(partnerId);
        return AjaxResult.success(list);
    }


}
