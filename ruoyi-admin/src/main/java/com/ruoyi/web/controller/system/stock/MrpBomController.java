package com.ruoyi.web.controller.system.stock;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.stock.MrpBomMsg;
import com.ruoyi.system.domain.stock.MrpBomPart;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.stock.MrpBom;
import com.ruoyi.system.service.IMrpBomService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物料清单Controller
 * 
 * @author Lin
 * @date 2023-04-18
 */
@RestController
@RequestMapping("/system/bom")
public class MrpBomController extends BaseController
{
    @Autowired
    private IMrpBomService mrpBomService;

    /**
     * 查询物料清单列表
     */
    @GetMapping("/list")
    public TableDataInfo list(MrpBom mrpBom)
    {
        startPage();
        List<MrpBom> list = mrpBomService.selectMrpBomList(mrpBom);
        return getDataTable(list);
    }

    /**
     * 导出物料清单列表
     */
    @Log(title = "物料清单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MrpBom mrpBom)
    {
        List<MrpBom> list = mrpBomService.selectMrpBomList(mrpBom);
        ExcelUtil<MrpBom> util = new ExcelUtil<MrpBom>(MrpBom.class);
        util.exportExcel(response, list, "物料清单数据");
    }

    /**
     * 获取物料清单详细信息
     */
    @Anonymous
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(mrpBomService.selectMrpBomById(id));
    }

    /**
     * 新增物料清单
     */
    @PostMapping
    public AjaxResult add(@RequestBody MrpBom mrpBom)
    {
        return toAjax(mrpBomService.insertMrpBom(mrpBom));
    }

    /**
     * 插入订单行以及编辑订单销售订单
     */
    @PostMapping("/bom_write")
    public AjaxResult writeIn(@RequestBody MrpBom mrpBom) throws IllegalAccessException {
        return AjaxResult.success(mrpBomService.writeMrpBom(mrpBom));
    }

    /**
     * 修改物料清单
     */
    @Log(title = "物料清单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MrpBom mrpBom)
    {
        return toAjax(mrpBomService.updateMrpBom(mrpBom));
    }

    /**
     * 删除物料清单
     */
    @Log(title = "物料清单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mrpBomService.deleteMrpBomByIds(ids));
    }


    /**
     * 获取物料清单详细信息
     */
    @GetMapping(value = "/bomParts/{bomId}")
    @Anonymous
    public AjaxResult BomPartGet(@PathVariable("bomId") Long bomId)
    {
        List<MrpBomPart> mrpBomParts = mrpBomService.selectMrpBomPartByBomId(bomId);
        return AjaxResult.success(mrpBomParts);
    }


    /**
     * 根据bom id 查询bom 操作详情
     */
    @GetMapping(value = "/bomMsg/{bomId}")
    @Anonymous
    public AjaxResult BomMsgGet(@PathVariable("bomId") Long bomId)
    {
        List<MrpBomMsg> mrpBomMsgs = mrpBomService.selectMrpBomMsgByBomId(bomId);
        return AjaxResult.success(mrpBomMsgs);
    }


    /**
     * 方法说明：
     * 传入库存产品id 返回对应的待编辑bom子组件
     * @param StockId
     * @return
     */
    @GetMapping("/bomPart_get/{stockId}")
    @Anonymous
    public AjaxResult GetBomPartInfoByStockId(@PathVariable("stockId")Long StockId){
        List list = new ArrayList();
        list.add(mrpBomService.getMrpBomPartPartFromStockProductTemplateId(StockId));
        return AjaxResult.success(list);
    }


    /**
     * 订单组件修改-订单组件修改前的信息获取
     * @param sequenceId 修改订单行的序列id
     * @param isInsert 编辑时修改/写入后修改
     * @return
     */
    @GetMapping("/bomPart_info/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult GetBomPartInfoBySequenceId(@PathVariable("sequenceId") Long sequenceId,@PathVariable("isInsert") String isInsert){
        return AjaxResult.success(mrpBomService.bomPartInfo(sequenceId,isInsert));
    }


    /**
     * 销售订单-编辑中：销售订单的 订单组件 更新
     */
    @PutMapping("/bomPart_update")
    public AjaxResult purchaseOrderPartEdit(@RequestBody MrpBomPart mrpBomPart) {
        return AjaxResult.success(mrpBomService.mrpBomPartUpdate(mrpBomPart));
    }

    /**
     * 采购订单组件删除
     * @param sequenceId
     * @return
     */
    @GetMapping("/bomPart_delete/{sequenceId}/{isInsert}")
    @Anonymous
    public AjaxResult BomPartBySequenceId(@PathVariable("sequenceId") Long sequenceId,@PathVariable("isInsert") String isInsert){
        return AjaxResult.success(mrpBomService.bomPartDelete(sequenceId,isInsert));
    }

    /**
     * bom编辑：放弃编辑
     * @return
     */
    @GetMapping("/bomUpdate_abandon")
    @Anonymous
    public AjaxResult BomUpdateAbandon(){
        return AjaxResult.success(mrpBomService.bomUpdateAbandon());
    }

    /**
     * bom编辑：放弃编辑
     * @return
     */
    @GetMapping("/bom/ok")
    @Anonymous
    public AjaxResult bomHandled(){
        mrpBomService.bomNameAndProductIdHandle();
        return AjaxResult.success();
    }


}
