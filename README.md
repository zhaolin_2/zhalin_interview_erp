# 由java+mysql重写python+postgresql的erp项目

## 项目概述
尊敬的面试官、领导：
您好！
本项目是一个基于若依开发框架（基于SpringBoot、Spring Security、Jwt、Vue的前后端分离的后台管理系统）开发的ERP系统，是由一个名为odoo的开源erp系统重写而来，重构后的系统由java语言、Mysql作为数据库组成，该项目用于重构的功能设计、代码编写、测试、项目部署、服务器搭建和环境搭建、接口文档编写由我独立完成，本项目的技术选型为：

1、系统环境
Java EE 8
Servlet 3.0
Apache Maven 3  
2、主框架
Spring Boot 2.2.x
Spring Framework 5.2.x
Spring Security 5.2.x   
3、持久层
Apache MyBatis 3.5.x
Hibernate Validation 6.0.x
Alibaba Druid 1.2.x     
4、视图层
Vue 2.6.x
Axios 0.21.x
Element 2.15.x  
## 项目详情
本项目是一个重构项目，原项目是一个名为odoo的开源ERP系统（该系统为python语言编写+postgreSql数据库），因为某种原因，该项目需要进行重构后扩展，所以使用了我熟悉的java语言+mysql数据库进行了重构，首先进行了数据库的转移，重新根据原来系统的功能设计了java版本的功能代码，但是由于缺少前项目的开发文档以及需要迁就旧数据的留存问题，该项目目前仍在完善中。  
重构后的系统拥有**库存**、**销售**、**采购**三个模块，内部运转逻辑开始于销售，库存不足后触发库存的预警数量，自动（或者手动）发起采购，生成采购合同，采购后入库库存系统，完整整体的闭环。   
    
其中库存模块包括的功能为：收发货、BOM管理、库存盘点、退料、库存数量预警；  
采购模块包含的功能：供应商管理、采购订单管理、编辑库存重订货规则、供应商报价管理；   
销售模块包含的功能：客户管理、销售订单管理   
除此之外，该系统包含常规的用户管理、权限管理、部门岗位管理等基础功能。

## 项目展示

项目地址：http://47.104.182.251:91/   账号：admin 密码：admin123       

项目接口文档地址(语雀文档)：https://www.yuque.com/zhaolin-xsahb/sga5em/kwk4gh8wapbmbtwu?singleDoc# 《ERP项目-接口文档》 密码：wdrr

gitee: https://gitee.com/zhaolin_2/zhalin_interview_erp
