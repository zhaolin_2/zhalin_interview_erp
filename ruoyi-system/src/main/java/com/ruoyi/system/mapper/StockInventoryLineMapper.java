package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockInventoryLine;
import org.apache.ibatis.annotations.Mapper;

/**
 * InventoryLineMapper接口
 * 
 * @author Lin
 * @date 2023-05-31
 */
@Mapper
public interface StockInventoryLineMapper 
{
    /**
     * 查询InventoryLine
     * 
     * @param id InventoryLine主键
     * @return InventoryLine
     */
    public StockInventoryLine selectStockInventoryLineById(Long id);

    /**
     * 查询InventoryLine列表
     * 
     * @param stockInventoryLine InventoryLine
     * @return InventoryLine集合
     */
    public List<StockInventoryLine> selectStockInventoryLineList(StockInventoryLine stockInventoryLine);

    /**
     * 新增InventoryLine
     * 
     * @param stockInventoryLine InventoryLine
     * @return 结果
     */
    public int insertStockInventoryLine(StockInventoryLine stockInventoryLine);

    /**
     * 修改InventoryLine
     * 
     * @param stockInventoryLine InventoryLine
     * @return 结果
     */
    public int updateStockInventoryLine(StockInventoryLine stockInventoryLine);

    /**
     * 删除InventoryLine
     * 
     * @param id InventoryLine主键
     * @return 结果
     */
    public int deleteStockInventoryLineById(Long id);

    /**
     * 批量删除InventoryLine
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockInventoryLineByIds(Long[] ids);


    /**
     * 根据inventory 查询inventory line
     */
    public List<StockInventoryLine> selectStockInventoryLineByInInventoryId(Long inventoryId);
}
