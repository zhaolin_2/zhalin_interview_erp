package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockProductEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品实体Mapper接口
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Mapper
public interface StockProductEntityMapper 
{
    /**
     * 查询产品实体
     * 
     * @param id 产品实体主键
     * @return 产品实体
     */
    public StockProductEntity selectStockProductEntityById(Long id);

    /**
     * 查询产品实体列表
     * 
     * @param stockProductEntity 产品实体
     * @return 产品实体集合
     */
    public List<StockProductEntity> selectStockProductEntityList(StockProductEntity stockProductEntity);

    /**
     * 新增产品实体
     * 
     * @param stockProductEntity 产品实体
     * @return 结果
     */
    public int insertStockProductEntity(StockProductEntity stockProductEntity);

    /**
     * 修改产品实体
     * 
     * @param stockProductEntity 产品实体
     * @return 结果
     */
    public int updateStockProductEntity(StockProductEntity stockProductEntity);

    /**
     * 删除产品实体
     * 
     * @param id 产品实体主键
     * @return 结果
     */
    public int deleteStockProductEntityById(Long id);

    /**
     * 批量删除产品实体
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockProductEntityByIds(Long[] ids);


    StockProductEntity selectStockProductEntityByTemplateId(Long stockId);
}
