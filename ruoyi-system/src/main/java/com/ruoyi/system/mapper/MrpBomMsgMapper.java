package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.MrpBomMsg;
import org.apache.ibatis.annotations.Mapper;

/**
 * bom操作信息Mapper接口
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Mapper
public interface MrpBomMsgMapper 
{
    /**
     * 查询bom操作信息
     * 
     * @param id bom操作信息主键
     * @return bom操作信息
     */
    public MrpBomMsg selectMrpBomMsgById(Long id);

    /**
     * 查询bom操作信息列表
     * 
     * @param mrpBomMsg bom操作信息
     * @return bom操作信息集合
     */
    public List<MrpBomMsg> selectMrpBomMsgList(MrpBomMsg mrpBomMsg);

    /**
     * 新增bom操作信息
     * 
     * @param mrpBomMsg bom操作信息
     * @return 结果
     */
    public int insertMrpBomMsg(MrpBomMsg mrpBomMsg);

    /**
     * 修改bom操作信息
     * 
     * @param mrpBomMsg bom操作信息
     * @return 结果
     */
    public int updateMrpBomMsg(MrpBomMsg mrpBomMsg);

    /**
     * 删除bom操作信息
     * 
     * @param id bom操作信息主键
     * @return 结果
     */
    public int deleteMrpBomMsgById(Long id);

    /**
     * 批量删除bom操作信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMrpBomMsgByIds(Long[] ids);

    /**
     * 根据bom id 查询bom操作信息
     * @param bomId
     * @return
     */
    List<MrpBomMsg> selectMrpBomMsgByBomId(Long bomId);
}
