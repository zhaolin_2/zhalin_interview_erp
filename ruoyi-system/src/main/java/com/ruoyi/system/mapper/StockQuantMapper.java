package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockQuant;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存数量Mapper接口
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Mapper
public interface StockQuantMapper 
{
    /**
     * 查询库存数量
     * 
     * @param id 库存数量主键
     * @return 库存数量
     */
    public StockQuant selectStockQuantById(Long id);


    public List<StockQuant> selectStockQuantListRealStockNum();

    /**
     * 查询库存数量列表
     * 
     * @param stockQuant 库存数量
     * @return 库存数量集合
     */
    public List<StockQuant> selectStockQuantList(StockQuant stockQuant);

    /**
     * 新增库存数量
     * 
     * @param stockQuant 库存数量
     * @return 结果
     */
    public int insertStockQuant(StockQuant stockQuant);

    /**
     * 修改库存数量
     * 
     * @param stockQuant 库存数量
     * @return 结果
     */
    public int updateStockQuant(StockQuant stockQuant);

    /**
     * 删除库存数量
     * 
     * @param id 库存数量主键
     * @return 结果
     */
    public int deleteStockQuantById(Long id);

    /**
     * 批量删除库存数量
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockQuantByIds(Long[] ids);

    /**
     * 根据产品id 查询产品实际剩余库存
     */
    StockQuant selectStockPhysicalInventoryByProductId(Long productId);

    /**
     * 库存查询  查询的是一个库存列表，包含者可能的产品库存 计算方式是 0-location_id为非15的quantity数量，就是产品在旧平台的显示数量
     */
    List<StockQuant> selectStockQuantByCalculate(Long productId);
}
