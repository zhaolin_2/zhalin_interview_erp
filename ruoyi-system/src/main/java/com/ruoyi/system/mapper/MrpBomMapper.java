package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.MrpBom;
import org.apache.ibatis.annotations.Mapper;

/**
 * 物料清单Mapper接口
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Mapper
public interface MrpBomMapper 
{
    /**
     * 查询物料清单
     * 
     * @param id 物料清单主键
     * @return 物料清单
     */
    public MrpBom selectMrpBomById(Long id);

    /**
     * 查询物料清单列表
     * 
     * @param mrpBom 物料清单
     * @return 物料清单集合
     */
    public List<MrpBom> selectMrpBomList(MrpBom mrpBom);

    /**
     * 新增物料清单
     * 
     * @param mrpBom 物料清单
     * @return 结果
     */
    public int insertMrpBom(MrpBom mrpBom);

    /**
     * 修改物料清单
     * 
     * @param mrpBom 物料清单
     * @return 结果
     */
    public int updateMrpBom(MrpBom mrpBom);

    /**
     * 删除物料清单
     * 
     * @param id 物料清单主键
     * @return 结果
     */
    public int deleteMrpBomById(Long id);

    /**
     * 批量删除物料清单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMrpBomByIds(Long[] ids);

    MrpBom selectMrpBomByStockTemplateId(Long stockId);
}
