package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockReorderRule;
import org.apache.ibatis.annotations.Mapper;

/**
 * 重订货Mapper接口
 * 
 * @author Lin
 * @date 2023-04-17
 */
@Mapper
public interface StockReorderRuleMapper 
{
    /**
     * 查询重订货
     * 
     * @param id 重订货主键
     * @return 重订货
     */
    public StockReorderRule selectStockReorderRuleById(Long id);

    /**
     * 查询重订货列表
     * 
     * @param stockReorderRule 重订货
     * @return 重订货集合
     */
    public List<StockReorderRule> selectStockReorderRuleList(StockReorderRule stockReorderRule);

    /**
     * 新增重订货
     * 
     * @param stockReorderRule 重订货
     * @return 结果
     */
    public int insertStockReorderRule(StockReorderRule stockReorderRule);

    /**
     * 修改重订货
     * 
     * @param stockReorderRule 重订货
     * @return 结果
     */
    public int updateStockReorderRule(StockReorderRule stockReorderRule);

    /**
     * 删除重订货
     * 
     * @param id 重订货主键
     * @return 结果
     */
    public int deleteStockReorderRuleById(Long id);

    /**
     * 批量删除重订货
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockReorderRuleByIds(Long[] ids);

    StockReorderRule selectStockReorderRuleByEntityId(Long id);
}
