package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.sale.ResPartnerAccount;
import org.apache.ibatis.annotations.Mapper;

/**
 * 账户信息Mapper接口
 * 
 * @author lin
 * @date 2023-04-03
 */
@Mapper
public interface ResPartnerAccountMapper 
{
    /**
     * 查询账户信息
     * 
     * @param id 账户信息主键
     * @return 账户信息
     */
    public ResPartnerAccount selectResPartnerAccountById(Long id);

    /**
     * 查询账户信息列表
     * 
     * @param resPartnerAccount 账户信息
     * @return 账户信息集合
     */
    public List<ResPartnerAccount> selectResPartnerAccountList(ResPartnerAccount resPartnerAccount);

    /**
     * 新增账户信息
     * 
     * @param resPartnerAccount 账户信息
     * @return 结果
     */
    public int insertResPartnerAccount(ResPartnerAccount resPartnerAccount);

    /**
     * 修改账户信息
     * 
     * @param resPartnerAccount 账户信息
     * @return 结果
     */
    public int updateResPartnerAccount(ResPartnerAccount resPartnerAccount);

    /**
     * 删除账户信息
     * 
     * @param id 账户信息主键
     * @return 结果
     */
    public int deleteResPartnerAccountById(Long id);

    /**
     * 批量删除账户信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResPartnerAccountByIds(Long[] ids);

    ResPartnerAccount selectResPartnerAccountByPartnerId(Long partnerId);
}
