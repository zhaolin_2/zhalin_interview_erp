package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockProductTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存Mapper接口
 * 
 * @author Lin
 * @date 2023-03-30
 */
@Mapper
public interface StockProductTemplateMapper
{
    /**
     * 查询库存
     * 
     * @param id 库存主键
     * @return 库存
     */
    public StockProductTemplate selectStockProductTemplateById(Long id);

    /**
     * 查询库存列表
     * 
     * @param stockProduct 库存
     * @return 库存集合
     */
    public List<StockProductTemplate> selectStockProductTemplateList(StockProductTemplate stockProduct);

    /**
     * 新增库存
     * 
     * @param stockProduct 库存
     * @return 结果
     */
    public int insertStockProductTemplate(StockProductTemplate stockProduct);

    /**
     * 修改库存
     * 
     * @param stockProduct 库存
     * @return 结果
     */
    public int updateStockProductTemplate(StockProductTemplate stockProduct);

    /**
     * 删除库存
     * 
     * @param id 库存主键
     * @return 结果
     */
    public int deleteStockProductTemplateById(Long id);

    /**
     * 批量删除库存
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockProductTemplateByIds(Long[] ids);
}
