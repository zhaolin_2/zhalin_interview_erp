package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单组件Mapper接口
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Mapper
public interface SaleOrderPartMapper 
{
    /**
     * 查询订单组件
     * 
     * @param id 订单组件主键
     * @return 订单组件
     */
    public SaleOrderPart selectSaleOrderPartById(Long id);

    /**
     * 查询订单组件列表
     * 
     * @param saleOrderPart 订单组件
     * @return 订单组件集合
     */
    public List<SaleOrderPart> selectSaleOrderPartList(SaleOrderPart saleOrderPart);

    /**
     * 新增订单组件
     * 
     * @param saleOrderPart 订单组件
     * @return 结果
     */
    public int insertSaleOrderPart(SaleOrderPart saleOrderPart);

    /**
     * 修改订单组件
     * 
     * @param saleOrderPart 订单组件
     * @return 结果
     */
    public int updateSaleOrderPart(SaleOrderPart saleOrderPart);

    /**
     * 删除订单组件
     * 
     * @param id 订单组件主键
     * @return 结果
     */
    public int deleteSaleOrderPartById(Long id);

    /**
     * 批量删除订单组件
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSaleOrderPartByIds(Long[] ids);

    List<SaleOrderPart> selectSaleOrderPartBySaleOrderId(Long orderId);
}
