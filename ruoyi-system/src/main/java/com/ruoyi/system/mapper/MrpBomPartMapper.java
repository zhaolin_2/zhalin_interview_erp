package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.MrpBomPart;
import org.apache.ibatis.annotations.Mapper;

/**
 * 物料清单组件Mapper接口
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Mapper
public interface MrpBomPartMapper 
{
    /**
     * 查询物料清单组件
     * 
     * @param id 物料清单组件主键
     * @return 物料清单组件
     */
    public MrpBomPart selectMrpBomPartById(Long id);

    /**
     * 查询物料清单组件列表
     * 
     * @param mrpBomPart 物料清单组件
     * @return 物料清单组件集合
     */
    public List<MrpBomPart> selectMrpBomPartList(MrpBomPart mrpBomPart);

    /**
     * 新增物料清单组件
     * 
     * @param mrpBomPart 物料清单组件
     * @return 结果
     */
    public int insertMrpBomPart(MrpBomPart mrpBomPart);

    /**
     * 修改物料清单组件
     * 
     * @param mrpBomPart 物料清单组件
     * @return 结果
     */
    public int updateMrpBomPart(MrpBomPart mrpBomPart);

    /**
     * 删除物料清单组件
     * 
     * @param id 物料清单组件主键
     * @return 结果
     */
    public int deleteMrpBomPartById(Long id);

    /**
     * 批量删除物料清单组件
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMrpBomPartByIds(Long[] ids);

    List<MrpBomPart> selectMrpBomPartByBomId(Long bomId);

    /**
     * 根据库存产品的实体id(stock_product_entity_id)也就是productId，查询所属的那些bom
     * @param id
     * @return
     */
    List<MrpBomPart> selectMrpBomPartByProductEntityId(Long id);
}
