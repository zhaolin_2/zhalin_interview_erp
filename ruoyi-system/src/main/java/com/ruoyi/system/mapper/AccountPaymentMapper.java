package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.account.AccountPayment;

/**
 * 付款Mapper接口
 * 
 * @author Lin
 * @date 2023-05-31
 */
public interface AccountPaymentMapper 
{
    /**
     * 查询付款
     * 
     * @param id 付款主键
     * @return 付款
     */
    public AccountPayment selectAccountPaymentById(Long id);

    /**
     * 查询付款列表
     * 
     * @param accountPayment 付款
     * @return 付款集合
     */
    public List<AccountPayment> selectAccountPaymentList(AccountPayment accountPayment);

    /**
     * 新增付款
     * 
     * @param accountPayment 付款
     * @return 结果
     */
    public int insertAccountPayment(AccountPayment accountPayment);

    /**
     * 修改付款
     * 
     * @param accountPayment 付款
     * @return 结果
     */
    public int updateAccountPayment(AccountPayment accountPayment);

    /**
     * 删除付款
     * 
     * @param id 付款主键
     * @return 结果
     */
    public int deleteAccountPaymentById(Long id);

    /**
     * 批量删除付款
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAccountPaymentByIds(Long[] ids);
}
