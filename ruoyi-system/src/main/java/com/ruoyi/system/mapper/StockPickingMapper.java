package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockPicking;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存拣货Mapper接口
 * 
 * @author Lin
 * @date 2023-05-14
 */
@Mapper
public interface StockPickingMapper 
{
    /**
     * 查询库存拣货
     * 
     * @param id 库存拣货主键
     * @return 库存拣货
     */
    public StockPicking selectStockPickingById(Long id);

    /**
     * 查询库存拣货列表
     * 
     * @param stockPicking 库存拣货
     * @return 库存拣货集合
     */
    public List<StockPicking> selectStockPickingList(StockPicking stockPicking);

    /**
     * 新增库存拣货
     * 
     * @param stockPicking 库存拣货
     * @return 结果
     */
    public int insertStockPicking(StockPicking stockPicking);

    /**
     * 修改库存拣货
     * 
     * @param stockPicking 库存拣货
     * @return 结果
     */
    public int updateStockPicking(StockPicking stockPicking);

    /**
     * 删除库存拣货
     * 
     * @param id 库存拣货主键
     * @return 结果
     */
    public int deleteStockPickingById(Long id);

    /**
     * 批量删除库存拣货
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockPickingByIds(Long[] ids);

    StockPicking selectStockPickingByMoveId(Long id);
}
