package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.ProductCategory;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品内部类别Mapper接口
 * 
 * @author Lin
 * @date 2023-06-09
 */
@Mapper
public interface ProductCategoryMapper 
{
    /**
     * 查询产品内部类别
     * 
     * @param id 产品内部类别主键
     * @return 产品内部类别
     */
    public ProductCategory selectProductCategoryById(Long id);

    /**
     * 查询产品内部类别列表
     * 
     * @param productCategory 产品内部类别
     * @return 产品内部类别集合
     */
    public List<ProductCategory> selectProductCategoryList(ProductCategory productCategory);

    /**
     * 新增产品内部类别
     * 
     * @param productCategory 产品内部类别
     * @return 结果
     */
    public int insertProductCategory(ProductCategory productCategory);

    /**
     * 修改产品内部类别
     * 
     * @param productCategory 产品内部类别
     * @return 结果
     */
    public int updateProductCategory(ProductCategory productCategory);

    /**
     * 删除产品内部类别
     * 
     * @param id 产品内部类别主键
     * @return 结果
     */
    public int deleteProductCategoryById(Long id);

    /**
     * 批量删除产品内部类别
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductCategoryByIds(Long[] ids);
}
