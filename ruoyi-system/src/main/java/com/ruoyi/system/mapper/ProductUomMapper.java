package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.stock.ProductUom;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
/**
 * 单位名称Mapper接口
 * 
 * @author Lin
 * @date 2023-06-09
 */
@Mapper
public interface ProductUomMapper 
{
    /**
     * 查询单位名称
     * 
     * @param id 单位名称主键
     * @return 单位名称
     */
    public ProductUom selectProductUomById(Long id);

    /**
     * 查询单位名称列表
     * 
     * @param productUom 单位名称
     * @return 单位名称集合
     */
    public List<ProductUom> selectProductUomList(ProductUom productUom);

    /**
     * 新增单位名称
     * 
     * @param productUom 单位名称
     * @return 结果
     */
    public int insertProductUom(ProductUom productUom);

    /**
     * 修改单位名称
     * 
     * @param productUom 单位名称
     * @return 结果
     */
    public int updateProductUom(ProductUom productUom);

    /**
     * 删除单位名称
     * 
     * @param id 单位名称主键
     * @return 结果
     */
    public int deleteProductUomById(Long id);

    /**
     * 批量删除单位名称
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductUomByIds(Long[] ids);
}
