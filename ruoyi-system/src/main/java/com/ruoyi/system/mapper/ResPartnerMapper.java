package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.sale.ResPartner;
import org.apache.ibatis.annotations.Mapper;

/**
 * 合作伙伴Mapper接口
 * 
 * @author Lin
 * @date 2023-04-03
 */
@Mapper
public interface ResPartnerMapper 
{
    /**
     * 查询合作伙伴
     * 
     * @param id 合作伙伴主键
     * @return 合作伙伴
     */
    public ResPartner selectResPartnerById(Long id);

    /**
     * 查询合作伙伴列表
     * 
     * @param resPartner 合作伙伴
     * @return 合作伙伴集合
     */
    public List<ResPartner> selectResPartnerList(ResPartner resPartner);

    /**
     * 新增合作伙伴
     * 
     * @param resPartner 合作伙伴
     * @return 结果
     */
    public int insertResPartner(ResPartner resPartner);

    /**
     * 修改合作伙伴
     * 
     * @param resPartner 合作伙伴
     * @return 结果
     */
    public int updateResPartner(ResPartner resPartner);

    /**
     * 删除合作伙伴
     * 
     * @param id 合作伙伴主键
     * @return 结果
     */
    public int deleteResPartnerById(Long id);

    /**
     * 批量删除合作伙伴
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResPartnerByIds(Long[] ids);

    List<ResPartner> selectContactsInfoByResPartnerId(Long id);
}
