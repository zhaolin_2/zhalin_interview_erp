package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockMove;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存收发Mapper接口
 * 
 * @author Lin
 * @date 2023-05-14
 */
@Mapper
public interface StockMoveMapper 
{
    /**
     * 查询库存收发
     * 
     * @param id 库存收发主键
     * @return 库存收发
     */
    public StockMove selectStockMoveById(Long id);

    /**
     * 查询库存收发列表
     * 
     * @param stockMove 库存收发
     * @return 库存收发集合
     */
    public List<StockMove> selectStockMoveList(StockMove stockMove);

    /**
     * 新增库存收发
     * 
     * @param stockMove 库存收发
     * @return 结果
     */
    public int insertStockMove(StockMove stockMove);

    /**
     * 修改库存收发
     * 
     * @param stockMove 库存收发
     * @return 结果
     */
    public int updateStockMove(StockMove stockMove);

    /**
     * 删除库存收发
     * 
     * @param id 库存收发主键
     * @return 结果
     */
    public int deleteStockMoveById(Long id);

    /**
     * 批量删除库存收发
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockMoveByIds(Long[] ids);

    List<StockMove> selectStockMoveByPickingId(Long pickingId);
}
