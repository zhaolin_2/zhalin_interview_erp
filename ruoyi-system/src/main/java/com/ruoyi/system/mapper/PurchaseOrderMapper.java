package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购Mapper接口
 * 
 * @author Lin
 * @date 2023-04-06
 */
@Mapper
public interface PurchaseOrderMapper 
{
    /**
     * 查询采购
     * 
     * @param id 采购主键
     * @return 采购
     */
    public PurchaseOrder selectPurchaseOrderById(Long id);

    /**
     * 查询采购列表
     * 
     * @param purchaseOrder 采购
     * @return 采购集合
     */
    public List<PurchaseOrder> selectPurchaseOrderList(PurchaseOrder purchaseOrder);

    /**
     * 新增采购
     * 
     * @param purchaseOrder 采购
     * @return 结果
     */
    public int insertPurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 修改采购
     * 
     * @param purchaseOrder 采购
     * @return 结果
     */
    public int updatePurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 删除采购
     * 
     * @param id 采购主键
     * @return 结果
     */
    public int deletePurchaseOrderById(Long id);

    /**
     * 批量删除采购
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchaseOrderByIds(Long[] ids);

    PurchaseOrder selectPurchaseOrderByName(String purhaseOrderName);
}
