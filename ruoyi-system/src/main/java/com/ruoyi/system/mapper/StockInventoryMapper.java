package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.stock.StockInventory;
import org.apache.ibatis.annotations.Mapper;

/**
 * InventoryMapper接口
 * 
 * @author Lin
 * @date 2023-05-31
 */
@Mapper
public interface StockInventoryMapper 
{
    /**
     * 查询Inventory
     * 
     * @param id Inventory主键
     * @return Inventory
     */
    public StockInventory selectStockInventoryById(Long id);

    /**
     * 查询Inventory列表
     * 
     * @param stockInventory Inventory
     * @return Inventory集合
     */
    public List<StockInventory> selectStockInventoryList(StockInventory stockInventory);

    /**
     * 新增Inventory
     * 
     * @param stockInventory Inventory
     * @return 结果
     */
    public int insertStockInventory(StockInventory stockInventory);

    /**
     * 修改Inventory
     * 
     * @param stockInventory Inventory
     * @return 结果
     */
    public int updateStockInventory(StockInventory stockInventory);

    /**
     * 删除Inventory
     * 
     * @param id Inventory主键
     * @return 结果
     */
    public int deleteStockInventoryById(Long id);

    /**
     * 批量删除Inventory
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockInventoryByIds(Long[] ids);


    public StockInventory selectStockInventoryByOrderId(Long saleOrderId);

    StockInventory selectStockInventoryByOrigin(String origin);
}
