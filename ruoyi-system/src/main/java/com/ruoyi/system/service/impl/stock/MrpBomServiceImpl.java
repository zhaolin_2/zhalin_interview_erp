package com.ruoyi.system.service.impl.stock;

import java.util.*;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.mapper.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.service.IMrpBomService;

/**
 * 物料清单Service业务层处理
 *
 * @author Lin
 * @date 2023-04-18
 */
@Service
public class MrpBomServiceImpl implements IMrpBomService {
    @Autowired
    private MrpBomMapper mrpBomMapper;

    @Autowired
    private MrpBomPartMapper mrpBomPartMapper;

    @Autowired
    private StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    @Autowired
    private StockQuantMapper stockQuantMapper;

    @Autowired
    private MrpBomMsgMapper bomMsgMapper;

    @Autowired
    private ProductUomMapper productUomMapper;

    private static Map<Long, MrpBom> INSERT_BOM_TEMP = new HashMap<>();

    private static Map<Long, MrpBom> UPDATE_BOM_TEMP = new HashMap<>();

    /**
     * 查询物料清单以及包含的子组件
     *
     * @param id 物料清单主键
     * @return 物料清单
     */
    @Override
    public MrpBom selectMrpBomById(Long id) {
        MrpBom mrpBom = mrpBomMapper.selectMrpBomById(id);
        List<MrpBomPart> mrpBomParts;
        try {
            mrpBomParts = mrpBomPartMapper.selectMrpBomPartByBomId(id);
            for (int i = 0; i < mrpBomParts.size(); i++) {
                MrpBomPart mrpBomPart =  mrpBomParts.get(i);
                StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(mrpBomPart.getProductId());
                StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
                mrpBomPart.setDisplayName(stockProductTemplate.getName());
                mrpBomPart.setProductTemplateId(stockProductTemplate.getId());

            }
        } catch (NullPointerException e) {
            mrpBomParts = new ArrayList<>();
        }
        mrpBom.setMrpBomParts(mrpBomParts);
        UPDATE_BOM_TEMP.put(SecurityUtils.getUserId(),mrpBom);
        return mrpBom;
    }

    /**
     * 查询物料清单列表
     *
     * @param mrpBom 物料清单
     * @return 物料清单
     */
    @Override
    public List<MrpBom> selectMrpBomList(MrpBom mrpBom) {
        return mrpBomMapper.selectMrpBomList(mrpBom);
    }

    /**
     * 新增物料清单
     *
     * @param mrpBom 物料清单
     * @return 结果
     */
    @Override
    public int insertMrpBom(MrpBom mrpBom) {
        //写入对象到数据库，并且删除暂存在map内的内容
        mrpBom.setCreateDate(new Date());
        mrpBom.setCreateUid(SecurityUtils.getUserId());
        mrpBomMapper.insertMrpBom(mrpBom);
        List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
        for (int i = 0; i < mrpBomParts.size(); i++) {
            MrpBomPart mrpBomPart = mrpBomParts.get(i);
            mrpBomPart.setBomId(mrpBom.getId());
            mrpBomPartMapper.insertMrpBomPart(mrpBomPart);
        }
        INSERT_BOM_TEMP.remove(SecurityUtils.getUserId());
        return 1;
    }

    /**
     * 修改物料清单
     *
     * @param mrpBom 物料清单
     * @return 结果
     */
    @Override
    public int updateMrpBom(MrpBom mrpBom) {
        List<MrpBomPart> mrpBomPartOld = mrpBomPartMapper.selectMrpBomPartByBomId(mrpBom.getId());
        for (MrpBomPart mrpBomPart : mrpBomPartOld) {
            mrpBomPartMapper.deleteMrpBomPartById(mrpBomPart.getId());
        }
        mrpBom.setWriteDate(new Date());
        mrpBom.setWriteUid(SecurityUtils.getUserId());
        mrpBomMapper.updateMrpBom(mrpBom);
        List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
        int num = 0;
        for (int i = 0; i < mrpBomParts.size(); i++) {
            MrpBomPart mrpBomPart = mrpBomParts.get(i);
            mrpBomPart.setBomId(mrpBom.getId());
            num += mrpBomPartMapper.insertMrpBomPart(mrpBomPart);
        }
        UPDATE_BOM_TEMP.remove(SecurityUtils.getUserId());
        return num;
    }

    /**
     * 批量删除物料清单
     *
     * @param ids 需要删除的物料清单主键
     * @return 结果
     */
    @Override
    public int deleteMrpBomByIds(Long[] ids) {
        for (Long id : ids) {
            deleteMrpBomById(id);
        }
        return ids.length;
    }

    /**
     * 删除物料清单信息
     *
     * @param id 物料清单主键
     * @return 结果
     */
    @Override
    public int deleteMrpBomById(Long id) {
        List<MrpBomPart> mrpBomParts = mrpBomPartMapper.selectMrpBomPartByBomId(id);
        for (int i = 0; i < mrpBomParts.size(); i++) {
            MrpBomPart mrpBomPart = mrpBomParts.get(i);
            mrpBomPartMapper.deleteMrpBomPartById(mrpBomPart.getId());
        }
        return mrpBomMapper.deleteMrpBomById(id);
    }

    /**
     * 根据bomid 查询bom组件 并且通过库存，查询stock_product_template
     * 查回库存产品名字与剩余库存（关联表：stock_quant）
     *
     * @param bomId
     * @return
     */
    @Override
    public List<MrpBomPart> selectMrpBomPartByBomId(Long bomId) {
        List<MrpBomPart> mrpBomParts = mrpBomPartMapper.selectMrpBomPartByBomId(bomId);
        for (int i = 0; i < mrpBomParts.size(); i++) {
            MrpBomPart mrpBomPart = mrpBomParts.get(i);
            //查询关联表：stock_product_template
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(mrpBomPart.getProductId());
            //查出对应的库存产品
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
            //查询对应的库存数量
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockProductEntity.getId());
            //回填库存产品名称
            mrpBomPart.setDisplayName(stockProductTemplate.getName());
            ProductUom productUom = productUomMapper.selectProductUomById(mrpBomPart.getProductUomId());
            mrpBomPart.setProductUomName(productUom.getName());
            mrpBomPart.setProductTemplateId(stockProductTemplate.getId());
            if (stockQuant == null) {
                mrpBomPart.setStockNum(0l);
            } else {
                mrpBomPart.setStockNum(stockQuant.getQuantity());
            }
        }
        return mrpBomParts;
    }

    @Override
    public List<MrpBomMsg> selectMrpBomMsgByBomId(Long bomId) {
        return bomMsgMapper.selectMrpBomMsgByBomId(bomId);
    }

    @Override
    public MrpBom selectMrpBomByStockTemplateId(Long stockId) {
        MrpBom mrpBom = mrpBomMapper.selectMrpBomByStockTemplateId(stockId);
        if (mrpBom!=null){
            List<MrpBomPart> mrpBomParts = mrpBomPartMapper.selectMrpBomPartByBomId(mrpBom.getId());
            mrpBom.setMrpBomParts(mrpBomParts);
        }
        return mrpBom;
    }


    @Override
    public MrpBom writeMrpBom(MrpBom mrpBom) throws IllegalAccessException {
        //传入订单没有id 则为新增
        if (mrpBom.getId() == null) {
            com.ruoyi.common.utils.bean.BeanUtils beanUtils = new com.ruoyi.common.utils.bean.BeanUtils();
            /**
             * 点击新增：传入内容为空，且map中的值为空，则为完全新增，返回空值提供给用户填写
             * */
            if (beanUtils.checkFieldAllNull(mrpBom) && !INSERT_BOM_TEMP.containsKey(SecurityUtils.getUserId())) {
                INSERT_BOM_TEMP.put(SecurityUtils.getUserId(), new MrpBom());
                return new MrpBom();
            }
            /**
             * 点击新增：传入内容为空，但map中的值不为空，则说明用户上一次中断了填写，返回上次中断前的内容提供给用户填写
             */
            if (beanUtils.checkFieldAllNull(mrpBom) && INSERT_BOM_TEMP.containsKey(SecurityUtils.getUserId())) {
                return INSERT_BOM_TEMP.get(SecurityUtils.getUserId());
            }

            //说明是第一次插入订单行，设置订单组件的序列
            if (INSERT_BOM_TEMP.get(SecurityUtils.getUserId()) == null) {
                //设置订单创建时间
                mrpBom.setWriteUid(SecurityUtils.getUserId());
                mrpBom.setWriteDate(new Date());
                mrpBom.setCreateUid(SecurityUtils.getUserId());
                mrpBom.setCreateDate(new Date());
                mrpBom.setActive("t");
                mrpBom.setProductUomId(2l);
                mrpBom.setType("normal");
            }
            MrpBom mrpBomChecked = checkMrpBom(mrpBom);
            //TODO 在这里进行其他项目的更新
            INSERT_BOM_TEMP.put(SecurityUtils.getUserId(), mrpBomChecked);
            return mrpBomChecked;
        }
        if (mrpBom.getId() != null) {
            //传入订单包含id 则为修改接口
            MrpBom mrpBomChecked = checkMrpBom(mrpBom);
            UPDATE_BOM_TEMP.put(SecurityUtils.getUserId(), mrpBomChecked);
            return mrpBomChecked;
        }
        return new MrpBom();
    }

    /**
     * 订单报价单-订单组件详情返回
     *
     * @param sequence 需要操作的订单行的序列
     * @return 要操作的订单行
     */
    @Override
    public MrpBomPart bomPartInfo(Long sequence, String isInsert) {
        if (isInsert.equals("t")) {
            /**
             * 当处于更新页面的时候：
             */
            MrpBom mrpBom = INSERT_BOM_TEMP.get(SecurityUtils.getUserId());
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (MrpBomPart mrpBomPart : mrpBomParts) {
                if (mrpBomPart.getSequence().equals(sequence)) {
                    return mrpBomPart;
                }
            }
        } else if (isInsert.equals("f")) {
            /**
             * 当处于更新页面的时候：
             */
            MrpBom mrpBom = UPDATE_BOM_TEMP.get(SecurityUtils.getUserId());
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (MrpBomPart mrpBomPart : mrpBomParts) {
                if (mrpBomPart.getSequence().equals(sequence)) {
                    return mrpBomPart;
                }
            }
        }
        return new MrpBomPart();
    }

    /**
     * 订单组件的更新
     *
     * @param mrpBomPartNew 修改的订单组件
     *                      查询序列一致的，将新的组件替换旧组件，并且核算其他内容，将最新的订单存入map 并且返回
     * @return 修改后的销售订单
     */
    @Override
    public MrpBom mrpBomPartUpdate(MrpBomPart mrpBomPartNew) {
        if (mrpBomPartNew.getBomId() == null) {
            MrpBom mrpBom = INSERT_BOM_TEMP.get(SecurityUtils.getUserId());
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (int i = 0; i < mrpBomParts.size(); i++) {
                MrpBomPart mrpBomPart = mrpBomParts.get(i);
                if (mrpBomPart.getSequence().equals(mrpBomPartNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(mrpBomPartNew, mrpBomPart);
                }
            }
            mrpBom.setMrpBomParts(mrpBomParts);
            MrpBom mrpBomChecked = checkMrpBom(mrpBom);
            INSERT_BOM_TEMP.put(SecurityUtils.getUserId(), mrpBomChecked);
            return mrpBomChecked;
        }
        if (mrpBomPartNew.getBomId() != null) {
            MrpBom mrpBom = UPDATE_BOM_TEMP.get(SecurityUtils.getUserId());
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (int i = 0; i < mrpBomParts.size(); i++) {
                MrpBomPart mrpBomPart = mrpBomParts.get(i);
                if (mrpBomPart.getSequence().equals(mrpBomPartNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(mrpBomPartNew, mrpBomPart);
                }
            }
            mrpBom.setMrpBomParts(mrpBomParts);
            MrpBom mrpBomChecked = checkMrpBom(mrpBom);
            UPDATE_BOM_TEMP.put(SecurityUtils.getUserId(), mrpBomChecked);
            return mrpBomChecked;
        }
        return new MrpBom();
    }

    /**
     * 根据订单序列（实际上是订单插入数据库前的唯一表示） 删除订单组件
     *
     * @param sequence
     * @return
     */
    @Override
    public MrpBom bomPartDelete(Long sequence, String isInsert) {
        if (isInsert.equals("t")) {
            MrpBom mrpBom = INSERT_BOM_TEMP.get(SecurityUtils.getUserId());
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (MrpBomPart mrpBomPart : mrpBomParts) {
                if (mrpBomPart.getSequence().equals(sequence)) {
                    mrpBomParts.remove(mrpBomPart);
                    break;
                }
            }
            mrpBom.setMrpBomParts(mrpBomParts);
            MrpBom mrpBomChecked = checkMrpBom(mrpBom);
            //TODO 这里别忘了核算总价之后再返回回去
            INSERT_BOM_TEMP.put(SecurityUtils.getUserId(), mrpBomChecked);
            return mrpBomChecked;
        }
        if (isInsert.equals("f")) {
            MrpBom mrpBom = UPDATE_BOM_TEMP.get(SecurityUtils.getUserId());
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (MrpBomPart mrpBomPart : mrpBomParts) {
                if (mrpBomPart.getSequence().equals(sequence)) {
                    mrpBomParts.remove(mrpBomPart);
                    break;
                }
            }
            mrpBom.setMrpBomParts(mrpBomParts);
            MrpBom mrpBomChecked = checkMrpBom(mrpBom);
            //TODO 这里别忘了核算总价之后再返回回去
            UPDATE_BOM_TEMP.put(SecurityUtils.getUserId(), mrpBomChecked);
            return mrpBomChecked;
        }
        return new MrpBom();
    }


    /**
     * 将库存产品信息处理成物料清单组件返回
     */
    @Override
    public MrpBomPart getMrpBomPartPartFromStockProductTemplateId(Long stockId) {

        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockId);
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockProductEntity.getId());
        //构造全新的订单组件
        MrpBomPart mrpBomPart = new MrpBomPart();
        mrpBomPart.setProductId(stockProductEntity.getId());
        if (stockQuant!=null){
            //stockNum是库存数量
            mrpBomPart.setStockNum(stockQuant.getQuantity());
        }else {
            mrpBomPart.setStockNum(0l);
        }
        mrpBomPart.setProductUomId(stockProductTemplate.getUomId());
        mrpBomPart.setProductUomName("件");
        mrpBomPart.setRoutingId(73l);
        mrpBomPart.setCreateDate(new Date());
        mrpBomPart.setCreateUid(SecurityUtils.getUserId());
        mrpBomPart.setWriteDate(new Date());
        mrpBomPart.setWriteUid(SecurityUtils.getUserId());
        mrpBomPart.setDisplayName(stockProductTemplate.getName());
        return mrpBomPart;
    }

    @Override
    public String bomUpdateAbandon() {
        INSERT_BOM_TEMP.remove(SecurityUtils.getUserId());
        return "新增已放弃";
    }


    /**
     * 给mrpBomParts设置序列 以及 其他更新信息
     *
     * @param mrpBom
     * @return
     */
    public MrpBom checkMrpBom(MrpBom mrpBom) {
        if (mrpBom.getId() == null){
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (int i = 0; i < mrpBomParts.size(); i++) {
                MrpBomPart mrpBomPart = mrpBomParts.get(i);
                mrpBomPart.setSequence(Long.valueOf(i) + 1);
                mrpBomPart.setWriteDate(new Date());
                StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(mrpBomPart.getProductId());
                StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
                mrpBomPart.setDisplayName(stockProductTemplate.getName());
                mrpBomPart.setProductTemplateId(stockProductTemplate.getId());
            }
            mrpBom.setMrpBomParts(mrpBomParts);
            mrpBom.setProductQty(1l);
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(mrpBom.getProductTmplId());
            mrpBom.setCode(stockProductTemplate.getName());
            mrpBom.setActive("t");
            mrpBom.setType("normal");
            return mrpBom;
        }
        if (mrpBom.getId() != null){
            List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
            for (int i = 0; i < mrpBomParts.size(); i++) {
                MrpBomPart mrpBomPart = mrpBomParts.get(i);
                mrpBomPart.setSequence(Long.valueOf(i) + 1);
                mrpBomPart.setWriteDate(new Date());
                StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(mrpBomPart.getProductId());
                StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
                mrpBomPart.setDisplayName(stockProductTemplate.getName());
                mrpBomPart.setProductTemplateId(stockProductTemplate.getId());
                mrpBomPart.setBomId(mrpBom.getId());
            }
            mrpBom.setMrpBomParts(mrpBomParts);
            mrpBom.setProductQty(1l);
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(mrpBom.getProductTmplId());
            mrpBom.setCode(stockProductTemplate.getName());
            mrpBom.setActive("t");
            mrpBom.setType("normal");
            return mrpBom;
        }
        return new MrpBom();
        }

    /**
     * Bom表初始化处理
     */
    @Override
    public void bomNameAndProductIdHandle(){
        List<MrpBom> mrpBoms = mrpBomMapper.selectMrpBomList(new MrpBom());
        for (int i = 0; i < mrpBoms.size(); i++) {
            MrpBom mrpBom =  mrpBoms.get(i);
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(mrpBom.getProductTmplId());
            mrpBom.setCode(stockProductTemplate.getName());
            mrpBomMapper.updateMrpBom(mrpBom);
        }
    }

}
