package com.ruoyi.system.service.impl.sale;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.impl.purchase.PurchaseOrderServiceImpl;
import com.ruoyi.system.service.impl.stock.StockPickingServiceImpl;
import com.ruoyi.system.service.impl.stock.StockProductTemplateServiceImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.service.ISaleOrderService;


/**
 * 销售订单Service业务层处理
 *
 * @author Lin
 * @date 2023-04-04
 */
@Service
public class SaleOrderServiceImpl implements ISaleOrderService {
    @Autowired
    private SaleOrderMapper saleOrderMapper;

    @Autowired
    private SaleOrderPartMapper saleOrderPartMapper;

    @Autowired
    private StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    @Autowired
    private ResPartnerMapper resPartnerMapper;

    @Autowired
    StockQuantMapper stockQuantMapper;

    @Autowired
    SysUserMapper sysUserMapper;

    @Autowired
    private StockReorderRuleMapper stockReorderRuleMapper;

    @Autowired
    private StockProductSupplierinfoMapper stockProductSupplierinfoMapper;


    /**
     * 在这里准备一个map
     * 当订单创建的时候，请求接口：
     * 首先程序创建一个全空SaleOrder对象，存入map，key为操作者的uid value为该操作者正在新建的对象（该做法保证在多人同时进行相同操作的时候数据的归属正常）
     * 每次添加（或者更改）完SaleOrderPart时 请求一次该接口 该接口会保存操作者操作当前的最新状态并且返回
     * 确认保存后：清除当前的key的map值
     * 何时触发这个接口：保存/修改订单行
     */
    private static Map<Long, SaleOrder> INSERT_SALE_ORDERS_TEMP = new HashMap<>();

    private static Map<Long, SaleOrder> UPDATE_SALE_ORDERS_TEMP = new HashMap<>();

    /**
     * 查询销售订单
     *
     * @param id 销售订单主键
     * @return 销售订单
     */
    @Override
    public SaleOrder selectSaleOrderById(Long id) {
        SaleOrder saleOrder = saleOrderMapper.selectSaleOrderById(id);
        try {
            //将订单的组件部分同步查出
            List<SaleOrderPart> saleOrderParts = saleOrderPartMapper.selectSaleOrderPartBySaleOrderId(id);
            for (int i = 0; i < saleOrderParts.size(); i++) {
                SaleOrderPart saleOrderPart = saleOrderParts.get(i);
                saleOrderPart.setSequence(Long.valueOf(i) + 1);
                saleOrderPart.setProductTemplateId(stockProductEntityMapper.selectStockProductEntityById(saleOrderPart.getProductId()).getProductTmplId());
                StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(saleOrderPart.getProductId());
                if (stockQuant != null) {
                    saleOrderPart.setStockNum(stockQuant.getQuantity());
                } else {
                    saleOrderPart.setStockNum(0l);
                }
            }
            saleOrder.setSaleOrderParts(saleOrderParts);
        } catch (NullPointerException e) {
            saleOrder.setSaleOrderParts(null);
        }
        saleOrderTranslation(saleOrder);
//        UPDATE_SALE_ORDERS_TEMP.put(SecurityUtils.getUserId(), saleOrder);
        return saleOrder;
    }

    /**
     * 查询销售订单列表
     *
     * @param saleOrder 销售订单
     * @return 销售订单
     */
    @Override
    public List<SaleOrder> selectSaleOrderList(SaleOrder saleOrder) {

        return saleOrderMapper.selectSaleOrderList(saleOrder);
    }

    /**
     * 销售订单编辑：根据业务 销售订单的编辑与提交分为两个接口：在用户新增订单组件时，请求的是订单编辑的接口
     * 在订单提交时，请求的是订单新增的接口，区别：是否写入数据库
     *
     * @param saleOrder 销售订单
     * @return 结果
     */
    @Override
    public SaleOrder writeSaleOrder(SaleOrder saleOrder) throws IllegalAccessException {
        //传入订单没有id 则为新增
        if (saleOrder.getId() == null) {
            //现在我想不光是保存订单组件的时候返回上次保存的内容，现在我需要用户点击新增订单的时候也能返回上次没有编辑完成的内容
            //以上做法目的是：代替草稿的作用
            com.ruoyi.common.utils.bean.BeanUtils beanUtils = new com.ruoyi.common.utils.bean.BeanUtils();
            /**
             * 点击新增：传入内容为空，且map中的值为空，则为完全新增，返回空值提供给用户填写
             */
            if (com.ruoyi.common.utils.bean.BeanUtils.checkFieldAllNull(saleOrder) && !INSERT_SALE_ORDERS_TEMP.containsKey(SecurityUtils.getUserId())) {
                return new SaleOrder();
            }
            /**
             * 点击新增：传入内容为空，但map中的值不为空，则说明用户上一次中断了填写，返回上次中断前的内容提供给用户填写
             */
            if (com.ruoyi.common.utils.bean.BeanUtils.checkFieldAllNull(saleOrder) && INSERT_SALE_ORDERS_TEMP.containsKey(SecurityUtils.getUserId())) {
                System.out.println(INSERT_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId()));
                return INSERT_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            }
            /**
             * 插入订单行确认按钮
             *传入数据不为空（），则说明用户是通过页面内提交订单组件按钮请求接口，保存用户传入的数据，并且返回给前端 提供用户编写
             * 第一次插入 map内什么都没有 直接写入处理后写入mao
             * 新增则追加
             * 修改应该如何处理（需要注意修改不要影响到新增）：
             * 现在map里是最新的 传回去的也是最新的 点击修改订单行：
             * 请求修改接口：传入序列 我将该序列对应的订单组件回传
             * 用户编辑
             * 点击这个write接口：
             * 前端
             */
            //说明是第一次插入订单行，设置订单组件的序列
            if (INSERT_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId()) == null) {
                //设置订单创建时间
                saleOrder.setDateOrder(new Date());
                saleOrder.setCreateDate(new Date());
            }
            System.out.println(saleOrder);
            SaleOrder saleOrderChecked = checkSaleOrderPrice(saleOrder);
            INSERT_SALE_ORDERS_TEMP.put(SecurityUtils.getUserId(), saleOrderChecked);
            return INSERT_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
        }
        if (saleOrder.getId() != null) {
            //传入订单包含id 则为修改接口
            SaleOrder saleOrderUpdateChecked = checkSaleOrderPrice(saleOrder);
            UPDATE_SALE_ORDERS_TEMP.put(SecurityUtils.getUserId(), saleOrderUpdateChecked);
            return saleOrderUpdateChecked;
        }
        return new SaleOrder();
    }


    /**
     * 新增销售订单
     *
     * @param saleOrder 销售订单
     * @return 结果
     */
    @Override
    public int insertSaleOrder(SaleOrder saleOrder) {
        //写入对象到数据库，并且删除暂存在map内的内容
        saleOrder.setDateOrder(new Date());
        saleOrder.setState("draft");
        saleOrder.setCreateDate(new Date());
        saleOrder.setCreateUid(SecurityUtils.getUserId());
        //date_order是订购日期
        saleOrder.setDateOrder(new Date());
        saleOrder.setUserId(SecurityUtils.getUserId().intValue());
        saleOrder.setName(orderNameGenerator(saleOrder.getPartnerId().longValue()));
        //差一个合同号 和 工作指令单号
        saleOrder.setInstructionOrder(saleOrderInstructionOrderGenerator());
        saleOrder.setContractNo(saleOrderContracNoGenerator(saleOrder.getName()));
        if (saleOrder.getIsTax().equals("t")) {
            saleOrder.setAmountTax(saleOrder.getAmountTotal().doubleValue());
        } else {
            saleOrder.setAmountUntaxed(saleOrder.getAmountTotal().doubleValue());
        }
        saleOrderMapper.insertSaleOrder(saleOrder);
        List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
        for (int i = 0; i < saleOrderParts.size(); i++) {
            SaleOrderPart saleOrderPart = saleOrderParts.get(i);
            saleOrderPart.setOrderId(saleOrder.getId());
            saleOrderPartMapper.insertSaleOrderPart(saleOrderPart);
        }
        //TODO :库存预留
        //TODO : 检查库存
        INSERT_SALE_ORDERS_TEMP.remove(SecurityUtils.getUserId());
        return 1;
    }


    /**
     * 销售订单-写入后 修改更新
     * 需要进行订单详情 与 订单组件的同步修改
     *
     * @param saleOrder 销售订单
     * @return 结果
     */
    @Override
    public int updateSaleOrder(SaleOrder saleOrder) {
        if (saleOrder.getState().equals("sale") || saleOrder.getState().equals("cancel")) {
            throw new RuntimeException("订单状态不符，无法修改");
        }
        List<SaleOrderPart> saleOrderPartsOld = saleOrderPartMapper.selectSaleOrderPartBySaleOrderId(saleOrder.getId());
        for (int i = 0; i < saleOrderPartsOld.size(); i++) {
            SaleOrderPart saleOrderPart = saleOrderPartsOld.get(i);
            saleOrderPartMapper.deleteSaleOrderPartById(saleOrderPart.getId());
        }
        saleOrder.setWriteDate(new Date());
        saleOrder.setWriteUid(SecurityUtils.getUserId());
        saleOrderMapper.updateSaleOrder(saleOrder);
        List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
        int num = 0;
        for (int i = 0; i < saleOrderParts.size(); i++) {
            SaleOrderPart saleOrderPart = saleOrderParts.get(i);
            saleOrderPart.setOrderId(saleOrder.getId());
            num += saleOrderPartMapper.insertSaleOrderPart(saleOrderPart);
        }
        UPDATE_SALE_ORDERS_TEMP.remove(SecurityUtils.getUserId());
        return num;
    }

    /**
     * 批量删除销售订单
     *
     * @param ids 需要删除的销售订单主键
     * @return 结果
     */
    @Override
    public int deleteSaleOrderByIds(Long[] ids) {
        for (Long id : ids) {
            deleteSaleOrderById(id);
        }
        return ids.length;
    }

    /**
     * 删除销售订单信息
     *
     * @param id 销售订单主键
     * @return 结果
     */
    @Override
    public int deleteSaleOrderById(Long id) {
        List<SaleOrderPart> saleOrderParts = saleOrderPartMapper.selectSaleOrderPartBySaleOrderId(id);
        for (int i = 0; i < saleOrderParts.size(); i++) {
            SaleOrderPart saleOrderPart = saleOrderParts.get(i);
            saleOrderPartMapper.deleteSaleOrderPartById(saleOrderPart.getId());
        }
        return saleOrderMapper.deleteSaleOrderById(id);
    }

    @Override
    public List<SaleOrderPart> GetOrderPartMsg(Long orderId) {
        List<SaleOrderPart> saleOrderParts = saleOrderPartMapper.selectSaleOrderPartBySaleOrderId(orderId);
        for (int i = 0; i < saleOrderParts.size(); i++) {
            SaleOrderPart saleOrderPart = saleOrderParts.get(i);
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(saleOrderPart.getProductId());
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(saleOrderPart.getProductId());
            if (stockQuant != null) {
                saleOrderPart.setStockNum(stockQuant.getQuantity());
            } else {
                saleOrderPart.setStockNum(0l);
            }
            saleOrderPart.setProductName(stockProductTemplate.getName());
        }
        return saleOrderParts;
    }

    /**
     * 根据库存产品id 重组订单组件对象返回前端
     *
     * @param stockId
     * @return
     */
    @Override
    public SaleOrderPart getSaleOrderPartFromStockProductTemplateId(Long stockId) {
        //根据库存产品模板id 查询库存模板产品
        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockId);
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        //构造全新的订单组件
        SaleOrderPart saleOrderPart = new SaleOrderPart();
        saleOrderPart.setPriceUnit(stockProductTemplate.getListPrice());
        saleOrderPart.setProductUomQty(1l);
        saleOrderPart.setName(stockProductTemplate.getName());
        saleOrderPart.setProductId(stockProductEntity.getId());
        saleOrderPart.setProductUom(stockProductTemplate.getUomId());
        saleOrderPart.setSalesmanId(SecurityUtils.getUserId());
        saleOrderPart.setCreateDate(new Date());
        saleOrderPart.setCreateUid(SecurityUtils.getUserId());
        saleOrderPart.setPriceSubtotal(saleOrderPart.getPriceUnit().multiply(BigDecimal.valueOf(saleOrderPart.getProductUomQty())));
        saleOrderPart.setWorldchampCurrency("cny");
        saleOrderPart.setWorldchampTaxId(1l);
        saleOrderPart.setPartMessage(stockProductTemplate.getSpecification());
        StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockProductEntity.getId());
        if (stockQuant != null) {
            saleOrderPart.setStockNum(stockQuant.getQuantity());
        } else {
            saleOrderPart.setStockNum(0l);
        }
        return saleOrderPart;
    }


    /**
     * 订单报价单-订单组件详情返回
     *
     * @param sequence 需要操作的订单行的序列
     * @return 要操作的订单行
     */
    @Override
    public SaleOrderPart saleOrderPartInfo(Long sequence, String isInsert) {
        if (isInsert.equals("t")) {
            /**
             * 当处于新增页面的时候：
             */
            SaleOrder saleOrder = INSERT_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
            for (SaleOrderPart saleOrderPart : saleOrderParts) {
                if (saleOrderPart.getSequence().equals(sequence)) {
                    return saleOrderPart;
                }
            }
        }
        if (isInsert.equals("f")) {
            /**
             * 当处于更新页面的时候：
             */
            SaleOrder saleOrder = UPDATE_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
            for (SaleOrderPart saleOrderPart : saleOrderParts) {
                if (saleOrderPart.getSequence().equals(sequence)) {
                    return saleOrderPart;
                }
            }
        }
        return new SaleOrderPart();
    }

    /**
     * 订单报价单-订单组件编辑
     *
     * @param saleOrderPartNew 修改的订单组件
     *                         查询序列一致的，将新的组件替换旧组件，并且核算其他内容，将最新的订单存入map 并且返回
     * @return 修改后的销售订单
     */
    @Override
    public SaleOrder saleOrderPartUpdate(SaleOrderPart saleOrderPartNew) {
        //如果传入的订单组件没有orderId，说明改订单没有插入过数据库内，从INSERT_SALE_ORDERS_TEMP 中取值即可
        if (saleOrderPartNew.getOrderId() == null) {
            SaleOrder saleOrder = INSERT_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
            for (int i = 0; i < saleOrderParts.size(); i++) {
                SaleOrderPart orderPart = saleOrderParts.get(i);
                if (orderPart.getSequence().equals(saleOrderPartNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(saleOrderPartNew, orderPart);
                }
            }
            saleOrder.setSaleOrderParts(saleOrderParts);
            SaleOrder saleOrderChecked = checkSaleOrderPrice(saleOrder);
            INSERT_SALE_ORDERS_TEMP.put(SecurityUtils.getUserId(), saleOrderChecked);
            return saleOrderChecked;
        }
        if (saleOrderPartNew.getOrderId() != null) {
            SaleOrder saleOrderUpdate = UPDATE_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<SaleOrderPart> saleOrderParts = saleOrderUpdate.getSaleOrderParts();
            for (int i = 0; i < saleOrderParts.size(); i++) {
                SaleOrderPart orderPart = saleOrderParts.get(i);
                if (orderPart.getSequence().equals(saleOrderPartNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(saleOrderPartNew, orderPart);
                }
            }
            saleOrderUpdate.setSaleOrderParts(saleOrderParts);
            SaleOrder saleOrderChecked = checkSaleOrderPrice(saleOrderUpdate);
            UPDATE_SALE_ORDERS_TEMP.put(SecurityUtils.getUserId(), saleOrderChecked);
            return saleOrderChecked;
        }
        return new SaleOrder();
    }


    /**
     * 根据订单序列（实际上是订单插入数据库前的唯一表示） 删除订单组件
     *
     * @param sequence
     * @return
     */
    @Override
    public SaleOrder saleOrderPartDelete(Long sequence, String isInsert) {
        if (isInsert.equals("t")) {
            SaleOrder saleOrder = INSERT_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
            for (SaleOrderPart saleOrderPart : saleOrderParts) {
                if (saleOrderPart.getSequence().equals(sequence)) {
                    saleOrderParts.remove(saleOrderPart);
                    break;
                }
            }
            saleOrder.setSaleOrderParts(saleOrderParts);
            SaleOrder saleOrderChecked = checkSaleOrderPrice(saleOrder);
            INSERT_SALE_ORDERS_TEMP.put(SecurityUtils.getUserId(), saleOrderChecked);
            return saleOrderChecked;
        }
        if (isInsert.equals("f")) {
            SaleOrder saleOrderUpdate = UPDATE_SALE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<SaleOrderPart> updateSaleOrderParts = saleOrderUpdate.getSaleOrderParts();
            for (SaleOrderPart saleOrderPart : updateSaleOrderParts) {
                if (saleOrderPart.getSequence().equals(sequence)) {
                    updateSaleOrderParts.remove(saleOrderPart);
                    break;
                }
            }
            saleOrderUpdate.setSaleOrderParts(updateSaleOrderParts);
            SaleOrder saleOrderUpdateChecked = checkSaleOrderPrice(saleOrderUpdate);
            UPDATE_SALE_ORDERS_TEMP.put(SecurityUtils.getUserId(), saleOrderUpdateChecked);
            return saleOrderUpdateChecked;
        }
        return new SaleOrder();
    }

    @Override
    public String saleOrderWriteAbandon() {
        INSERT_SALE_ORDERS_TEMP.remove(SecurityUtils.getUserId());
        return "新增已放弃";
    }


    /**
     * 计算订单价格和给订单组件设置序列
     *
     * @param saleOrder
     * @return
     */
    public SaleOrder checkSaleOrderPrice(@NotNull SaleOrder saleOrder) {
        if (saleOrder.getId() == null) {
            List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
            BigDecimal price = new BigDecimal(0.00);
            StockQuantMapper beanQuant = SpringUtils.getBean(StockQuantMapper.class);

            for (int i = 0; i < saleOrderParts.size(); i++) {
                SaleOrderPart saleOrderPart = saleOrderParts.get(i);
                saleOrderPart.setSequence(Long.valueOf(i) + 1);
                StockQuant stockQuant = beanQuant.selectStockPhysicalInventoryByProductId(saleOrderPart.getProductId());
                if (stockQuant != null) {
                    saleOrderPart.setStockNum(stockQuant.getQuantity());
                } else {
                    saleOrderPart.setStockNum(0l);
                }
                StockProductEntityMapper beanEntity = SpringUtils.getBean(StockProductEntityMapper.class);
                StockProductEntity stockProductEntity = beanEntity.selectStockProductEntityById(saleOrderPart.getProductId());
                saleOrderPart.setProductTemplateId(stockProductEntity.getProductTmplId());
                saleOrderPart.setPriceSubtotal(saleOrderPart.getPriceUnit().multiply(BigDecimal.valueOf(saleOrderPart.getProductUomQty())));
                price = price.add(saleOrderPart.getPriceSubtotal());
            }
            saleOrder.setAmountTotal(price);
            saleOrder.setAmountTotalDaxie(Convert.digitToChinese(saleOrder.getAmountTotal()));
            saleOrder.setSaleOrderParts(saleOrderParts);
            saleOrder.setPartnerInvoiceId(saleOrder.getPartnerId());
            return saleOrder;
        }
        if (saleOrder.getId() != null) {
            List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
            StockQuantMapper beanQuant = SpringUtils.getBean(StockQuantMapper.class);
            BigDecimal price = new BigDecimal(0.00);
            for (int i = 0; i < saleOrderParts.size(); i++) {
                SaleOrderPart saleOrderPart = saleOrderParts.get(i);
                saleOrderPart.setSequence(Long.valueOf(i) + 1);
                saleOrderPart.setPriceSubtotal(saleOrderPart.getPriceUnit().multiply(BigDecimal.valueOf(saleOrderPart.getProductUomQty())));
                price = price.add(saleOrderPart.getPriceSubtotal());
                StockQuant stockQuant = beanQuant.selectStockPhysicalInventoryByProductId(saleOrderPart.getProductId());
                if (stockQuant != null) {
                    saleOrderPart.setStockNum(stockQuant.getQuantity());
                } else {
                    saleOrderPart.setStockNum(0l);
                }
                saleOrderPart.setOrderId(saleOrder.getId());
                StockProductEntityMapper beanEntity = SpringUtils.getBean(StockProductEntityMapper.class);
                StockProductEntity stockProductEntity = beanEntity.selectStockProductEntityById(saleOrderPart.getProductId());
                saleOrderPart.setProductTemplateId(stockProductEntity.getProductTmplId());
            }
            saleOrder.setAmountTotal(price);
            saleOrder.setAmountTotalDaxie(Convert.digitToChinese(saleOrder.getAmountTotal()));
            saleOrder.setSaleOrderParts(saleOrderParts);
            saleOrder.setWriteUid(SecurityUtils.getUserId());
            saleOrder.setState("draft");
            return saleOrder;
        }
        return new SaleOrder();
    }


    /**
     * 销售报价单 确认为销售订单 draft  to  sale
     * 1.轮询库存重订货规则，生成采购询价单
     * 2.生成出货单（IN/OUT中的OUT）
     *
     * @param id
     */
    @Override
    public void draftSaleOrderConfirmToSaleOrder(Long id) {
        try {
            //根据id查询出销售订单，需要包含内含的订单组件
            SaleOrder saleOrder = selectSaleOrderById(id);
            if (saleOrder.getState().equals("cancel") || saleOrder.getState().equals("sale")) {
                throw new RuntimeException("订单状态处于已取消或已经是销售订单");
            }
            saleOrder.setState("sale");
            saleOrder.setConfirmationDate(new Date());
            StockProductTemplateServiceImpl bean = SpringUtils.getBean(StockProductTemplateServiceImpl.class);
            StockPickingServiceImpl pick = SpringUtils.getBean(StockPickingServiceImpl.class);
            saleOrderMapper.updateSaleOrder(saleOrder);

            //TODO 预留 实际 可操作
            //生成出货管理（WH/OUT）
            pick.stockPickingGenerateBySaleOrder(saleOrder);

            //订单扣减库存计算
            Map<Long, Long> longLongMap = bean.takeSaleOrderApartProductNeeded(saleOrder.getId());

            //初始化一个map,key为供应商id value为该id下的产品
            Map<Long, Map<Long, Long>> productGroupedByPartner = new HashMap<>();
            //库存拟扣 处理重订货
            for (Long aLong : longLongMap.keySet()) {
                Long productId = aLong;
                StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(productId);
                stockQuant.setOperationalQuantity(stockQuant.getQuantity() - longLongMap.get(productId));
                stockQuant.setPredictiveQuantity(stockQuant.getQuantity() - longLongMap.get(productId));
                stockQuant.setWriteDate(new Date());
                stockQuant.setWriteUid(1l);
                stockQuantMapper.updateStockQuant(stockQuant);
                //查询产品重订货规则 如果有 则生成采购询价单  首先 需要把产品按照供应商进行分类
                List<StockProductSupplierinfo> stockProductSupplierinfos = stockProductSupplierinfoMapper.selectStockProductSupplierinfoByStockTmplId(productId);
                Long partnerId = stockProductSupplierinfos.get(0).getName().longValue();
                if (stockProductSupplierinfos.size() != 0) {
                    if (productGroupedByPartner.containsKey(partnerId)) {
                        Map<Long, Long> longLongMap1 = productGroupedByPartner.get(partnerId);
                        longLongMap1.put(productId, longLongMap.get(productId));
                        productGroupedByPartner.put(partnerId, longLongMap1);
                    } else {
                        Map<Long, Long> map = new HashMap<>();
                        map.put(productId, longLongMap.get(productId));
                        productGroupedByPartner.put(partnerId, map);
                    }
                }
            }
            //拿到分组后的productGroupedByPartner 并且轮询 重订货规则生成采购订单
            for (Long aLong : productGroupedByPartner.keySet()) {
                //一个是要查重订货规则  //另一个要查库存数量
                Long partnerId = aLong;
                PurchaseOrder purchaseOrder = new PurchaseOrder();
                purchaseOrder.setName(saleOrder.getName());
                purchaseOrder.setOrigin(saleOrder.getName());
                purchaseOrder.setPartnerId(partnerId);
                purchaseOrder.setPickingTypeId(3l);
                purchaseOrder.setCreateMen(saleOrder.getUserName());
                List<PurchaseOrderPart> purchaseOrderParts = new ArrayList<>();
                Map<Long, Long> productAndNumMap = productGroupedByPartner.get(partnerId);
                for (Long productId : productAndNumMap.keySet()) {
                    StockReorderRule stockReorderRule = stockReorderRuleMapper.selectStockReorderRuleByEntityId(productId);
                    if (stockReorderRule != null) {
                        PurchaseOrderPart purchaseOrderPart = new PurchaseOrderPart();
                        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(productId);
                        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
                        StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(productId);
                        purchaseOrderPart.setName(stockProductTemplate.getName());
                        purchaseOrderPart.setProductQty(stockReorderRule.getProductMaxQty() - stockQuant.getOperationalQuantity());
                        purchaseOrderPart.setDatePlanned(new Date());
                        purchaseOrderPart.setProductUom(stockProductTemplate.getUomId());
                        purchaseOrderPart.setProductId(productId);
                        purchaseOrderPart.setPriceUnit(new BigDecimal(1.0));
                        purchaseOrderPart.setPriceSubtotal(purchaseOrderPart.getPriceUnit().multiply(BigDecimal.valueOf(purchaseOrderPart.getProductQty())));
                        purchaseOrderPart.setState("draft");
                        purchaseOrderPart.setQtyReceived(0l);
                        purchaseOrderPart.setPartnerId(partnerId);
                        purchaseOrderPart.setCurrencyId(8l);
                        purchaseOrderPart.setCreateDate(new Date());
                        purchaseOrderPart.setCreateUid(1l);
                        purchaseOrderParts.add(purchaseOrderPart);
                    }
                }
                if (purchaseOrder.getPurchaseOrderParts() != null) {
                    purchaseOrder.setPurchaseOrderParts(purchaseOrderParts);
                    PurchaseOrderServiceImpl purchaseOrderService = SpringUtils.getBean(PurchaseOrderServiceImpl.class);
                    PurchaseOrder purchaseOrderChecked = purchaseOrderService.checkPurchaseOrderPrice(purchaseOrder);
                    purchaseOrderService.insertPurchaseOrder(purchaseOrderChecked);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 报价单取消订单
     *
     * @param id
     */
    @Override
    public void saleOrderCancel(Long id) {
        SaleOrder saleOrder = saleOrderMapper.selectSaleOrderById(id);
        if (saleOrder.getState().equals("cancel") || saleOrder.getState().equals("sale")) {
            throw new RuntimeException("订单状态处于已取消或已经是销售订单");
        }
        saleOrder.setState("cancel");
        saleOrderMapper.updateSaleOrder(saleOrder);
    }

    /**
     * 销售订单转译英文数字
     */
    @Override
    public void saleOrderTranslation(SaleOrder saleOrder) {
        ResPartner resPartner = resPartnerMapper.selectResPartnerById(saleOrder.getPartnerId().longValue());
        if (resPartner != null) {
            saleOrder.setPartName(resPartner.getName());
        }
        SysUser sysUser = sysUserMapper.selectUserById(saleOrder.getCreateUid());
        saleOrder.setWriteUserName(sysUser.getNickName());
        saleOrder.setUserName(sysUser.getNickName());
        saleOrder.setPartnerInvoiceAddress(resPartner.getCity() + resPartner.getZip() + resPartner.getStreet());
        saleOrder.setPartnerShippingAddress(resPartner.getDeliveryAddress());
        saleOrder.setPartnerName(resPartner.getName());
    }


    /**
     * 该方法用于生成销售订单的订单名称
     * 生成规则如下：需要寻找该客户的信息：
     * 1.找到客户编码字段  如：QZ
     * 2.找到该客户的最后一个销售订单，查询其编号末尾数字 TODO:这里需要注意如果是第一个订单 无法取到末尾数字的情况
     * 3.在最后数字上进行递增+1操作
     *
     * @param partnerId
     * @return
     */
    @Override
    public String orderNameGenerator(Long partnerId) {
        SaleOrder saleOrder = saleOrderMapper.selectSaleOrderByPartnerId(partnerId);
        if (saleOrder == null) {
            //该情况说明 该客户此前没有任何订单
            ResPartner resPartner = resPartnerMapper.selectResPartnerById(partnerId);
            String customerCode = resPartner.getCustomerCode();
            return customerCode + 0001;
        }
        String lastOrderName = saleOrder.getName();
        int lastDigits = Integer.parseInt(lastOrderName.substring(lastOrderName.length() - 4));  // 获取最后四位数字并转换为整型
        int newDigits = lastDigits + 1;  // 进行自增加一操作
        String newOrderNumber = lastOrderName.substring(0, lastOrderName.length() - 4) + String.format("%04d", newDigits);  // 拼接生成新订单号
        return newOrderNumber;
    }

    /**
     * 生成订单生产指令单号的方法
     *
     * @return
     */
    @Override
    public String saleOrderInstructionOrderGenerator() {
        String year = String.valueOf(DateUtil.year(new Date()));
        SaleOrder saleOrder = saleOrderMapper.selectLastSaleOrder();
        String lastOrderContractNo = "";
        if (saleOrder==null){
            lastOrderContractNo = "000001";
        }else {
            saleOrder.getInstructionOrder();
        }
        String lastFour = lastOrderContractNo.substring(lastOrderContractNo.length() - 4); // 截取后四位
        int num = Integer.parseInt(lastFour) + 1; // 转换为数字并自增
        String result = year + "_" + String.format("%04d", num); // 格式化为四位数字字符串
        return result;
    }

    /**
     * 生成订单合同号的方法
     *
     * @return
     */
    @Override
    public String saleOrderContracNoGenerator(String saleOrderName) {
        String[] splitStr = saleOrderName.split("_"); // 将字符串按照'_'分割成数组
        String pcode = splitStr[0]; // 取出第一个元素作为pcode
        String pno = saleOrderName.substring(saleOrderName.length() - 4); // 取出最后四位作为pno
        return "MIC-" + pcode + "-" + pno;
    }

    @Override
    public SaleOrder selectSaleOrderBySaleOrderName(String saleOrderName) {
        SaleOrder saleOrder = saleOrderMapper.selectSaleOrderBySaleOrderName(saleOrderName);
        return selectSaleOrderById(saleOrder.getId());
    }


}
