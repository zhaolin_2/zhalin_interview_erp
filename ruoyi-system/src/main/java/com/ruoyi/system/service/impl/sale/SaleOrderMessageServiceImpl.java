package com.ruoyi.system.service.impl.sale;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SaleOrderMessageMapper;
import com.ruoyi.system.domain.sale.SaleOrderMessage;
import com.ruoyi.system.service.ISaleOrderMessageService;

/**
 * 采购订单操作日志Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-06
 */
@Service
public class SaleOrderMessageServiceImpl implements ISaleOrderMessageService 
{
    @Autowired
    private SaleOrderMessageMapper saleOrderMessageMapper;

    /**
     * 查询采购订单操作日志
     * 
     * @param id 采购订单操作日志主键
     * @return 采购订单操作日志
     */
    @Override
    public SaleOrderMessage selectSaleOrderMessageById(Long id)
    {
        return saleOrderMessageMapper.selectSaleOrderMessageById(id);
    }

    /**
     * 查询采购订单操作日志列表
     * 
     * @param saleOrderMessage 采购订单操作日志
     * @return 采购订单操作日志
     */
    @Override
    public List<SaleOrderMessage> selectSaleOrderMessageList(SaleOrderMessage saleOrderMessage)
    {
        return saleOrderMessageMapper.selectSaleOrderMessageList(saleOrderMessage);
    }

    /**
     * 新增采购订单操作日志
     * 
     * @param saleOrderMessage 采购订单操作日志
     * @return 结果
     */
    @Override
    public int insertSaleOrderMessage(SaleOrderMessage saleOrderMessage)
    {
        return saleOrderMessageMapper.insertSaleOrderMessage(saleOrderMessage);
    }

    /**
     * 修改采购订单操作日志
     * 
     * @param saleOrderMessage 采购订单操作日志
     * @return 结果
     */
    @Override
    public int updateSaleOrderMessage(SaleOrderMessage saleOrderMessage)
    {
        return saleOrderMessageMapper.updateSaleOrderMessage(saleOrderMessage);
    }

    /**
     * 批量删除采购订单操作日志
     * 
     * @param ids 需要删除的采购订单操作日志主键
     * @return 结果
     */
    @Override
    public int deleteSaleOrderMessageByIds(Long[] ids)
    {
        return saleOrderMessageMapper.deleteSaleOrderMessageByIds(ids);
    }

    /**
     * 删除采购订单操作日志信息
     * 
     * @param id 采购订单操作日志主键
     * @return 结果
     */
    @Override
    public int deleteSaleOrderMessageById(Long id)
    {
        return saleOrderMessageMapper.deleteSaleOrderMessageById(id);
    }

    /**
     * 根据订单id查询 销售订单详情
     * @param saleOrderId
     * @return
     */
    @Override
    public List<SaleOrderMessage> getSaleOrderMessageByOrderId(Long saleOrderId) {
        return saleOrderMessageMapper.selectSaleOrderMessageByOrderId(saleOrderId);
    }
}
