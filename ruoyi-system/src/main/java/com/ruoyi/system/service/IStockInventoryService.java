package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.StockInventory;
import com.ruoyi.system.domain.stock.StockInventoryLine;

/**
 * InventoryService接口
 * 
 * @author Lin
 * @date 2023-05-31
 */
public interface IStockInventoryService 
{
    /**
     * 查询Inventory
     * 
     * @param id Inventory主键
     * @return Inventory
     */
    public StockInventory selectStockInventoryById(Long id);

    /**
     * 查询Inventory列表
     * 
     * @param stockInventory Inventory
     * @return Inventory集合
     */
    public List<StockInventory> selectStockInventoryList(StockInventory stockInventory);

    /**
     * 新增Inventory
     * 
     * @param stockInventory Inventory
     * @return 结果
     */
    public int insertStockInventory(StockInventory stockInventory);

    /**
     * 修改Inventory
     * 
     * @param stockInventory Inventory
     * @return 结果
     */
    public int updateStockInventory(StockInventory stockInventory);

    /**
     * 批量删除Inventory
     * 
     * @param ids 需要删除的Inventory主键集合
     * @return 结果
     */
    public int deleteStockInventoryByIds(Long[] ids);

    /**
     * 删除Inventory信息
     * 
     * @param id Inventory主键
     * @return 结果
     */
    public int deleteStockInventoryById(Long id);

}
