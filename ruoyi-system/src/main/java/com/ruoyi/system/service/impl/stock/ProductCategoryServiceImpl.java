package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductCategoryMapper;
import com.ruoyi.system.domain.stock.ProductCategory;
import com.ruoyi.system.service.IProductCategoryService;

/**
 * 产品内部类别Service业务层处理
 * 
 * @author Lin
 * @date 2023-06-09
 */
@Service
public class ProductCategoryServiceImpl implements IProductCategoryService 
{
    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    /**
     * 查询产品内部类别
     * 
     * @param id 产品内部类别主键
     * @return 产品内部类别
     */
    @Override
    public ProductCategory selectProductCategoryById(Long id)
    {
        return productCategoryMapper.selectProductCategoryById(id);
    }

    /**
     * 查询产品内部类别列表
     * 
     * @param productCategory 产品内部类别
     * @return 产品内部类别
     */
    @Override
    public List<ProductCategory> selectProductCategoryList(ProductCategory productCategory)
    {
        return productCategoryMapper.selectProductCategoryList(productCategory);
    }

    /**
     * 新增产品内部类别
     * 
     * @param productCategory 产品内部类别
     * @return 结果
     */
    @Override
    public int insertProductCategory(ProductCategory productCategory)
    {
        return productCategoryMapper.insertProductCategory(productCategory);
    }

    /**
     * 修改产品内部类别
     * 
     * @param productCategory 产品内部类别
     * @return 结果
     */
    @Override
    public int updateProductCategory(ProductCategory productCategory)
    {
        return productCategoryMapper.updateProductCategory(productCategory);
    }

    /**
     * 批量删除产品内部类别
     * 
     * @param ids 需要删除的产品内部类别主键
     * @return 结果
     */
    @Override
    public int deleteProductCategoryByIds(Long[] ids)
    {
        return productCategoryMapper.deleteProductCategoryByIds(ids);
    }

    /**
     * 删除产品内部类别信息
     * 
     * @param id 产品内部类别主键
     * @return 结果
     */
    @Override
    public int deleteProductCategoryById(Long id)
    {
        return productCategoryMapper.deleteProductCategoryById(id);
    }
}
