package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.StockProductEntity;
import com.ruoyi.system.domain.stock.StockProductTemplate;
import com.ruoyi.system.mapper.StockProductEntityMapper;
import com.ruoyi.system.mapper.StockProductTemplateMapper;
import com.ruoyi.system.service.IPrintFileService;
import com.ruoyi.system.service.impl.purchase.PurchaseOrderServiceImpl;
import com.ruoyi.system.service.impl.sale.ResPartnerServiceImpl;
import com.ruoyi.system.service.impl.sale.SaleOrderServiceImpl;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author Lin
 * @Date 2023 06 07 14
 * 报表打印类
 **/
@Service
public class PrintFileServiceImpl implements IPrintFileService {

    @Autowired
    StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    StockProductEntityMapper stockProductEntityMapper;


    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static String PATH = "C:\\Users\\12023\\Desktop\\";


    public static void main(String[] args) throws IOException {

        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\12023\\Desktop\\采购合同样本JL-8.4-03.xlsx");
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet xssfWorkbookSheet = xssfWorkbook.getSheet("Sheet1");

        //联系方式行:第四行
        XSSFRow row4 = xssfWorkbookSheet.getRow(3);
        XSSFCell cell46 = row4.getCell(5);
        cell46.setCellValue(cell46.getStringCellValue()+"17660628432");

        //传真行：第五行
        XSSFRow row5 = xssfWorkbookSheet.getRow(4);
        XSSFCell cell56 = row5.getCell(5);
        cell56.setCellValue(cell56.getStringCellValue()+"532.8356.8621");

        //电子邮件行：第六行
        XSSFRow row6 = xssfWorkbookSheet.getRow(5);
        XSSFCell cell65 = row6.getCell(4);
        cell65.setCellValue(cell65.getStringCellValue()+"zl19970225@163.com");

        //第十行：供货方行
        XSSFRow row10 = xssfWorkbookSheet.getRow(9);
        XSSFCell cell101 = row10.getCell(0);
        cell101.setCellValue(cell101.getStringCellValue()+"莱尼尔智能科技");

        XSSFCell cell107 = row10.getCell(6);
        cell107.setCellValue(new Date());

        //第11行编辑 联络人和 采购单号
        XSSFRow row11 = xssfWorkbookSheet.getRow(10);
        XSSFCell cell111 = row11.getCell(0);
        cell111.setCellValue(cell111.getStringCellValue()+"赵林");

        XSSFCell cell117 = row11.getCell(6);
        cell117.setCellValue("MIC-QZ-001");

        //第12行编辑 电话/传真     预计交货期
        XSSFRow row12 = xssfWorkbookSheet.getRow(11);
        XSSFCell cell121 = row12.getCell(0);
        cell121.setCellValue(cell121.getStringCellValue()+"+86 17660628432");

        XSSFCell cell126 = row12.getCell(5);
        cell126.setCellValue(cell126.getStringCellValue()+new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

        //第13行编辑 ：来源
        XSSFRow row13 = xssfWorkbookSheet.getRow(12);
        XSSFCell cell136 = row13.getCell(5);
        cell136.setCellValue(cell136.getStringCellValue()+"MIC-XX-001");

        //第14行  供货地址编辑
        XSSFRow row14 = xssfWorkbookSheet.getRow(13);
        XSSFCell cell141 = row14.getCell(0);
        cell141.setCellValue(cell141.getStringCellValue()+"青岛市崂山区同安路880号大拇指广场");


        //第15行 是否含税 编辑
        XSSFRow row15 = xssfWorkbookSheet.getRow(14);
        XSSFCell cell156 = row15.getCell(5);
        cell156.setCellValue(cell156.getStringCellValue()+"("+"含税"+")");

        //16-20 行是采购订单的订单行部分




        //编制：
        XSSFRow row34 = xssfWorkbookSheet.getRow(33);
        XSSFCell cell344 = row34.getCell(3);
        cell344.setCellValue(cell344.getStringCellValue()+"徐春燕2023.06.19");


        //生成一张表(IO流)，03版本就是使用xlsx结尾
        FileOutputStream fos = new FileOutputStream(PATH + "测试表.xlsx");
        //输出
        xssfWorkbook.write(fos);
        //关闭流
        fos.close();
        System.out.println("文件生成完毕");
    }


    /**
     *
     * @param
     * @param startRow 插入行的行标,即在哪一行下插入
     * @param rows 插入多少行
     * @param sheet XSSFSheet
     * @param copyvalue 新行复制(startRow-1)行的样式,而且在拷贝行的时候可以指定是否需要拷贝值
     * @Author Wsong qzsoft
     */
    private static void InsertRow(int startRow, int rows, XSSFSheet sheet, Boolean copyvalue) {
        if(sheet.getRow(startRow+1)==null){
            // 如果复制最后一行，首先需要创建最后一行的下一行，否则无法插入，Bug 2023/03/20修复
            sheet.createRow(startRow+1);
        }

        //先获取原始的合并单元格address集合
        List<CellRangeAddress> originMerged = sheet.getMergedRegions();

        for (int i = sheet.getNumMergedRegions() - 1; i >= 0; i--) {
            CellRangeAddress region = sheet.getMergedRegion(i);
            //判断移动的行数后重新拆分
            if(region.getFirstRow()>startRow){
                sheet.removeMergedRegion(i);
            }
        }

        sheet.shiftRows(startRow,sheet.getLastRowNum(),rows,true,false);
        sheet.createRow(startRow);

        for(CellRangeAddress cellRangeAddress : originMerged) {
            //这里的8是插入行的index，表示这行之后才重新合并
            if(cellRangeAddress.getFirstRow() > startRow) {
                //你插入了几行就加几，我这里插入了一行，加1
                int firstRow = cellRangeAddress.getFirstRow() + rows;
                CellRangeAddress newCellRangeAddress = new CellRangeAddress(firstRow, (firstRow + (cellRangeAddress
                        .getLastRow() - cellRangeAddress.getFirstRow())), cellRangeAddress.getFirstColumn(),
                        cellRangeAddress.getLastColumn());
                sheet.addMergedRegion(newCellRangeAddress);
            }
        }
        CellCopyPolicy cellCopyPolicy = new CellCopyPolicy();
        cellCopyPolicy.setCopyCellValue(copyvalue);
        cellCopyPolicy.isCopyCellValue();
        for (int i = 0; i < rows; i++) {
            sheet.copyRows(startRow-1,startRow-1,startRow+i,cellCopyPolicy);
        }
    }


    /**
     * 订单审核表
     * @param saleOrderId 销售订单id
     * @return
     */
    @Override
    public Workbook writeSaleOrderConfirmFormExcel(Long saleOrderId) throws IOException {

        SaleOrderServiceImpl saleOrderBean = SpringUtils.getBean(SaleOrderServiceImpl.class);
        SaleOrder saleOrder = saleOrderBean.selectSaleOrderById(saleOrderId);
        List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();

        ResPartnerServiceImpl partnerService = SpringUtils.getBean(ResPartnerServiceImpl.class);
        ResPartner resPartner = partnerService.selectResPartnerById(saleOrder.getPartnerId().longValue());


        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\12023\\Desktop\\MIC-JL-8.2-02.xlsx");
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet xssfWorkbookSheet = xssfWorkbook.getSheet("订单审核表");

        //第三行
        XSSFRow row3 = xssfWorkbookSheet.getRow(2);
        XSSFCell cell31 = row3.getCell(0);
        //合同号
        cell31.setCellValue(saleOrder.getContractNo());

        //第四行
        XSSFRow row4 = xssfWorkbookSheet.getRow(3);
        XSSFCell cell42 = row4.getCell(2);
        //客户名称
        cell42.setCellValue("QZ");
        XSSFCell cell43 = row4.getCell(4);
        //制单日期
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        cell43.setCellValue(simpleDateFormat.format(new Date()));

        //第五行
        XSSFRow row5 = xssfWorkbookSheet.getRow(4);
        XSSFCell cell52 = row5.getCell(2);
        //客户订单号
        cell52.setCellValue(saleOrder.getName());
        XSSFCell cell55 = row5.getCell(4);
        //要求交期
        cell55.setCellValue(simpleDateFormat.format(saleOrder.getValidityDate()));

        //第六行
        XSSFRow row6 = xssfWorkbookSheet.getRow(5);
        XSSFCell cell63 = row6.getCell(2);
        //工作指令单号
        cell63.setCellValue(saleOrder.getInstructionOrder());

        //订单行内容
        InsertRow(8,saleOrderParts.size()-1,xssfWorkbookSheet,false);

        //合并单元格：订单内容
        CellRangeAddress cellAddresses = new CellRangeAddress(7, 7+saleOrderParts.size(), 0, 0);
        xssfWorkbookSheet.addMergedRegion(cellAddresses);
        int lastRowNum = xssfWorkbookSheet.getLastRowNum();
        System.out.println(lastRowNum);



        for (int i = 0; i < saleOrderParts.size(); i++) {
            SaleOrderPart saleOrderPart =  saleOrderParts.get(i);
            XSSFRow rowLine = xssfWorkbookSheet.getRow(7 + i);
            //组件名称
            XSSFCell cell72 = rowLine.getCell(1);
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(saleOrderPart.getProductId());
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
            cell72.setCellValue(stockProductTemplate.getName());

            XSSFCell cell74 = rowLine.getCell(3);
            cell74.setCellValue(saleOrderPart.getSurfaceTreatment());

            XSSFCell cell75 = rowLine.getCell(4);
            cell75.setCellValue(saleOrderPart.getProductUomQty());

            XSSFCell cell77 = rowLine.getCell(6);
            cell77.setCellValue(saleOrderPart.getPartMessage());
        }
        //末尾行
        XSSFRow rowLast = xssfWorkbookSheet.getRow(lastRowNum-1);
        XSSFCell cell = rowLast.getCell(6);
        cell.setCellValue("赵林");
        return xssfWorkbook;
    }


    /**
     * 订单审核表
     * @param
     * @return
     */
    @Override
    public Workbook writePurchaseOrderContactFormExcel(Long purchaseOrderId) throws IOException {
        PurchaseOrderServiceImpl purchaseOrderService = SpringUtils.getBean(PurchaseOrderServiceImpl.class);
//        PurchaseOrder purchaseOrder = purchaseOrderService.selectPurchaseOrderById(purchaseOrderId);

        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\12023\\Desktop\\采购合同样本JL-8.4-03.xlsx");
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
        XSSFSheet xssfWorkbookSheet = xssfWorkbook.getSheet("Sheet1");

        //联系方式行:第四行
        XSSFRow row4 = xssfWorkbookSheet.getRow(3);
        XSSFCell cell46 = row4.getCell(5);
        cell46.setCellValue(cell46.getStringCellValue()+"17660628432");

        //传真行：第五行
        XSSFRow row5 = xssfWorkbookSheet.getRow(4);
        XSSFCell cell56 = row5.getCell(5);
        cell56.setCellValue(cell56.getStringCellValue()+"532.8356.8621");

        //电子邮件行：第六行
        XSSFRow row6 = xssfWorkbookSheet.getRow(5);
        XSSFCell cell65 = row6.getCell(4);
        cell65.setCellValue(cell65.getStringCellValue()+"zl19970225@163.com");

        //第十行：供货方行
        XSSFRow row10 = xssfWorkbookSheet.getRow(9);
        XSSFCell cell101 = row10.getCell(0);
        cell101.setCellValue(cell101.getStringCellValue()+"莱尼尔智能科技");

        XSSFCell cell107 = row10.getCell(6);
        cell107.setCellValue(new Date());

        //第11行编辑 联络人和 采购单号
        XSSFRow row11 = xssfWorkbookSheet.getRow(10);
        XSSFCell cell111 = row11.getCell(0);
        cell111.setCellValue(cell111.getStringCellValue()+"赵林");

        XSSFCell cell117 = row11.getCell(6);
        cell117.setCellValue("MIC-QZ-001");

        //第12行编辑 电话/传真     预计交货期
        XSSFRow row12 = xssfWorkbookSheet.getRow(11);
        XSSFCell cell121 = row12.getCell(0);
        cell121.setCellValue(cell121.getStringCellValue()+"+86 17660628432");

        XSSFCell cell126 = row12.getCell(5);
        cell126.setCellValue(cell126.getStringCellValue()+new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

        //第13行编辑 ：来源
        XSSFRow row13 = xssfWorkbookSheet.getRow(12);
        XSSFCell cell136 = row13.getCell(5);
        cell136.setCellValue(cell136.getStringCellValue()+"MIC-XX-001");

        //第14行  供货地址编辑
        XSSFRow row14 = xssfWorkbookSheet.getRow(13);
        XSSFCell cell141 = row14.getCell(0);
        cell141.setCellValue(cell141.getStringCellValue()+"青岛市崂山区同安路880号大拇指广场");


        //第15行 是否含税 编辑
        XSSFRow row15 = xssfWorkbookSheet.getRow(14);
        XSSFCell cell156 = row15.getCell(5);
        cell156.setCellValue(cell156.getStringCellValue()+"("+"含税"+")");

        //16-20 行是采购订单的订单行部分


        //编制：
        XSSFRow row34 = xssfWorkbookSheet.getRow(33);
        XSSFCell cell344 = row34.getCell(3);
        cell344.setCellValue(cell344.getStringCellValue()+"徐春燕2023.06.19");

        System.out.println("文件生成完毕");
        return xssfWorkbook;
    }

}
