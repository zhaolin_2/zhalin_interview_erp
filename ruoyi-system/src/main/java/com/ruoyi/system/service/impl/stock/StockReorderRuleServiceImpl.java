package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockReorderRuleMapper;
import com.ruoyi.system.domain.stock.StockReorderRule;
import com.ruoyi.system.service.IStockReorderRuleService;

/**
 * 重订货Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-17
 */
@Service
public class StockReorderRuleServiceImpl implements IStockReorderRuleService 
{
    @Autowired
    private StockReorderRuleMapper stockReorderRuleMapper;

    /**
     * 查询重订货
     * 
     * @param id 重订货主键
     * @return 重订货
     */
    @Override
    public StockReorderRule selectStockReorderRuleById(Long id)
    {
        return stockReorderRuleMapper.selectStockReorderRuleById(id);
    }

    /**
     * 查询重订货列表
     * 
     * @param stockReorderRule 重订货
     * @return 重订货
     */
    @Override
    public List<StockReorderRule> selectStockReorderRuleList(StockReorderRule stockReorderRule)
    {
        return stockReorderRuleMapper.selectStockReorderRuleList(stockReorderRule);
    }

    /**
     * 新增重订货
     *
     *      * 新增重订货新增接口
     *      * 情况一：库存产品新建时添加重订货规则(1)
     *      * 情况二：库存产品详情页面添加重订货规则(2)
     *      * 情况三：重订货规则总览页面添加重订货规则(3)
     *
     * @param stockReorderRule 重订货
     * @return 结果
     */
    @Override
    public int insertStockReorderRule(StockReorderRule stockReorderRule)
    {
        /**
         * 情况一：库存产品新建时添加重订货规则(1)
         * 没有产品id 需要产品插入后，插入该产品的重订货规则
         */

        /**
         * 库存产品详情页面添加重订货规则(2)
         * 有产品id 可以直接添加
         */

        //情况三：重订货规则总览页面添加重订货规则(3)
        return stockReorderRuleMapper.insertStockReorderRule(stockReorderRule);
    }

    /**
     * 修改重订货
     * 
     * @param stockReorderRule 重订货
     * @return 结果
     */
    @Override
    public int updateStockReorderRule(StockReorderRule stockReorderRule)
    {
        return stockReorderRuleMapper.updateStockReorderRule(stockReorderRule);
    }

    /**
     * 批量删除重订货
     * 
     * @param ids 需要删除的重订货主键
     * @return 结果
     */
    @Override
    public int deleteStockReorderRuleByIds(Long[] ids)
    {
        return stockReorderRuleMapper.deleteStockReorderRuleByIds(ids);
    }

    /**
     * 删除重订货信息
     * 
     * @param id 重订货主键
     * @return 结果
     */
    @Override
    public int deleteStockReorderRuleById(Long id)
    {
        return stockReorderRuleMapper.deleteStockReorderRuleById(id);
    }
}
