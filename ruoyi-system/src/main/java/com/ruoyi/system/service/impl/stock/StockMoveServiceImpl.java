package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockMoveMapper;
import com.ruoyi.system.domain.stock.StockMove;
import com.ruoyi.system.service.IStockMoveService;

/**
 * 库存收发Service业务层处理
 * 
 * @author Lin
 * @date 2023-05-14
 */
@Service
public class StockMoveServiceImpl implements IStockMoveService 
{
    @Autowired
    private StockMoveMapper stockMoveMapper;

    /**
     * 查询库存收发
     * 
     * @param id 库存收发主键
     * @return 库存收发
     */
    @Override
    public StockMove selectStockMoveById(Long id)
    {
        return stockMoveMapper.selectStockMoveById(id);
    }

    /**
     * 查询库存收发列表
     * 
     * @param stockMove 库存收发
     * @return 库存收发
     */
    @Override
    public List<StockMove> selectStockMoveList(StockMove stockMove)
    {
        return stockMoveMapper.selectStockMoveList(stockMove);
    }

    /**
     * 新增库存收发
     * 
     * @param stockMove 库存收发
     * @return 结果
     */
    @Override
    public int insertStockMove(StockMove stockMove)
    {
        return stockMoveMapper.insertStockMove(stockMove);
    }

    /**
     * 修改库存收发
     * 
     * @param stockMove 库存收发
     * @return 结果
     */
    @Override
    public int updateStockMove(StockMove stockMove)
    {
        return stockMoveMapper.updateStockMove(stockMove);
    }

    /**
     * 批量删除库存收发
     * 
     * @param ids 需要删除的库存收发主键
     * @return 结果
     */
    @Override
    public int deleteStockMoveByIds(Long[] ids)
    {
        return stockMoveMapper.deleteStockMoveByIds(ids);
    }

    /**
     * 删除库存收发信息
     * 
     * @param id 库存收发主键
     * @return 结果
     */
    @Override
    public int deleteStockMoveById(Long id)
    {
        return stockMoveMapper.deleteStockMoveById(id);
    }
}
