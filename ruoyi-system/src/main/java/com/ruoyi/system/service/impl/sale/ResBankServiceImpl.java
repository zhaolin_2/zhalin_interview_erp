package com.ruoyi.system.service.impl.sale;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResBankMapper;
import com.ruoyi.system.domain.sale.ResBank;
import com.ruoyi.system.service.IResBankService;

/**
 * 银行管理Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-03
 */
@Service
public class ResBankServiceImpl implements IResBankService 
{
    @Autowired
    private ResBankMapper resBankMapper;

    /**
     * 查询银行管理
     * 
     * @param id 银行管理主键
     * @return 银行管理
     */
    @Override
    public ResBank selectResBankById(Long id)
    {
        return resBankMapper.selectResBankById(id);
    }

    /**
     * 查询银行管理列表
     * 
     * @param resBank 银行管理
     * @return 银行管理
     */
    @Override
    public List<ResBank> selectResBankList(ResBank resBank)
    {
        return resBankMapper.selectResBankList(resBank);
    }

    /**
     * 新增银行管理
     * 
     * @param resBank 银行管理
     * @return 结果
     */
    @Override
    public int insertResBank(ResBank resBank)
    {
        return resBankMapper.insertResBank(resBank);
    }

    /**
     * 修改银行管理
     * 
     * @param resBank 银行管理
     * @return 结果
     */
    @Override
    public int updateResBank(ResBank resBank)
    {
        return resBankMapper.updateResBank(resBank);
    }

    /**
     * 批量删除银行管理
     * 
     * @param ids 需要删除的银行管理主键
     * @return 结果
     */
    @Override
    public int deleteResBankByIds(Long[] ids)
    {
        return resBankMapper.deleteResBankByIds(ids);
    }

    /**
     * 删除银行管理信息
     * 
     * @param id 银行管理主键
     * @return 结果
     */
    @Override
    public int deleteResBankById(Long id)
    {
        return resBankMapper.deleteResBankById(id);
    }
}
