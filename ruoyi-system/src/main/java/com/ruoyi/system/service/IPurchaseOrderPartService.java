package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;

/**
 * 采购订单组件Service接口
 * 
 * @author Lin
 * @date 2023-04-06
 */
public interface IPurchaseOrderPartService 
{
    /**
     * 查询采购订单组件
     * 
     * @param id 采购订单组件主键
     * @return 采购订单组件
     */
    public PurchaseOrderPart selectPurchaseOrderPartById(Long id);

    /**
     * 查询采购订单组件列表
     * 
     * @param purchaseOrderPart 采购订单组件
     * @return 采购订单组件集合
     */
    public List<PurchaseOrderPart> selectPurchaseOrderPartList(PurchaseOrderPart purchaseOrderPart);

    /**
     * 新增采购订单组件
     * 
     * @param purchaseOrderPart 采购订单组件
     * @return 结果
     */
    public int insertPurchaseOrderPart(PurchaseOrderPart purchaseOrderPart);

    /**
     * 修改采购订单组件
     * 
     * @param purchaseOrderPart 采购订单组件
     * @return 结果
     */
    public int updatePurchaseOrderPart(PurchaseOrderPart purchaseOrderPart);

    /**
     * 批量删除采购订单组件
     * 
     * @param ids 需要删除的采购订单组件主键集合
     * @return 结果
     */
    public int deletePurchaseOrderPartByIds(Long[] ids);

    /**
     * 删除采购订单组件信息
     * 
     * @param id 采购订单组件主键
     * @return 结果
     */
    public int deletePurchaseOrderPartById(Long id);

    int purchaseOrderPartNamePut();
}
