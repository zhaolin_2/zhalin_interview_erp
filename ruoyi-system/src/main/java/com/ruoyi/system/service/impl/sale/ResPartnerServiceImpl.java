package com.ruoyi.system.service.impl.sale;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.hutool.extra.pinyin.PinyinUtil;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.sale.ResBank;
import com.ruoyi.system.domain.sale.ResPartnerAccount;
import com.ruoyi.system.mapper.ResBankMapper;
import com.ruoyi.system.mapper.ResPartnerAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResPartnerMapper;
import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.service.IResPartnerService;

/**
 * 合作伙伴Service业务层处理
 *
 * @author Lin
 * @date 2023-04-03
 */
@Service
public class ResPartnerServiceImpl implements IResPartnerService {
    @Autowired
    private ResPartnerMapper resPartnerMapper;

    @Autowired
    private ResBankMapper resBankMapper;

    @Autowired
    private ResPartnerAccountMapper resPartnerAccountMapper;

    /**
     * 查询合作伙伴
     * 返回前端提供给更新功能前的数据返回 在原有基础上新增了银行与账户信息
     * @param id 合作伙伴主键
     * @return 合作伙伴
     */
    @Override
    public ResPartner selectResPartnerById(Long id) {
        ResPartner resPartner = resPartnerMapper.selectResPartnerById(id);
        List<ResPartner> resPartners = resPartnerMapper.selectContactsInfoByResPartnerId(id);
        resPartner.setContacts(resPartners);
        try {
            ResPartnerAccount resPartnerAccount = resPartnerAccountMapper.selectResPartnerAccountByPartnerId(resPartner.getId());
            ResBank resBank = resBankMapper.selectResBankById(resPartnerAccount.getBankId());
            resPartner.setBankName(resBank.getName());
            resPartner.setAccountNumber(resPartnerAccount.getAccNumber());
            resPartner.setBankBic(resBank.getBic());
        }catch (NullPointerException e){
            return resPartner;
        }
        return resPartner;
    }

    /**
     * 查询合作伙伴列表
     *
     * @param resPartner 合作伙伴
     * @return 合作伙伴
     */
    @Override
    public List<ResPartner> selectResPartnerList(ResPartner resPartner) {
        return resPartnerMapper.selectResPartnerList(resPartner);
    }

    /**
     * 新增合作伙伴
     * 添加客户对应的银行与账户信息
     *
     * @param resPartner 合作伙伴
     * @return 结果
     */
    @Override
    public int insertResPartner(ResPartner resPartner) {
        if (resPartner.getParentId()== null){
            //添加客户信息 id 回传
            resPartner.setDisplayName(resPartner.getName());
            resPartner.setWriteDate(new Date());
            resPartner.setWriteUid(SecurityUtils.getUserId());
            resPartnerMapper.insertResPartner(resPartner);
            ResBank resBank = new ResBank(null, resPartner.getBankName(), "t", resPartner.getBankBic(), SecurityUtils.getUserId(), new Date());
            resBankMapper.insertResBank(resBank);
            ResPartnerAccount resPartnerAccount = new ResPartnerAccount(null, resPartner.getAccountNumber(), resPartner.getId(), resPartner.getBankName(), resBank.getId(), SecurityUtils.getUserId(), new Date());
            resPartnerAccountMapper.insertResPartnerAccount(resPartnerAccount);
            return 2;
        }

        if (resPartner.getParentId() != null){
            //添加客户信息 id 回传
            resPartner.setDisplayName(resPartner.getName());
            resPartner.setWriteDate(new Date());
            resPartner.setWriteUid(SecurityUtils.getUserId());
            ResPartner resPartnerFather = resPartnerMapper.selectResPartnerById(resPartner.getParentId());
            resPartner.setCustomer(resPartnerFather.getCustomer());
            resPartner.setSupplier(resPartnerFather.getSupplier());
            resPartner.setIsCompany("f");
            resPartnerMapper.insertResPartner(resPartner);
            return 1;
        }
        return 0;
    }

    /**
     * 修改合作伙伴
     *
     * @param resPartner 合作伙伴
     * @return 结果
     */
    @Override
    public int updateResPartner(ResPartner resPartner) {
        if (resPartner.getParentId() == null){
            //查询账户信息 并更新
            ResPartnerAccount resPartnerAccount = resPartnerAccountMapper.selectResPartnerAccountByPartnerId(resPartner.getId());
            resPartnerAccount.setAccNumber(resPartner.getAccountNumber());
            resPartnerAccount.setWriteDate(new Date());
            resPartnerAccount.setWriteUid(SecurityUtils.getUserId());
            resPartnerAccountMapper.updateResPartnerAccount(resPartnerAccount);
            //查询银行信息 并更新
            ResBank resBank = resBankMapper.selectResBankById(resPartnerAccount.getBankId());
            resBank.setBic(resPartner.getBankBic());
            resBank.setName(resPartner.getName());
            resBank.setWriteDate(new Date());
            resBank.setWriteUid(SecurityUtils.getUserId());
            resBankMapper.updateResBank(resBank);
            //更新合作伙伴信息
            resPartnerMapper.updateResPartner(resPartner);
            return 1;
        }
        if (resPartner.getParentId()!= null){
            //更新合作伙伴信息
            resPartnerMapper.updateResPartner(resPartner);
            return 1;
        }
        return 0;

    }

    /**
     * 批量删除合作伙伴
     *
     * @param ids 需要删除的合作伙伴主键
     * @return 结果
     */
    @Override
    public int deleteResPartnerByIds(Long[] ids) {
        for (Long id : ids) {
                deleteResPartnerById(id);
        }
        return ids.length;
    }

    /**
     * 删除合作伙伴信息
     *
     * @param id 合作伙伴主键
     * @return 结果
     */
    @Override
    public int deleteResPartnerById(Long id) {
        ResPartner resPartner = resPartnerMapper.selectResPartnerById(id);
        ResPartnerAccount resPartnerAccount = resPartnerAccountMapper.selectResPartnerAccountByPartnerId(resPartner.getId());
        if (resPartnerAccount !=null) {
            resPartnerAccountMapper.deleteResPartnerAccountById(resPartnerAccount.getId());
        }
        return resPartnerMapper.deleteResPartnerById(id);
    }

    /**
     * 根据伙伴id 查询合作伙伴的账户信息
     *
     * @param partnerId 伙伴id
     * @return
     */
    @Override
    public List selectPartnerMessageByPartnerId(Long partnerId) {
        List list = new ArrayList();
        //根据id 查询伙伴
        ResPartner resPartner = resPartnerMapper.selectResPartnerById(partnerId);
        List<ResPartner> resPartners = resPartnerMapper.selectContactsInfoByResPartnerId(partnerId);
        resPartner.setContacts(resPartners);
        //根据伙伴id 查询账户信息
        ResPartnerAccount resPartnerAccount = resPartnerAccountMapper.selectResPartnerAccountByPartnerId(partnerId);
        if (resPartnerAccount != null) {
            //根据账户信息关联的银行id 查询银行信息 回填 返回
            ResBank resBank = resBankMapper.selectResBankById(resPartnerAccount.getBankId());
            //注意：这里可能出现个人（非公司） 出现没有银行信息的情况，后期需要分类处理
            resPartnerAccount.setBankName(resBank.getName());
            list.add(resPartner);
            list.add(resPartnerAccount);
        } else {
            list.add(resPartner);
            list.add(new ResPartnerAccount());
        }
        return list;
    }

    //查询公司联系人详情：
    public List<ResPartner> getContactsInfoByResPartnerId(Long id){
        List<ResPartner> resPartners = resPartnerMapper.selectContactsInfoByResPartnerId(id);
        return resPartners;
    }

}
