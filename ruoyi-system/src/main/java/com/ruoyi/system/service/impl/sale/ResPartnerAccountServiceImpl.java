package com.ruoyi.system.service.impl.sale;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ResPartnerAccountMapper;
import com.ruoyi.system.domain.sale.ResPartnerAccount;
import com.ruoyi.system.service.IResPartnerAccountService;

/**
 * 账户信息Service业务层处理
 * 
 * @author lin
 * @date 2023-04-03
 */
@Service
public class ResPartnerAccountServiceImpl implements IResPartnerAccountService 
{
    @Autowired
    private ResPartnerAccountMapper resPartnerAccountMapper;

    /**
     * 查询账户信息
     * 
     * @param id 账户信息主键
     * @return 账户信息
     */
    @Override
    public ResPartnerAccount selectResPartnerAccountById(Long id)
    {
        return resPartnerAccountMapper.selectResPartnerAccountById(id);
    }

    /**
     * 查询账户信息列表
     * 
     * @param resPartnerAccount 账户信息
     * @return 账户信息
     */
    @Override
    public List<ResPartnerAccount> selectResPartnerAccountList(ResPartnerAccount resPartnerAccount)
    {
        return resPartnerAccountMapper.selectResPartnerAccountList(resPartnerAccount);
    }

    /**
     * 新增账户信息
     * 
     * @param resPartnerAccount 账户信息
     * @return 结果
     */
    @Override
    public int insertResPartnerAccount(ResPartnerAccount resPartnerAccount)
    {
        return resPartnerAccountMapper.insertResPartnerAccount(resPartnerAccount);
    }

    /**
     * 修改账户信息
     * 
     * @param resPartnerAccount 账户信息
     * @return 结果
     */
    @Override
    public int updateResPartnerAccount(ResPartnerAccount resPartnerAccount)
    {
        return resPartnerAccountMapper.updateResPartnerAccount(resPartnerAccount);
    }

    /**
     * 批量删除账户信息
     * 
     * @param ids 需要删除的账户信息主键
     * @return 结果
     */
    @Override
    public int deleteResPartnerAccountByIds(Long[] ids)
    {
        return resPartnerAccountMapper.deleteResPartnerAccountByIds(ids);
    }

    /**
     * 删除账户信息信息
     * 
     * @param id 账户信息主键
     * @return 结果
     */
    @Override
    public int deleteResPartnerAccountById(Long id)
    {
        return resPartnerAccountMapper.deleteResPartnerAccountById(id);
    }
}
