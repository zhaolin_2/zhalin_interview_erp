package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.StockProductEntity;

/**
 * 产品实体Service接口
 * 
 * @author Lin
 * @date 2023-04-04
 */
public interface IStockProductEntityService 
{
    /**
     * 查询产品实体
     * 
     * @param id 产品实体主键
     * @return 产品实体
     */
    public StockProductEntity selectStockProductEntityById(Long id);

    /**
     * 查询产品实体列表
     * 
     * @param stockProductEntity 产品实体
     * @return 产品实体集合
     */
    public List<StockProductEntity> selectStockProductEntityList(StockProductEntity stockProductEntity);

    /**
     * 新增产品实体
     * 
     * @param stockProductEntity 产品实体
     * @return 结果
     */
    public int insertStockProductEntity(StockProductEntity stockProductEntity);

    /**
     * 修改产品实体
     * 
     * @param stockProductEntity 产品实体
     * @return 结果
     */
    public int updateStockProductEntity(StockProductEntity stockProductEntity);

    /**
     * 批量删除产品实体
     * 
     * @param ids 需要删除的产品实体主键集合
     * @return 结果
     */
    public int deleteStockProductEntityByIds(Long[] ids);

    /**
     * 删除产品实体信息
     * 
     * @param id 产品实体主键
     * @return 结果
     */
    public int deleteStockProductEntityById(Long id);
}
