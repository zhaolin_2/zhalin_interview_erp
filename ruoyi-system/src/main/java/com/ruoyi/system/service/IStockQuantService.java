package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.StockQuant;

/**
 * 库存数量Service接口
 * 
 * @author Lin
 * @date 2023-04-04
 */
public interface IStockQuantService 
{
    /**
     * 查询库存数量
     * 
     * @param id 库存数量主键
     * @return 库存数量
     */
    public StockQuant selectStockQuantById(Long id);

    /**
     * 查询库存数量列表
     * 
     * @param stockQuant 库存数量
     * @return 库存数量集合
     */
    public List<StockQuant> selectStockQuantList(StockQuant stockQuant);


    List<StockQuant> selectStockQuantListRealNum();

    /**
     * 新增库存数量
     * 
     * @param stockQuant 库存数量
     * @return 结果
     */
    public int insertStockQuant(StockQuant stockQuant);

    /**
     * 修改库存数量
     * 
     * @param stockQuant 库存数量
     * @return 结果
     */
    public int updateStockQuant(StockQuant stockQuant);

    /**
     * 批量删除库存数量
     * 
     * @param ids 需要删除的库存数量主键集合
     * @return 结果
     */
    public int deleteStockQuantByIds(Long[] ids);

    /**
     * 删除库存数量信息
     * 
     * @param id 库存数量主键
     * @return 结果
     */
    public int deleteStockQuantById(Long id);

    StockQuant selectStockQuantPhysical(Long id);

    void stockQuantCalculate();
}
