package com.ruoyi.system.service.impl.stock;

import java.util.List;

import com.ruoyi.system.domain.stock.StockQuant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductUomMapper;
import com.ruoyi.system.domain.stock.ProductUom;
import com.ruoyi.system.service.IProductUomService;

/**
 * 单位名称Service业务层处理
 * 
 * @author Lin
 * @date 2023-06-09
 */
@Service
public class ProductUomServiceImpl implements IProductUomService
{
    @Autowired
    private ProductUomMapper productUomMapper;

    /**
     * 查询单位名称
     * 
     * @param id 单位名称主键
     * @return 单位名称
     */
    @Override
    public ProductUom selectProductUomById(Long id)
    {
        return productUomMapper.selectProductUomById(id);
    }

    /**
     * 查询单位名称列表
     * 
     * @param productUom 单位名称
     * @return 单位名称
     */
    @Override
    public List<ProductUom> selectProductUomList(ProductUom productUom)
    {
        return productUomMapper.selectProductUomList(productUom);
    }

    /**
     * 新增单位名称
     * 
     * @param productUom 单位名称
     * @return 结果
     */
    @Override
    public int insertProductUom(ProductUom productUom)
    {
        return productUomMapper.insertProductUom(productUom);
    }

    /**
     * 修改单位名称
     * 
     * @param productUom 单位名称
     * @return 结果
     */
    @Override
    public int updateProductUom(ProductUom productUom)
    {
        return productUomMapper.updateProductUom(productUom);
    }

    /**
     * 批量删除单位名称
     * 
     * @param ids 需要删除的单位名称主键
     * @return 结果
     */
    @Override
    public int deleteProductUomByIds(Long[] ids)
    {
        return productUomMapper.deleteProductUomByIds(ids);
    }

    /**
     * 删除单位名称信息
     * 
     * @param id 单位名称主键
     * @return 结果
     */
    @Override
    public int deleteProductUomById(Long id)
    {
        return productUomMapper.deleteProductUomById(id);
    }


}
