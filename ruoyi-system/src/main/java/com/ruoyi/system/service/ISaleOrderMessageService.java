package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.sale.SaleOrderMessage;

/**
 * 采购订单操作日志Service接口
 * 
 * @author Lin
 * @date 2023-04-06
 */
public interface ISaleOrderMessageService 
{
    /**
     * 查询采购订单操作日志
     * 
     * @param id 采购订单操作日志主键
     * @return 采购订单操作日志
     */
    public SaleOrderMessage selectSaleOrderMessageById(Long id);

    /**
     * 查询采购订单操作日志列表
     * 
     * @param saleOrderMessage 采购订单操作日志
     * @return 采购订单操作日志集合
     */
    public List<SaleOrderMessage> selectSaleOrderMessageList(SaleOrderMessage saleOrderMessage);

    /**
     * 新增采购订单操作日志
     * 
     * @param saleOrderMessage 采购订单操作日志
     * @return 结果
     */
    public int insertSaleOrderMessage(SaleOrderMessage saleOrderMessage);

    /**
     * 修改采购订单操作日志
     * 
     * @param saleOrderMessage 采购订单操作日志
     * @return 结果
     */
    public int updateSaleOrderMessage(SaleOrderMessage saleOrderMessage);

    /**
     * 批量删除采购订单操作日志
     * 
     * @param ids 需要删除的采购订单操作日志主键集合
     * @return 结果
     */
    public int deleteSaleOrderMessageByIds(Long[] ids);

    /**
     * 删除采购订单操作日志信息
     * 
     * @param id 采购订单操作日志主键
     * @return 结果
     */
    public int deleteSaleOrderMessageById(Long id);

    List<SaleOrderMessage> getSaleOrderMessageByOrderId(Long saleOrderId);
}
