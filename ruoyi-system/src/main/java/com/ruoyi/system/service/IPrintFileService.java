package com.ruoyi.system.service;

import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;

/**
 * @Author Lin
 * @Date 2023 06 07 14
 **/
public interface IPrintFileService {
    Workbook writeSaleOrderConfirmFormExcel(Long saleOrderId) throws IOException;

    Workbook writePurchaseOrderContactFormExcel(Long purchaseOrderId) throws IOException;
}
