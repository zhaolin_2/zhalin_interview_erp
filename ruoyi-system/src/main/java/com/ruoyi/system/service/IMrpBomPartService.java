package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.MrpBomPart;

/**
 * 物料清单组件Service接口
 * 
 * @author Lin
 * @date 2023-04-18
 */
public interface IMrpBomPartService 
{
    /**
     * 查询物料清单组件
     * 
     * @param id 物料清单组件主键
     * @return 物料清单组件
     */
    public MrpBomPart selectMrpBomPartById(Long id);

    /**
     * 查询物料清单组件列表
     * 
     * @param mrpBomPart 物料清单组件
     * @return 物料清单组件集合
     */
    public List<MrpBomPart> selectMrpBomPartList(MrpBomPart mrpBomPart);

    /**
     * 新增物料清单组件
     * 
     * @param mrpBomPart 物料清单组件
     * @return 结果
     */
    public int insertMrpBomPart(MrpBomPart mrpBomPart);

    /**
     * 修改物料清单组件
     * 
     * @param mrpBomPart 物料清单组件
     * @return 结果
     */
    public int updateMrpBomPart(MrpBomPart mrpBomPart);

    /**
     * 批量删除物料清单组件
     * 
     * @param ids 需要删除的物料清单组件主键集合
     * @return 结果
     */
    public int deleteMrpBomPartByIds(Long[] ids);

    /**
     * 删除物料清单组件信息
     * 
     * @param id 物料清单组件主键
     * @return 结果
     */
    public int deleteMrpBomPartById(Long id);
}
