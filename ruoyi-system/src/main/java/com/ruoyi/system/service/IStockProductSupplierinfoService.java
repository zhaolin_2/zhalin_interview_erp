package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.StockProductSupplierinfo;

/**
 * 库存供应商信息Service接口
 * 
 * @author Lin
 * @date 2023-04-17
 */
public interface IStockProductSupplierinfoService 
{
    /**
     * 查询库存供应商信息
     * 
     * @param id 库存供应商信息主键
     * @return 库存供应商信息
     */
    public StockProductSupplierinfo selectStockProductSupplierinfoById(Long id);

    /**
     * 查询库存供应商信息列表
     * 
     * @param stockProductSupplierinfo 库存供应商信息
     * @return 库存供应商信息集合
     */
    public List<StockProductSupplierinfo> selectStockProductSupplierinfoList(StockProductSupplierinfo stockProductSupplierinfo);

    /**
     * 新增库存供应商信息
     * 
     * @param stockProductSupplierinfo 库存供应商信息
     * @return 结果
     */
    public int insertStockProductSupplierinfo(StockProductSupplierinfo stockProductSupplierinfo);

    /**
     * 修改库存供应商信息
     * 
     * @param stockProductSupplierinfo 库存供应商信息
     * @return 结果
     */
    public int updateStockProductSupplierinfo(StockProductSupplierinfo stockProductSupplierinfo);

    /**
     * 批量删除库存供应商信息
     * 
     * @param ids 需要删除的库存供应商信息主键集合
     * @return 结果
     */
    public int deleteStockProductSupplierinfoByIds(Long[] ids);

    /**
     * 删除库存供应商信息信息
     * 
     * @param id 库存供应商信息主键
     * @return 结果
     */
    public int deleteStockProductSupplierinfoById(Long id);

    List<StockProductSupplierinfo> selectStockProductSupplierinfoByStockTmplId(Long stockId);

    //TODO
    void setProductIdByTmplId();
}
