package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MrpBomMsgMapper;
import com.ruoyi.system.domain.stock.MrpBomMsg;
import com.ruoyi.system.service.IMrpBomMsgService;

/**
 * bom操作信息Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Service
public class MrpBomMsgServiceImpl implements IMrpBomMsgService 
{
    @Autowired
    private MrpBomMsgMapper mrpBomMsgMapper;

    /**
     * 查询bom操作信息
     * 
     * @param id bom操作信息主键
     * @return bom操作信息
     */
    @Override
    public MrpBomMsg selectMrpBomMsgById(Long id)
    {
        return mrpBomMsgMapper.selectMrpBomMsgById(id);
    }

    /**
     * 查询bom操作信息列表
     * 
     * @param mrpBomMsg bom操作信息
     * @return bom操作信息
     */
    @Override
    public List<MrpBomMsg> selectMrpBomMsgList(MrpBomMsg mrpBomMsg)
    {
        return mrpBomMsgMapper.selectMrpBomMsgList(mrpBomMsg);
    }

    /**
     * 新增bom操作信息
     * 
     * @param mrpBomMsg bom操作信息
     * @return 结果
     */
    @Override
    public int insertMrpBomMsg(MrpBomMsg mrpBomMsg)
    {
        return mrpBomMsgMapper.insertMrpBomMsg(mrpBomMsg);
    }

    /**
     * 修改bom操作信息
     * 
     * @param mrpBomMsg bom操作信息
     * @return 结果
     */
    @Override
    public int updateMrpBomMsg(MrpBomMsg mrpBomMsg)
    {
        return mrpBomMsgMapper.updateMrpBomMsg(mrpBomMsg);
    }

    /**
     * 批量删除bom操作信息
     * 
     * @param ids 需要删除的bom操作信息主键
     * @return 结果
     */
    @Override
    public int deleteMrpBomMsgByIds(Long[] ids)
    {
        return mrpBomMsgMapper.deleteMrpBomMsgByIds(ids);
    }

    /**
     * 删除bom操作信息信息
     * 
     * @param id bom操作信息主键
     * @return 结果
     */
    @Override
    public int deleteMrpBomMsgById(Long id)
    {
        return mrpBomMsgMapper.deleteMrpBomMsgById(id);
    }
}
