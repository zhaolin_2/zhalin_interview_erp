package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockLocationMapper;
import com.ruoyi.system.domain.stock.StockLocation;
import com.ruoyi.system.service.IStockLocationService;

/**
 * 库存位置Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-14
 */
@Service
public class StockLocationServiceImpl implements IStockLocationService 
{
    @Autowired
    private StockLocationMapper stockLocationMapper;

    /**
     * 查询库存位置
     * 
     * @param id 库存位置主键
     * @return 库存位置
     */
    @Override
    public StockLocation selectStockLocationById(Long id)
    {
        return stockLocationMapper.selectStockLocationById(id);
    }

    /**
     * 查询库存位置列表
     * 
     * @param stockLocation 库存位置
     * @return 库存位置
     */
    @Override
    public List<StockLocation> selectStockLocationList(StockLocation stockLocation)
    {
        return stockLocationMapper.selectStockLocationList(stockLocation);
    }

    /**
     * 新增库存位置
     * 
     * @param stockLocation 库存位置
     * @return 结果
     */
    @Override
    public int insertStockLocation(StockLocation stockLocation)
    {
        return stockLocationMapper.insertStockLocation(stockLocation);
    }

    /**
     * 修改库存位置
     * 
     * @param stockLocation 库存位置
     * @return 结果
     */
    @Override
    public int updateStockLocation(StockLocation stockLocation)
    {
        return stockLocationMapper.updateStockLocation(stockLocation);
    }

    /**
     * 批量删除库存位置
     * 
     * @param ids 需要删除的库存位置主键
     * @return 结果
     */
    @Override
    public int deleteStockLocationByIds(Long[] ids)
    {
        return stockLocationMapper.deleteStockLocationByIds(ids);
    }

    /**
     * 删除库存位置信息
     * 
     * @param id 库存位置主键
     * @return 结果
     */
    @Override
    public int deleteStockLocationById(Long id)
    {
        return stockLocationMapper.deleteStockLocationById(id);
    }
}
