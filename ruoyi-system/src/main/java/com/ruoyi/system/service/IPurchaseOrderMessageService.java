package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.purchase.PurchaseOrderMessage;

/**
 * 采购订单日志Service接口
 * 
 * @author Lin
 * @date 2023-04-06
 */
public interface IPurchaseOrderMessageService 
{
    /**
     * 查询采购订单日志
     * 
     * @param id 采购订单日志主键
     * @return 采购订单日志
     */
    public PurchaseOrderMessage selectPurchaseOrderMessageById(Long id);

    /**
     * 查询采购订单日志列表
     * 
     * @param purchaseOrderMessage 采购订单日志
     * @return 采购订单日志集合
     */
    public List<PurchaseOrderMessage> selectPurchaseOrderMessageList(PurchaseOrderMessage purchaseOrderMessage);

    /**
     * 新增采购订单日志
     * 
     * @param purchaseOrderMessage 采购订单日志
     * @return 结果
     */
    public int insertPurchaseOrderMessage(PurchaseOrderMessage purchaseOrderMessage);

    /**
     * 修改采购订单日志
     * 
     * @param purchaseOrderMessage 采购订单日志
     * @return 结果
     */
    public int updatePurchaseOrderMessage(PurchaseOrderMessage purchaseOrderMessage);

    /**
     * 批量删除采购订单日志
     * 
     * @param ids 需要删除的采购订单日志主键集合
     * @return 结果
     */
    public int deletePurchaseOrderMessageByIds(Long[] ids);

    /**
     * 删除采购订单日志信息
     * 
     * @param id 采购订单日志主键
     * @return 结果
     */
    public int deletePurchaseOrderMessageById(Long id);

    /**
     * 根据订单id 查询采购订单操作信息
     * @param purchaseOrderId
     * @return
     */
    List<PurchaseOrderMessage> getOrderMessageByOrderId(Long purchaseOrderId);
}
