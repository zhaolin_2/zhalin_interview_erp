package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.MrpBomMsg;

/**
 * bom操作信息Service接口
 * 
 * @author Lin
 * @date 2023-04-18
 */
public interface IMrpBomMsgService 
{
    /**
     * 查询bom操作信息
     * 
     * @param id bom操作信息主键
     * @return bom操作信息
     */
    public MrpBomMsg selectMrpBomMsgById(Long id);

    /**
     * 查询bom操作信息列表
     * 
     * @param mrpBomMsg bom操作信息
     * @return bom操作信息集合
     */
    public List<MrpBomMsg> selectMrpBomMsgList(MrpBomMsg mrpBomMsg);

    /**
     * 新增bom操作信息
     * 
     * @param mrpBomMsg bom操作信息
     * @return 结果
     */
    public int insertMrpBomMsg(MrpBomMsg mrpBomMsg);

    /**
     * 修改bom操作信息
     * 
     * @param mrpBomMsg bom操作信息
     * @return 结果
     */
    public int updateMrpBomMsg(MrpBomMsg mrpBomMsg);

    /**
     * 批量删除bom操作信息
     * 
     * @param ids 需要删除的bom操作信息主键集合
     * @return 结果
     */
    public int deleteMrpBomMsgByIds(Long[] ids);

    /**
     * 删除bom操作信息信息
     * 
     * @param id bom操作信息主键
     * @return 结果
     */
    public int deleteMrpBomMsgById(Long id);
}
