package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.MrpBom;
import com.ruoyi.system.domain.stock.MrpBomMsg;
import com.ruoyi.system.domain.stock.MrpBomPart;

/**
 * 物料清单Service接口
 * 
 * @author Lin
 * @date 2023-04-18
 */
public interface IMrpBomService 
{
    /**
     * 查询物料清单
     * 
     * @param id 物料清单主键
     * @return 物料清单
     */
    public MrpBom selectMrpBomById(Long id);

    /**
     * 查询物料清单列表
     * 
     * @param mrpBom 物料清单
     * @return 物料清单集合
     */
    public List<MrpBom> selectMrpBomList(MrpBom mrpBom);

    /**
     * 新增物料清单
     * 
     * @param mrpBom 物料清单
     * @return 结果
     */
    public int insertMrpBom(MrpBom mrpBom);

    /**
     * 修改物料清单
     * 
     * @param mrpBom 物料清单
     * @return 结果
     */
    public int updateMrpBom(MrpBom mrpBom);

    /**
     * 批量删除物料清单
     * 
     * @param ids 需要删除的物料清单主键集合
     * @return 结果
     */
    public int deleteMrpBomByIds(Long[] ids);

    /**
     * 删除物料清单信息
     * 
     * @param id 物料清单主键
     * @return 结果
     */
    public int deleteMrpBomById(Long id);

    /**
     * 根据bom id 查询bom组件
     * @param bomId
     * @return
     */
    List<MrpBomPart> selectMrpBomPartByBomId(Long bomId);

    /**
     * 根据bomId 查询bom操作信息
     * @param bomId
     * @return
     */
    List<MrpBomMsg> selectMrpBomMsgByBomId(Long bomId);

    MrpBom selectMrpBomByStockTemplateId(Long stockId);

    MrpBom writeMrpBom(MrpBom mrpBom) throws IllegalAccessException;

    MrpBomPart bomPartInfo(Long sequence, String isInsert);

    MrpBom mrpBomPartUpdate(MrpBomPart mrpBomPartNew);

    MrpBom bomPartDelete(Long sequence, String isInsert);

    MrpBomPart getMrpBomPartPartFromStockProductTemplateId(Long stockId);

    String bomUpdateAbandon();

    void bomNameAndProductIdHandle();
}
