package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.stock.StockMove;
import com.ruoyi.system.domain.stock.StockPicking;
import com.ruoyi.system.domain.stock.vo.StockDeliveryOrReceiveVo;

/**
 * 库存拣货Service接口
 * 
 * @author Lin
 * @date 2023-05-14
 */
public interface IStockPickingService 
{
    /**
     * 查询库存拣货
     * 
     * @param id 库存拣货主键
     * @return 库存拣货
     */
    public StockPicking selectStockPickingById(Long id);

    /**
     * 查询库存拣货列表
     * 
     * @param stockPicking 库存拣货
     * @return 库存拣货集合
     */
    public List<StockPicking> selectStockPickingList(StockPicking stockPicking);

    /**
     * 新增库存拣货
     * 
     * @param stockPicking 库存拣货
     * @return 结果
     */
    public int insertStockPicking(StockPicking stockPicking);

    /**
     * 修改库存拣货
     * 
     * @param stockPicking 库存拣货
     * @return 结果
     */
    public int updateStockPicking(StockPicking stockPicking);

    /**
     * 批量删除库存拣货
     * 
     * @param ids 需要删除的库存拣货主键集合
     * @return 结果
     */
    public int deleteStockPickingByIds(Long[] ids);

    /**
     * 删除库存拣货信息
     * 
     * @param id 库存拣货主键
     * @return 结果
     */
    public int deleteStockPickingById(Long id);

    StockMove movePartInfo(Long sequence, String isInsert);

    List<StockMove> getMovePartsInfoByPickingId(Long pickingId);

    void stockPickingGenerateBySaleOrder(SaleOrder saleOrder);

    void stockPickingGenerateBySPurchaseOrder(PurchaseOrder purchaseOrder);

    String stockDelivery(StockPicking stockPicking);

    String stockReceive(StockPicking stockPicking);

    StockPicking writePicking(StockPicking stockPicking) throws IllegalAccessException;

    StockMove getMovePartFromStockProductTemplateId(Long stockId);

    StockPicking stockMovePartDelete(Long sequence, String isInsert);

    StockPicking stockMovePartUpdate(StockMove stockMoveNew);

    String stockPickingUpdateAbandon();

    String stockPickingConfirm(Long id);

    void setPartnerNameByPid();
}
