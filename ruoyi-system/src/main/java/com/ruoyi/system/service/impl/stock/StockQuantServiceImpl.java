package com.ruoyi.system.service.impl.stock;

import java.util.*;

import com.ruoyi.system.domain.stock.StockProductEntity;
import com.ruoyi.system.domain.stock.StockReorderRule;
import com.ruoyi.system.mapper.StockProductEntityMapper;
import com.ruoyi.system.mapper.StockReorderRuleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockQuantMapper;
import com.ruoyi.system.domain.stock.StockQuant;
import com.ruoyi.system.service.IStockQuantService;

/**
 * 库存数量Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Service
public class StockQuantServiceImpl implements IStockQuantService 
{
    @Autowired
    private StockQuantMapper stockQuantMapper;

    @Autowired
    private StockReorderRuleMapper stockReorderRuleMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    /**
     * 查询库存数量
     * 
     * @param id 库存数量主键
     * @return 库存数量
     */
    @Override
    public StockQuant selectStockQuantById(Long id)
    {
        return stockQuantMapper.selectStockQuantById(id);
    }

    /**
     * 查询库存数量列表
     * 
     * @param stockQuant 库存数量
     * @return 库存数量
     */
    @Override
    public List<StockQuant> selectStockQuantList(StockQuant stockQuant)
    {
        return stockQuantMapper.selectStockQuantList(stockQuant);
    }

    @Override
    public List<StockQuant> selectStockQuantListRealNum()
    {
        return stockQuantMapper.selectStockQuantListRealStockNum();
    }

    /**
     * 新增库存数量
     * 
     * @param stockQuant 库存数量
     * @return 结果
     */
    @Override
    public int insertStockQuant(StockQuant stockQuant)
    {
        return stockQuantMapper.insertStockQuant(stockQuant);
    }

    /**
     * 修改库存数量
     * 
     * @param stockQuant 库存数量
     * @return 结果
     */
    @Override
    public int updateStockQuant(StockQuant stockQuant)
    {
        return stockQuantMapper.updateStockQuant(stockQuant);
    }

    /**
     * 批量删除库存数量
     * 
     * @param ids 需要删除的库存数量主键
     * @return 结果
     */
    @Override
    public int deleteStockQuantByIds(Long[] ids)
    {
        return stockQuantMapper.deleteStockQuantByIds(ids);
    }

    /**
     * 删除库存数量信息
     * 
     * @param id 库存数量主键
     * @return 结果
     */
    @Override
    public int deleteStockQuantById(Long id)
    {
        return stockQuantMapper.deleteStockQuantById(id);
    }


    @Override
    public StockQuant selectStockQuantPhysical(Long id){
        return stockQuantMapper.selectStockPhysicalInventoryByProductId(id);
    }


    /**
     * TODO
     * 给库存初始化 把没有实际库存的算出来 更新回去
     * 已经有的就不算
     */
    @Override
    public void stockQuantCalculate(){
        List<StockProductEntity> entities = stockProductEntityMapper.selectStockProductEntityList(new StockProductEntity());
        List<StockQuant> stockQuantToInsert = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            StockProductEntity stockProductEntity = entities.get(i);
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockProductEntity.getId());
            //分两种情况，分别是包含15号库存的产品和不包含15号库存的产品
            if (stockQuant == null){
                //当stockQuant为空，则说明该库存不包含库存位置为15号的产品 根据计算规则 算出数字来 插入数据库
                List<StockQuant> stockQuants = stockQuantMapper.selectStockQuantByCalculate(stockProductEntity.getId());
                //该数组是一个产品库存数组,但是不包含15号库存的内容，现在需要进行补充操作，给没有15号位置的补充一个15号
                if (stockQuants.size()==0){
                    //说明该产品没有任何一个库存信息，编写一个库存为0的
                    stockQuant = new StockQuant();
                    stockQuant.setQuantity(0l);
                    stockQuant.setCreateUid(1l);
                    stockQuant.setProductId(stockProductEntity.getId());
                    stockQuant.setLocationId(15l);
                    stockQuant.setPredictiveQuantity(0l);
                    stockQuant.setOperationalQuantity(0l);
                    stockQuant.setInDate(new Date());
                    stockQuantToInsert.add(stockQuant);
                }else if (stockQuants.size() > 0){
                    //说明该产品有库存信息
                    Long quantity = 0l;
                    for (StockQuant quant : stockQuants) {
                            quantity  = quantity - quant.getQuantity();
                    }
                    stockQuant = new StockQuant();
                    stockQuant.setQuantity(quantity);
                    stockQuant.setCreateUid(1l);
                    stockQuant.setProductId(stockProductEntity.getId());
                    stockQuant.setPredictiveQuantity(0l);
                    stockQuant.setLocationId(15l);
                    stockQuant.setOperationalQuantity(0l);
                    stockQuant.setInDate(new Date());
                    stockQuantToInsert.add(stockQuant);
                }
            }
        }
        for (StockQuant stockQuant : stockQuantToInsert) {
            stockQuantMapper.insertStockQuant(stockQuant);
        }
// filteredMap就是筛选出不包含15的数据的Map集合
    }



    /**
     * 根据订单的需求产品数量，对所有的产品进行模拟扣除 和 库存占用操作
     * 步骤：
     * 库存模拟扣减 重订货
     */
    public void stockDeductVirtuallyAndReorderProduct(Map<Long,Long> productAndNeedMap){
        if (productAndNeedMap.isEmpty()){
            return;
        }
        for (Long a : productAndNeedMap.keySet()) {
            //取出key(产品id)
            Long productId = a;
            //查询是否存在重订货规则
            StockReorderRule stockReorderRule = stockReorderRuleMapper.selectStockReorderRuleByEntityId(productId);
            //当存在重订货规则
            if (stockReorderRule!=null){
                //模拟扣减
                StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(productId);

            }


        }

    }

}
