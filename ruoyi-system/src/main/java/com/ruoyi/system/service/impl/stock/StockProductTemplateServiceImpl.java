package com.ruoyi.system.service.impl.stock;

import java.math.BigDecimal;
import java.util.*;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.domain.stock.vo.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.impl.sale.SaleOrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.service.IStockProductTemplateService;

import javax.swing.*;

/**
 * 库存Service业务层处理
 *
 * @author Lin
 * @date 2023-03-30
 */
// TODO 在此做开发特别说明，因为程序重构概念关系，原程序使用库存模板与实体分开存表模式
// TODO 前端传入的stockId，实际上为库存模板id(stock_product_template 的 id) 后端称template 为模板
// TODO entity 为实体 ,实际stock_product_entity为中间表 ,库存产品主体为：stock_product_template,但是大部分信息是通entity的id来关联的
@Service
public class StockProductTemplateServiceImpl implements IStockProductTemplateService {
    @Autowired
    private StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    private StockTemplateMessageMapper stockMessageMapper;

    @Autowired
    private StockReorderRuleMapper stockReorderRuleMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    @Autowired
    private PurchaseOrderMapper purchaseOrderMapper;

    @Autowired
    private PurchaseOrderPartMapper purchaseOrderPartMapper;

    @Autowired
    private SaleOrderPartMapper saleOrderPartMapper;

    @Autowired
    private SaleOrderMapper saleOrderMapper;

    @Autowired
    private ResPartnerMapper resPartnerMapper;

    @Autowired
    private MrpBomPartMapper mrpBomPartMapper;

    @Autowired
    private MrpBomMapper mrpBomMapper;

    @Autowired
    private StockQuantMapper stockQuantMapper;

    @Autowired
    private StockProductSupplierinfoMapper stockProductSupplierinfoMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Autowired
    private ProductUomMapper productUomMapper;

    /**
     * 存放以订单为key值的产品扣减记录
     */
    private static Map<String, Map<Long, Long>> DEDUCTION_CALCULATED_MAP = new HashMap<>();


    /**
     * 查询库存
     *
     * @param id 库存主键
     * @return 库存
     */
    @Override
    public StockProductTemplate selectStockProductById(Long id) {
        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(id);
        stockProductTemplateTranslation(stockProductTemplate);
        return stockProductTemplate;
    }

    /**
     * 查询库存列表
     *
     * @param stockProduct 库存
     * @return 库存
     */
    @Override
    public List<StockProductTemplate> selectStockProductList(StockProductTemplate stockProduct) {
        return stockProductTemplateMapper.selectStockProductTemplateList(stockProduct);
    }


    /**
     * 新增库存
     *
     * @param stockProductVo 新增的插入对象
     * @return 结果
     */

    @Override
    public int insertStockProduct(StockProductTemplateVo stockProductVo) {
        //插入库存产品信息到stock_product_template表内
        StockProductTemplate stockProduct = new StockProductTemplate();
        BeanUtils.copyBeanProp(stockProduct, stockProductVo);
        //查询库存中是否存在与之完全相同的内容
        List<StockProductTemplate> stockProductTemplates = stockProductTemplateMapper.selectStockProductTemplateList(stockProduct);
        if (!stockProductTemplates.isEmpty()) {
            throw new RuntimeException("已有该产品的库存产品，无法重复添加");
        }
        try {
            stockProduct.setActive("t");//启用状态
            stockProduct.setCreateDate(new Date());//创建日期
            stockProduct.setType("product");
            stockProduct.setCreateUid(SecurityUtils.getUserId());//创建者id
            stockProduct.setWriteDate(new Date());//最新更新日期
            stockProduct.setListPrice(new BigDecimal(0.00));
            stockProduct.setWriteUid(SecurityUtils.getUserId());//最新更新者
            stockProductTemplateMapper.insertStockProductTemplate(stockProduct);
            System.out.println("库存模板添加成功");
            //添加库存操作信息 初始化
            StockTemplateMessage msgInsert = new StockTemplateMessage(null, stockProduct.getId(), "新增", "由" + SecurityUtils.getLoginUser().getUser().getNickName() + "添加了该存库产品", SecurityUtils.getLoginUser().getUser().getNickName(), stockProductVo.getQuantyWhenInsert().longValue(), new Date());
            stockMessageMapper.insertStockMessage(msgInsert);
            System.out.println("库存操作信息添加成功了");
            //为关联表stock_product_entity添加产品
            StockProductEntity stockProductEntity = new StockProductEntity();
            stockProductEntity.setActive("t");
            stockProductEntity.setProductTmplId(stockProduct.getId());
            stockProductEntity.setCreateDate(new Date());
            stockProductEntity.setCreateUid(SecurityUtils.getUserId());
            stockProductEntityMapper.insertStockProductEntity(stockProductEntity);
            System.out.println("库存实体对应添加OK");
            //为该库存产品添加库存数量信息
            StockQuant stockQuant = new StockQuant();
            stockQuant.setProductId(stockProductEntity.getId());
            stockQuant.setLocationId(15L);
            stockQuant.setQuantity(stockProductVo.getQuantyWhenInsert().longValue());
            stockQuant.setCreateDate(new Date());
            stockQuant.setCreateUid(SecurityUtils.getUserId());
            stockQuantMapper.insertStockQuant(stockQuant);
            System.out.println("库存数量对应表添加成功了");

            //给重订货规则添加对应产品规则
            if (stockProduct.getProductMinQty() != null & stockProduct.getProductMaxQty() != null) {
                //重订货规则同步库存产品插入
                StockReorderRule stockReorderRuleAdd = new StockReorderRule();
                stockReorderRuleAdd.setName(stockProduct.getName() + "重订货规则");
                stockReorderRuleAdd.setActive("t");
                stockReorderRuleAdd.setWarehouseId(1L);
                stockReorderRuleAdd.setProductId(15l);
                stockReorderRuleAdd.setProductId(stockProductEntity.getId());
                stockReorderRuleAdd.setProductMinQty(stockProduct.getProductMinQty().longValue());
                stockReorderRuleAdd.setProductMaxQty(stockProduct.getProductMaxQty().longValue());
                stockReorderRuleAdd.setQtyMultiple(1l);
                stockReorderRuleAdd.setCreateUid(SecurityUtils.getUserId());
                stockReorderRuleAdd.setCreateDate(new Date());
                stockReorderRuleAdd.setWriteDate(new Date());
                stockReorderRuleAdd.setWriteUid(SecurityUtils.getUserId());
                stockReorderRuleMapper.insertStockReorderRule(stockReorderRuleAdd);
                System.out.println("重订货规则添加成功了");
            }

            if (stockProduct.getPurchaseOk().equals("t") & stockProduct.getPartnerId() != null) {
                //供应商信息（如果是可采购产品）
                StockProductSupplierinfo stockProductSupplierinfo = new StockProductSupplierinfo();
                stockProductSupplierinfo.setName(stockProduct.getPartnerId().longValue());
                stockProductSupplierinfo.setMinQty(stockProductVo.getPurchaseMinQty().longValue());
                stockProductSupplierinfo.setProductTmplId(stockProduct.getId());
                stockProductSupplierinfo.setCreateUid(SecurityUtils.getUserId());
                stockProductSupplierinfo.setCreateDate(new Date());
                stockProductSupplierinfo.setPrice(stockProductVo.getPrice());
                stockProductSupplierinfoMapper.insertStockProductSupplierinfo(stockProductSupplierinfo);
                System.out.println("库存产品供应商信息添加成功了");
            }
            return stockProduct.getId().intValue();
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new RuntimeException("插入失败");
        }
    }


    /**
     * 修改库存
     *
     * @param stockProduct 库存
     * @return 结果
     */
    @Override
    public int updateStockProduct(StockProductTemplate stockProduct) {
        return stockProductTemplateMapper.updateStockProductTemplate(stockProduct);
    }

    /**
     * 批量删除库存
     *
     * @param ids 需要删除的库存主键
     * @return 结果
     */
    @Override
    public int deleteStockProductByIds(Long[] ids) {
        int i = 0;
        for (Long id : ids) {
            i += this.deleteStockProductById(id);
        }
        return i;
    }

    /**
     * 删除库存信息
     *
     * @param id 库存主键
     * @return 结果
     */
    @Override
    public int deleteStockProductById(Long id) {
        //删除库存产品模板
        int i1 = stockProductTemplateMapper.deleteStockProductTemplateById(id);
        //删除库存产品实体
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(id);
        stockProductEntityMapper.deleteStockProductEntityById(stockProductEntity.getId());
        //删除数量信息
        StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockProductEntity.getId());
        stockQuantMapper.deleteStockQuantById(stockQuant.getId());
        //删除供应商信息
        List<StockProductSupplierinfo> stockProductSupplierinfos = stockProductSupplierinfoMapper.selectStockProductSupplierinfoByStockTmplId(id);
        for (int i = 0; i < stockProductSupplierinfos.size(); i++) {
            StockProductSupplierinfo stockProductSupplierinfo = stockProductSupplierinfos.get(i);
            stockProductSupplierinfoMapper.deleteStockProductSupplierinfoById(stockProductSupplierinfo.getId());
        }
        //删除重订货规则信息
        StockReorderRule stockReorderRule = stockReorderRuleMapper.selectStockReorderRuleByEntityId(stockProductEntity.getId());
        if (stockReorderRule!=null){
            stockReorderRuleMapper.deleteStockReorderRuleById(stockReorderRule.getId());
        }
        //删除库存产品操作信息
        List<StockTemplateMessage> stockTemplateMessages = stockMessageMapper.selectStockMessageByStockId(id);
        for (int i = 0; i < stockTemplateMessages.size(); i++) {
            StockTemplateMessage stockTemplateMessage = stockTemplateMessages.get(i);
            stockMessageMapper.deleteStockMessageById(stockTemplateMessage.getId());
        }
        return i1;
    }

    @Override
    public List<StockTemplateMessage> getStockMessageByStockId(Long id) {
        return stockMessageMapper.selectStockMessageByStockId(id);
    }

    @Override
    public StockReorderRule getStockReorderRulesByStockId(Long stockId) {
        //根据模板id 查询实体id
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        //根据实体id 查询产品重订货规则并且返回
        return stockReorderRuleMapper.selectStockReorderRuleByEntityId(stockProductEntity.getId());
    }


    /**
     * @param stockId 库存产品模板id
     * @return
     */
    @Override
    public List<PurchaseOrderPart> selectStockPurchaseHistoryByStockId(Long stockId) {
        List<PurchaseOrderPart> purchaseOrderParts;
        try {
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
            //构造查询条件
            PurchaseOrderPart purchaseOrderPart = new PurchaseOrderPart();
            purchaseOrderPart.setProductId(stockProductEntity.getId());
            //查询出该库存产品在采购订单组件中的记录
            purchaseOrderParts = purchaseOrderPartMapper.selectPurchaseOrderPartList(purchaseOrderPart);
            for (int i = 0; i < purchaseOrderParts.size(); i++) {
                PurchaseOrderPart orderPart = purchaseOrderParts.get(i);
                //根据采购订单组件保存的订单id 查询对应订单
                PurchaseOrder purchaseOrder = purchaseOrderMapper.selectPurchaseOrderById(orderPart.getOrderId());
                //根据订单查询出该订单的供应商信息
                ResPartner resPartner = resPartnerMapper.selectResPartnerById(purchaseOrder.getPartnerId());
                /**
                 * 给订单组件设置对应需要展示的值:
                 * 1.所属订单名称（做链接跳转）：根据订单名称查询订单详情
                 * 2.订单供应商名称（做链接跳转：根据供应商名称查询供应商详情）
                 * 3.该产品采购数量(表内存在)
                 * 4.该产品采购时单价(表内存在)
                 * 5.该产品采购时 价格总计(表内存在)
                 */
                orderPart.setOrderDisplayName(purchaseOrder.getName());
                orderPart.setPartnerDisplayName(resPartner.getName());
            }
        } catch (NullPointerException e) {
            purchaseOrderParts = new ArrayList<>();
        }
        return purchaseOrderParts;
    }

    /**
     * @param stockId 库存产品模板id
     * @return
     */
    @Override
    public List<SaleOrderPart> selectStockSaleHistoryByStockId(Long stockId) {
        List<SaleOrderPart> saleOrderParts;
        try {
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
            //构造查询条件
            SaleOrderPart saleOrderPart = new SaleOrderPart();
            saleOrderPart.setProductId(stockProductEntity.getId());
            //查询出该库存产品在销售订单组件中的记录
            saleOrderParts = saleOrderPartMapper.selectSaleOrderPartList(saleOrderPart);
            for (int i = 0; i < saleOrderParts.size(); i++) {
                SaleOrderPart orderPart = saleOrderParts.get(i);
                //根据销售订单组件保存的订单id 查询对应订单
                SaleOrder saleOrder = saleOrderMapper.selectSaleOrderById(orderPart.getOrderId());
                //根据订单查询出该订单的客户信息
                ResPartner resPartner = resPartnerMapper.selectResPartnerById(saleOrder.getPartnerId().longValue());
                /* 给订单组件设置对应需要展示的值:
                 * 1.所属订单名称（做链接跳转）：根据订单名称查询订单详情
                 * 2.订单客户名称（做链接跳转：根据客户名称查询供应商详情）
                 * 3.该产品销售数量(表内存在)
                 * 4.该产品采购时单价(表内存在)
                 * 5.该产品采购时 价格总计(表内存在)
                 */
                orderPart.setOrderDisplayName(saleOrder.getName());
                orderPart.setOrderPartnerDisplayName(resPartner.getDisplayName());
            }
        } catch (NullPointerException e) {
            saleOrderParts = new ArrayList<>();
        }
        return saleOrderParts;
    }

    /**
     * 根据库存id 查看该产品的bom组件
     *
     * @param stockId 库存id
     * @return 该产品对应的bom 的 bomParts
     */
    @Override
    public List<MrpBomPart> selectStockBomPartByStockId(Long stockId) {
        List<MrpBomPart> mrpBomParts = new ArrayList<>();
        try {
            MrpBom mrpBom = mrpBomMapper.selectMrpBomByStockTemplateId(stockId);
            mrpBomParts = new MrpBomServiceImpl().selectMrpBomPartByBomId(mrpBom.getId());
        } catch (NullPointerException e) {
            mrpBomParts = new ArrayList<>();
        }
        return mrpBomParts;
    }

    /**
     * 根据库存产品id 查询该库存产品归属于哪些bom
     *
     * @param stockId
     * @return * 需要返回一个bom列表：内含
     * * 1.bom名称
     * * 2.该组件在该bom中的使用数量
     * * 3.该组件目前现存的库存数量
     */
    @Override
    public List<MrpBomInStockVo> selectStockBomBelongTo(Long stockId) {
        List<MrpBomInStockVo> mrpBomInStockVos = new ArrayList<>();
        //根据关联表查询出该库存产品的entityId，使用该id进入bom组件表内查询（bom_part）
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        //根据product id 查询所在所有组件
        List<MrpBomPart> mrpBomParts = mrpBomPartMapper.selectMrpBomPartByProductEntityId(stockProductEntity.getId());
        for (int i = 0; i < mrpBomParts.size(); i++) {
            MrpBomPart mrpBomPart = mrpBomParts.get(i);
            //根据组件保存的bom_id 查询对应bom 与该组件的库存数量
            MrpBom mrpBom = mrpBomMapper.selectMrpBomById(mrpBomPart.getBomId());
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(mrpBomPart.getProductId());
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(mrpBom.getProductTmplId());
            MrpBomInStockVo mrpBomInStockVo = new MrpBomInStockVo();
            mrpBomInStockVo.setId(mrpBom.getId());
            mrpBomInStockVo.setProductTmplId(mrpBom.getProductTmplId());
            mrpBomInStockVo.setProductTmplName(stockProductTemplate.getName());
            ProductUom productUom = productUomMapper.selectProductUomById(stockProductTemplate.getUomId());
            mrpBomInStockVo.setProductUomName(productUom.getName());
            if (stockQuant == null) {
                mrpBomInStockVo.setProductQty(0l);
            } else {
                mrpBomInStockVo.setProductQty(stockQuant.getQuantity());
            }
            mrpBomInStockVo.setProductUomId(mrpBom.getProductUomId());
            mrpBomInStockVo.setBomPartNumInBom(mrpBomPart.getProductQty());
            mrpBomInStockVos.add(mrpBomInStockVo);
        }
        return mrpBomInStockVos;
    }

    /**
     * 为库存产品添加供应商信息
     *
     * @param stockProductSupplierVo 需要库存产品id 供应商id 最低采购数量 供应商报价
     * @return
     */
    @Override
    public int insertStockProductTemplateSupplier(StockProductSupplierVo stockProductSupplierVo) {
        StockProductSupplierinfo stockProductSupplierinfo = new StockProductSupplierinfo();
        stockProductSupplierinfo.setName(stockProductSupplierVo.getPartnerId().longValue());
        stockProductSupplierinfo.setMinQty(stockProductSupplierVo.getMinQty().longValue());
        stockProductSupplierinfo.setPrice(stockProductSupplierVo.getPrice());
        stockProductSupplierinfo.setProductTmplId(stockProductSupplierVo.getId());
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockProductSupplierVo.getId());
        stockProductSupplierinfo.setProductId(stockProductEntity.getId());
        stockProductSupplierinfo.setCreateUid(SecurityUtils.getUserId());
        stockProductSupplierinfo.setCreateDate(new Date());
        stockProductSupplierinfo.setWriteDate(new Date());
        stockProductSupplierinfo.setWriteUid(SecurityUtils.getUserId());
        return stockProductSupplierinfoMapper.insertStockProductSupplierinfo(stockProductSupplierinfo);
    }

    /**
     * 库存产品 根据供应商信息id 删除该库存产品对应的供应商信息
     *
     * @param supplierInfoId
     * @return
     */
    @Override
    public int deleteStockProductSupplierInfoById(Long supplierInfoId) {
        return stockProductSupplierinfoMapper.deleteStockProductSupplierinfoById(supplierInfoId);
    }

    /**
     * 为库存产品添加重订货规则（库存产品详情页）
     *
     * @param stockReorderRuleVo
     * @return
     */
    @Override
    public int stockTemplateReorderRuleAdd(StockReorderRuleVo stockReorderRuleVo) {
        if (stockReorderRuleVo.getProductMinQty() > stockReorderRuleVo.getProductMaxQty()) {
            throw new RuntimeException("库存预警数量无法大于补货推荐数量，请重新填写");
        }
        //构造全新的重订货规则对象
        StockReorderRule stockReorderRuleAdd = new StockReorderRule();
        //查询库内产品模板表 和库存产品实体表 取出对应信息
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockReorderRuleVo.getStockId());
        if (stockReorderRuleMapper.selectStockReorderRuleByEntityId(stockProductEntity.getId()) != null) {
            throw new RuntimeException("该产品已经存在了一个重订货规则，无法继续添加");
        } else {
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockReorderRuleVo.getStockId());
            //设置值
            stockReorderRuleAdd.setName(stockProductTemplate.getName() + "重订货规则");
            stockReorderRuleAdd.setActive("t");
            stockReorderRuleAdd.setWarehouseId(1L);
            stockReorderRuleAdd.setProductId(15L);
            stockReorderRuleAdd.setProductId(stockProductEntity.getId());
            stockReorderRuleAdd.setProductMinQty(stockReorderRuleVo.getProductMinQty().longValue());
            stockReorderRuleAdd.setProductMaxQty(stockReorderRuleVo.getProductMaxQty().longValue());
            stockReorderRuleAdd.setQtyMultiple(1L);
            stockReorderRuleAdd.setCreateUid(SecurityUtils.getUserId());
            stockReorderRuleAdd.setCreateDate(new Date());
            stockReorderRuleAdd.setWriteDate(new Date());
            stockReorderRuleAdd.setWriteUid(SecurityUtils.getUserId());
            System.out.println("重订货规则添加成功了");
        }
        //插入
        return stockReorderRuleMapper.insertStockReorderRule(stockReorderRuleAdd);
    }

    /**
     * 更新库存产品的重订货规则
     *
     * @param stockReorderRuleVo 内含库存产品id 库存预警数量 补货推荐数量
     * @return
     */
    @Override
    public int stockTemplateReorderRuleUpdate(StockReorderRuleVo stockReorderRuleVo) {
        if (stockReorderRuleVo.getProductMinQty() > stockReorderRuleVo.getProductMaxQty()) {
            throw new RuntimeException("库存预警数量无法大于补货推荐数量，请重新填写");
        }
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockReorderRuleVo.getStockId());
        StockReorderRule stockReorderRule = stockReorderRuleMapper.selectStockReorderRuleByEntityId(stockProductEntity.getId());
        stockReorderRule.setWriteUid(SecurityUtils.getUserId());
        stockReorderRule.setWriteDate(new Date());
        stockReorderRule.setProductMinQty(stockReorderRuleVo.getProductMinQty());
        stockReorderRule.setProductMaxQty(stockReorderRuleVo.getProductMaxQty());
        return stockReorderRuleMapper.updateStockReorderRule(stockReorderRule);
    }

    /**
     * 库存产品 重订货规则删除
     *
     * @param stockId
     * @return
     */
    @Override
    public int stockTemplateReorderRuleDelete(Long stockId) {
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        StockReorderRule stockReorderRule = stockReorderRuleMapper.selectStockReorderRuleByEntityId(stockProductEntity.getId());
        return stockReorderRuleMapper.deleteStockReorderRuleById(stockReorderRule.getId());
    }

    @Override
    public int stockTemplateBomPartsAdd(MrpBomPartVo mrpBomPartVo) {
        //库存产品没有对应的bom时,需要首先的bom中新增一条bom
        MrpBom mrpBom = mrpBomMapper.selectMrpBomByStockTemplateId(mrpBomPartVo.getStockId());
        MrpBomPart mrpBomPart = new MrpBomPart();
        if (mrpBom == null) {
            //创建bom
            MrpBom mrpBomNew = new MrpBom();
            mrpBomNew.setActive("t");
            mrpBomNew.setProductTmplId(mrpBomPartVo.getStockId());
            mrpBomNew.setProductQty(mrpBomPartVo.getProductQty());
            mrpBomNew.setProductUomId(2l);
            mrpBomNew.setCreateDate(new Date());
            mrpBomNew.setCreateUid(SecurityUtils.getUserId());
            mrpBomNew.setWriteDate(new Date());
            mrpBomNew.setWriteUid(SecurityUtils.getUserId());
            mrpBomMapper.insertMrpBom(mrpBomNew);
            //给bom添加组件
            mrpBomPart.setBomId(mrpBomNew.getId());
        } else if (mrpBom != null) {
            mrpBomPart.setBomId(mrpBom.getId());
        }
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(mrpBomPartVo.getProductTemplateId());
        mrpBomPart.setProductId(stockProductEntity.getId());
        mrpBomPart.setProductQty(mrpBomPartVo.getProductQty());
        mrpBomPart.setProductUomId(2l);
        mrpBomPart.setCreateDate(new Date());
        mrpBomPart.setCreateUid(SecurityUtils.getUserId());
        mrpBomPart.setWriteDate(new Date());
        mrpBomPart.setWriteUid(SecurityUtils.getUserId());
        return mrpBomPartMapper.insertMrpBomPart(mrpBomPart);
    }

    @Override
    public StockProductTemplate copyStockTemplateProductByStockId(Long stockId) {
        //查询库存产品
        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockId);
        //查询产品下对应的重订货规则
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        stockReorderRuleMapper.selectStockReorderRuleByEntityId(stockProductEntity.getId());
        return null;
    }


    @Override
    public void stockProductTemplateTranslation(StockProductTemplate stockProductTemplate) {
        SysUser sysUserWrite = sysUserMapper.selectUserById(stockProductTemplate.getWriteUid());
        SysUser sysUserCreate = sysUserMapper.selectUserById(stockProductTemplate.getCreateUid());
        stockProductTemplate.setCreateUserName(sysUserCreate.getNickName());
        stockProductTemplate.setWriteUserName(sysUserWrite.getNickName());
        stockProductTemplate.setType("product");
        ProductUom productUom = productUomMapper.selectProductUomById(stockProductTemplate.getUomId());
        stockProductTemplate.setUomName(productUom.getName());
        ProductCategory productCategory = productCategoryMapper.selectProductCategoryById(stockProductTemplate.getCategId());
        stockProductTemplate.setCategName(productCategory.getCompleteName());
        return;
    }

    /**
     * 根据库存产品模板id 查询库存数量信息
     * @param stockId
     * @return 库存数量类
     */
    @Override
    public StockQuant selectStockQuantByStockTemplateId(Long stockId) {
        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockId);
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockProductTemplate.getId());
        StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockProductEntity.getId());
        if (stockQuant == null) {
            return new StockQuant();
        }
        return stockQuant;
    }


    /**
     * 销售订单 产品扣减数量计算
     *
     * @param id 传入的销售订单id
     * @return map集合，key为产品id,value为需要扣减的数量
     */
    @Override
    public Map<Long, Long> takeSaleOrderApartProductNeeded(Long id) {
        SaleOrderServiceImpl bean = SpringUtils.getBean(SaleOrderServiceImpl.class);
        SaleOrder saleOrder = bean.selectSaleOrderById(id);
        //取出订单组件列表
        List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
        //把每个订单组件对应的产品与需要的数量转换为map存放，如：id:1（铅笔）,quanty：20（需求20个）
        Map<Long, Long> productAndQuantNeededMap = new HashMap(); //产品id  和需求数量

        for (SaleOrderPart saleOrderPart : saleOrderParts) {
            productAndQuantNeededMap.put(saleOrderPart.getProductId(), saleOrderPart.getProductUomQty());
        }
        //map中已经包含了产品id 以及 该产品各自需求的数量 测试一下
        System.out.println(productAndQuantNeededMap + "产品id+订单需求数量");
        //{11012=10, 19112=50, 9563=10}
        //初始化最终结果map,存入一个全空的map
        DEDUCTION_CALCULATED_MAP.put(saleOrder.getName(), new HashMap<Long,Long>());
        Map<Long, Long> resultMap = new HashMap<>();
        //首次处理
        resultMap = bomCalculator(productAndQuantNeededMap);
        while (!isMapEqual(DEDUCTION_CALCULATED_MAP.get(saleOrder.getName()),resultMap)) {
            //存
            DEDUCTION_CALCULATED_MAP.put(saleOrder.getName(),resultMap);
            //处理
            resultMap = bomCalculator(resultMap);
        }
        //已经拿到了处理后的map
        System.out.println("处理后:" + resultMap);
        //扣减最终数量数量map
            DEDUCTION_CALCULATED_MAP.remove(saleOrder.getName());
        return resultMap;
    }


    /**
     * 传入订单的初始需求 进行产品以及bom的拆分
     * @param productAndQuantNeededMap
     * @return map类型，key 为产品id  value 为在这个订单中所有涉及到的产品要扣减的数量
     */
    public Map<Long, Long> bomCalculator(Map<Long, Long> productAndQuantNeededMap) {
        MrpBomServiceImpl mrpBomBean = SpringUtils.getBean(MrpBomServiceImpl.class);
        StockQuantServiceImpl stockQuantBean = SpringUtils.getBean(StockQuantServiceImpl.class);
        StockProductEntityServiceImpl entityBean = SpringUtils.getBean(StockProductEntityServiceImpl.class);
        //使用map（一个中转） 来存储扣减数量：key为产品id  value为扣减数量
        Map<Long, Long> deductMap = new HashMap<>();


        //查询对应的数据库数量数据,使用循环
        for (Long aLong : productAndQuantNeededMap.keySet()) {
            //取出每一个key(产品id) 并且进行判断，
            Long productId = aLong;
            //bom查询，没有bom就直接计算扣减数量
            MrpBom mrpBom = mrpBomBean.selectMrpBomByStockTemplateId(entityBean.selectStockProductEntityById(productId).getProductTmplId());
            //说明该产品没有bom 可以直接扣除 并且扣除数量可以大于库存内的实际数量
            if (mrpBom == null || mrpBom.getMrpBomParts().isEmpty()) {
                deductMap.put(productId, productAndQuantNeededMap.get(productId));
            }
            //在这个if中，是订单产品有bom的情况，首先需要确认这个包含bom的产品 库存内是否有库存，如果大于0 先扣掉一部分，把不足的部分记下来
            if (mrpBom != null) {
                //首先检测库存，如果有库存：
                // 1.库存足够，可以直接扣除,不涉及其bom子项的扣减问题
                StockQuant stockQuant = stockQuantBean.selectStockQuantPhysical(productId);
                if (stockQuant.getQuantity() >= productAndQuantNeededMap.get(productId)) {
                    deductMap.put(productId, productAndQuantNeededMap.get(productId));
                } else {
                    //2.库存不足 只能扣除库存有的部分
                    // 2.1.欠缺的部分需要保存下来 用于计算其bom子项的扣除数量,在此需要考虑库存是否为负数的问题
                    Long lackNum = 0l;
                    if (stockQuant.getQuantity() >= 0) {
                        deductMap.put(productId, stockQuant.getQuantity());
                        lackNum = stockQuant.getQuantity() - productAndQuantNeededMap.get(productId);
                        //出现了库存数量不足的问题 则找到物料清单
                        List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
                        for (MrpBomPart mrpBomPart : mrpBomParts) {
//                            productAndQuantNeededMap.put(mrpBomPart.getProductId(),lackNum * mrpBomPart.getProductQty());
                            deductMap.put(mrpBomPart.getProductId(), Math.abs(lackNum) * mrpBomPart.getProductQty());
                        }
                    } else {
                        //库存数量为负 则该产品不能扣除 扣除数量为0
                        deductMap.put(productId, 0l);
                        lackNum = stockQuant.getQuantity() - productAndQuantNeededMap.get(productId);
                        List<MrpBomPart> mrpBomParts = mrpBom.getMrpBomParts();
                        for (MrpBomPart mrpBomPart : mrpBomParts) {
//                            productAndQuantNeededMap.put(mrpBomPart.getProductId(),Math.abs(lackNum) * mrpBomPart.getProductQty());
                            deductMap.put(mrpBomPart.getProductId(), Math.abs(lackNum) * mrpBomPart.getProductQty());
                        }
                    }
                    System.out.println(productId + "产品欠缺数量：" + lackNum);
                    //欠缺数量是在该区域生成的，用于计算bom扣减数量的方法添加在此
                    //可以获取的数据为： 已经存放扣减记录的deductMap，新的扣减信息应该也存入此map，产品的欠缺数量：lackNum
                }
            }
        }
        return deductMap;
    }


    /**
     * 判断两个Map是否完全相同
     *
     * @param map1 第一个Map
     * @param map2 第二个Map
     * @param <K>  Map中键的类型
     * @param <V>  Map中值的类型
     * @return 如果两个Map完全相同返回true，否则返回false
     */
    public static <K, V> boolean isMapEqual(Map<K, V> map1, Map<K, V> map2) {
        if (map1 == map2) {
            return true;
        }
        if (map1 == null || map2 == null || map1.size() != map2.size()) {
            return false;
        }

        for (Map.Entry<K, V> entry : map1.entrySet()) {
            K key = entry.getKey();
            V value1 = entry.getValue();
            V value2 = map2.get(key);

            if (!Objects.equals(value1, value2)) {
                return false;
            }
        }
        return true;
    }
}