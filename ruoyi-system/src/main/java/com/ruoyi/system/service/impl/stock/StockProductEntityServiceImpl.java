package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockProductEntityMapper;
import com.ruoyi.system.domain.stock.StockProductEntity;
import com.ruoyi.system.service.IStockProductEntityService;

/**
 * 产品实体Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Service
public class StockProductEntityServiceImpl implements IStockProductEntityService 
{
    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    /**
     * 查询产品实体
     * 
     * @param id 产品实体主键
     * @return 产品实体
     */
    @Override
    public StockProductEntity selectStockProductEntityById(Long id)
    {
        return stockProductEntityMapper.selectStockProductEntityById(id);
    }

    /**
     * 查询产品实体列表
     * 
     * @param stockProductEntity 产品实体
     * @return 产品实体
     */
    @Override
    public List<StockProductEntity> selectStockProductEntityList(StockProductEntity stockProductEntity)
    {
        return stockProductEntityMapper.selectStockProductEntityList(stockProductEntity);
    }

    /**
     * 新增产品实体
     * 
     * @param stockProductEntity 产品实体
     * @return 结果
     */
    @Override
    public int insertStockProductEntity(StockProductEntity stockProductEntity)
    {
        return stockProductEntityMapper.insertStockProductEntity(stockProductEntity);
    }

    /**
     * 修改产品实体
     * 
     * @param stockProductEntity 产品实体
     * @return 结果
     */
    @Override
    public int updateStockProductEntity(StockProductEntity stockProductEntity)
    {
        return stockProductEntityMapper.updateStockProductEntity(stockProductEntity);
    }

    /**
     * 批量删除产品实体
     * 
     * @param ids 需要删除的产品实体主键
     * @return 结果
     */
    @Override
    public int deleteStockProductEntityByIds(Long[] ids)
    {
        return stockProductEntityMapper.deleteStockProductEntityByIds(ids);
    }

    /**
     * 删除产品实体信息
     * 
     * @param id 产品实体主键
     * @return 结果
     */
    @Override
    public int deleteStockProductEntityById(Long id)
    {
        return stockProductEntityMapper.deleteStockProductEntityById(id);
    }
}
