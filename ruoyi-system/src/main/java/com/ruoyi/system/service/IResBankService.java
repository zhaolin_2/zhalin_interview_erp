package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.sale.ResBank;

/**
 * 银行管理Service接口
 * 
 * @author Lin
 * @date 2023-04-03
 */
public interface IResBankService 
{
    /**
     * 查询银行管理
     * 
     * @param id 银行管理主键
     * @return 银行管理
     */
    public ResBank selectResBankById(Long id);

    /**
     * 查询银行管理列表
     * 
     * @param resBank 银行管理
     * @return 银行管理集合
     */
    public List<ResBank> selectResBankList(ResBank resBank);

    /**
     * 新增银行管理
     * 
     * @param resBank 银行管理
     * @return 结果
     */
    public int insertResBank(ResBank resBank);

    /**
     * 修改银行管理
     * 
     * @param resBank 银行管理
     * @return 结果
     */
    public int updateResBank(ResBank resBank);

    /**
     * 批量删除银行管理
     * 
     * @param ids 需要删除的银行管理主键集合
     * @return 结果
     */
    public int deleteResBankByIds(Long[] ids);

    /**
     * 删除银行管理信息
     * 
     * @param id 银行管理主键
     * @return 结果
     */
    public int deleteResBankById(Long id);
}
