package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.domain.stock.vo.*;

/**
 * 库存Service接口
 * 
 * @author Lin
 * @date 2023-03-30
 */
public interface IStockProductTemplateService
{
    /**
     * 查询库存
     * 
     * @param id 库存主键
     * @return 库存
     */
    public StockProductTemplate selectStockProductById(Long id);

    /**
     * 查询库存列表
     * 
     * @param stockProduct 库存
     * @return 库存集合
     */
    public List<StockProductTemplate> selectStockProductList(StockProductTemplate stockProduct);

    /**
     * 新增库存
     * 
     * @param stockProductVo 库存
     * @return 结果
     */
    public int insertStockProduct(StockProductTemplateVo stockProductVo);

    /**
     * 修改库存
     * 
     * @param stockProduct 库存
     * @return 结果
     */
    public int updateStockProduct(StockProductTemplate stockProduct);

    /**
     * 批量删除库存
     * 
     * @param ids 需要删除的库存主键集合
     * @return 结果
     */
    public int deleteStockProductByIds(Long[] ids);

    /**
     * 删除库存信息
     * 
     * @param id 库存主键
     * @return 结果
     */
    public int deleteStockProductById(Long id);

    List<StockTemplateMessage> getStockMessageByStockId(Long id);

    StockReorderRule getStockReorderRulesByStockId(Long stockId);

    List<PurchaseOrderPart> selectStockPurchaseHistoryByStockId(Long stockId);

    List<SaleOrderPart> selectStockSaleHistoryByStockId(Long stockId);

    List<MrpBomPart> selectStockBomPartByStockId(Long stockId);

    List<MrpBomInStockVo> selectStockBomBelongTo(Long stockId);

    int insertStockProductTemplateSupplier(StockProductSupplierVo stockProductSupplierVo);

    int deleteStockProductSupplierInfoById(Long supplierInfoId);

    int stockTemplateReorderRuleAdd(StockReorderRuleVo stockReorderRuleVo);

    int stockTemplateReorderRuleUpdate(StockReorderRuleVo stockReorderRuleVo);

    int stockTemplateReorderRuleDelete(Long stockId);

    int stockTemplateBomPartsAdd(MrpBomPartVo mrpBomPartVo);

    StockProductTemplate copyStockTemplateProductByStockId(Long stockId);

    void stockProductTemplateTranslation(StockProductTemplate stockProductTemplate);

    StockQuant selectStockQuantByStockTemplateId(Long stockId);

    Map<Long,Long> takeSaleOrderApartProductNeeded(Long id);

}
