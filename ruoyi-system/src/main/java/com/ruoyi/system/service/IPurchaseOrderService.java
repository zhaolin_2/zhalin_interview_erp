package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.domain.stock.StockProductTemplate;

/**
 * 采购Service接口
 * 
 * @author Lin
 * @date 2023-04-06
 */
public interface IPurchaseOrderService 
{
    /**
     * 查询采购
     * 
     * @param id 采购主键
     * @return 采购
     */
    public PurchaseOrder selectPurchaseOrderById(Long id);

    /**
     * 查询采购列表
     * 
     * @param purchaseOrder 采购
     * @return 采购集合
     */
    public List<PurchaseOrder> selectPurchaseOrderList(PurchaseOrder purchaseOrder);

    /**
     * 新增采购
     * 
     * @param purchaseOrder 采购
     * @return 结果
     */
    public int insertPurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 修改采购
     * 
     * @param purchaseOrder 采购
     * @return 结果
     */
    public int updatePurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 批量删除采购
     * 
     * @param ids 需要删除的采购主键集合
     * @return 结果
     */
    public int deletePurchaseOrderByIds(Long[] ids);

    /**
     * 删除采购信息
     * 
     * @param id 采购主键
     * @return 结果
     */
    public int deletePurchaseOrderById(Long id);

    List<PurchaseOrderPart> GetOrderPartMsg(Long orderId);

    /**
     * 根据采购订单编号 获取供应商详情
     * @param orderId
     * @return
     */
    List getOrderPartnerMsg(Long orderId);

    String sequenceHandle();

    /**
     * 采购订单在编辑过程中的删除操作
     * @param sequenceId 组件在该订单中的序列
     * @param isInsert 编辑时还是更新时
     * @return
     */
    PurchaseOrder purchaseOrderPartDelete(Long sequenceId, String isInsert);

    PurchaseOrder purchaseOrderPartUpdate(PurchaseOrderPart purchaseOrderPart);

    PurchaseOrderPart getPurchaseOrderPartFromStockProductTemplateId(Long stockId);

    PurchaseOrder writePurchaseOrder(PurchaseOrder purchaseOrder) throws IllegalAccessException;

    List<StockProductTemplate> selectPurchaseOrderStockListAvailable(String isInsert,String name);

    String purchaseOrderPartUpdateAbandon();

    PurchaseOrderPart purchaseOrderPartInfo(Long sequenceId, String isInsert);

    void draftPurchaseOrderConfirmToPurchaseOrder(Long id);

    void purchaseOrderCancel(Long id);

    void purchaseOrderTranslation(PurchaseOrder purchaseOrder);

    PurchaseOrder selectPurchaseOrderByName(String purhaseOrderName);

    /**
     * 自动生成采购询价单
     * @param purchaseOrderPartIds 需要采购的订单组件
     * @return
     */
//    PurchaseOrder purchaseOrderGenerate(List purchaseOrderPartIds);
}
