package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockMoveLineMapper;
import com.ruoyi.system.domain.stock.StockMoveLine;
import com.ruoyi.system.service.IStockMoveLineService;

/**
 * 库存移动记录Service业务层处理
 * 
 * @author Lin
 * @date 2023-06-05
 */
@Service
public class StockMoveLineServiceImpl implements IStockMoveLineService 
{
    @Autowired
    private StockMoveLineMapper stockMoveLineMapper;

    /**
     * 查询库存移动记录
     * 
     * @param id 库存移动记录主键
     * @return 库存移动记录
     */
    @Override
    public StockMoveLine selectStockMoveLineById(Long id)
    {
        return stockMoveLineMapper.selectStockMoveLineById(id);
    }

    /**
     * 查询库存移动记录列表
     * 
     * @param stockMoveLine 库存移动记录
     * @return 库存移动记录
     */
    @Override
    public List<StockMoveLine> selectStockMoveLineList(StockMoveLine stockMoveLine)
    {
        return stockMoveLineMapper.selectStockMoveLineList(stockMoveLine);
    }

    /**
     * 新增库存移动记录
     * 
     * @param stockMoveLine 库存移动记录
     * @return 结果
     */
    @Override
    public int insertStockMoveLine(StockMoveLine stockMoveLine)
    {
        return stockMoveLineMapper.insertStockMoveLine(stockMoveLine);
    }

    /**
     * 修改库存移动记录
     * 
     * @param stockMoveLine 库存移动记录
     * @return 结果
     */
    @Override
    public int updateStockMoveLine(StockMoveLine stockMoveLine)
    {
        return stockMoveLineMapper.updateStockMoveLine(stockMoveLine);
    }

    /**
     * 批量删除库存移动记录
     * 
     * @param ids 需要删除的库存移动记录主键
     * @return 结果
     */
    @Override
    public int deleteStockMoveLineByIds(Long[] ids)
    {
        return stockMoveLineMapper.deleteStockMoveLineByIds(ids);
    }

    /**
     * 删除库存移动记录信息
     * 
     * @param id 库存移动记录主键
     * @return 结果
     */
    @Override
    public int deleteStockMoveLineById(Long id)
    {
        return stockMoveLineMapper.deleteStockMoveLineById(id);
    }
}
