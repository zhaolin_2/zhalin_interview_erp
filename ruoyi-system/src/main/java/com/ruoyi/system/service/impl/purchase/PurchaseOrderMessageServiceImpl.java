package com.ruoyi.system.service.impl.purchase;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PurchaseOrderMessageMapper;
import com.ruoyi.system.domain.purchase.PurchaseOrderMessage;
import com.ruoyi.system.service.IPurchaseOrderMessageService;

/**
 * 采购订单日志Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-06
 */
@Service
public class PurchaseOrderMessageServiceImpl implements IPurchaseOrderMessageService 
{
    @Autowired
    private PurchaseOrderMessageMapper purchaseOrderMessageMapper;

    /**
     * 查询采购订单日志
     * 
     * @param id 采购订单日志主键
     * @return 采购订单日志
     */
    @Override
    public PurchaseOrderMessage selectPurchaseOrderMessageById(Long id)
    {
        return purchaseOrderMessageMapper.selectPurchaseOrderMessageById(id);
    }

    /**
     * 查询采购订单日志列表
     * 
     * @param purchaseOrderMessage 采购订单日志
     * @return 采购订单日志
     */
    @Override
    public List<PurchaseOrderMessage> selectPurchaseOrderMessageList(PurchaseOrderMessage purchaseOrderMessage)
    {
        return purchaseOrderMessageMapper.selectPurchaseOrderMessageList(purchaseOrderMessage);
    }

    /**
     * 新增采购订单日志
     * 
     * @param purchaseOrderMessage 采购订单日志
     * @return 结果
     */
    @Override
    public int insertPurchaseOrderMessage(PurchaseOrderMessage purchaseOrderMessage)
    {
        return purchaseOrderMessageMapper.insertPurchaseOrderMessage(purchaseOrderMessage);
    }

    /**
     * 修改采购订单日志
     * 
     * @param purchaseOrderMessage 采购订单日志
     * @return 结果
     */
    @Override
    public int updatePurchaseOrderMessage(PurchaseOrderMessage purchaseOrderMessage)
    {
        return purchaseOrderMessageMapper.updatePurchaseOrderMessage(purchaseOrderMessage);
    }

    /**
     * 批量删除采购订单日志
     * 
     * @param ids 需要删除的采购订单日志主键
     * @return 结果
     */
    @Override
    public int deletePurchaseOrderMessageByIds(Long[] ids)
    {
        return purchaseOrderMessageMapper.deletePurchaseOrderMessageByIds(ids);
    }

    /**
     * 删除采购订单日志信息
     * 
     * @param id 采购订单日志主键
     * @return 结果
     */
    @Override
    public int deletePurchaseOrderMessageById(Long id)
    {
        return purchaseOrderMessageMapper.deletePurchaseOrderMessageById(id);
    }

    /**
     * 根据订单id 查询采购订单详情
     * @param purchaseOrderId
     * @return
     */
    @Override
    public List<PurchaseOrderMessage> getOrderMessageByOrderId(Long purchaseOrderId) {
        return purchaseOrderMessageMapper.selectPurchaseOrderMessageByOrderId(purchaseOrderId);
    }
}
