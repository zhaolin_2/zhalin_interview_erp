package com.ruoyi.system.service.impl.account;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AccountPaymentMapper;
import com.ruoyi.system.domain.account.AccountPayment;
import com.ruoyi.system.service.IAccountPaymentService;

/**
 * 付款Service业务层处理
 * 
 * @author Lin
 * @date 2023-05-31
 */
@Service
public class AccountPaymentServiceImpl implements IAccountPaymentService 
{
    @Autowired
    private AccountPaymentMapper accountPaymentMapper;

    /**
     * 查询付款
     * 
     * @param id 付款主键
     * @return 付款
     */
    @Override
    public AccountPayment selectAccountPaymentById(Long id)
    {
        return accountPaymentMapper.selectAccountPaymentById(id);
    }

    /**
     * 查询付款列表
     * 
     * @param accountPayment 付款
     * @return 付款
     */
    @Override
    public List<AccountPayment> selectAccountPaymentList(AccountPayment accountPayment)
    {
        return accountPaymentMapper.selectAccountPaymentList(accountPayment);
    }

    /**
     * 新增付款
     * 
     * @param accountPayment 付款
     * @return 结果
     */
    @Override
    public int insertAccountPayment(AccountPayment accountPayment)
    {
        return accountPaymentMapper.insertAccountPayment(accountPayment);
    }

    /**
     * 修改付款
     * 
     * @param accountPayment 付款
     * @return 结果
     */
    @Override
    public int updateAccountPayment(AccountPayment accountPayment)
    {
        return accountPaymentMapper.updateAccountPayment(accountPayment);
    }

    /**
     * 批量删除付款
     * 
     * @param ids 需要删除的付款主键
     * @return 结果
     */
    @Override
    public int deleteAccountPaymentByIds(Long[] ids)
    {
        return accountPaymentMapper.deleteAccountPaymentByIds(ids);
    }

    /**
     * 删除付款信息
     * 
     * @param id 付款主键
     * @return 结果
     */
    @Override
    public int deleteAccountPaymentById(Long id)
    {
        return accountPaymentMapper.deleteAccountPaymentById(id);
    }
}
