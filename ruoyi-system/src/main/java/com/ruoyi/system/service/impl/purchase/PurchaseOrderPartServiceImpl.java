package com.ruoyi.system.service.impl.purchase;

import java.util.List;

import com.ruoyi.system.domain.stock.StockProductEntity;
import com.ruoyi.system.domain.stock.StockProductTemplate;
import com.ruoyi.system.mapper.StockProductEntityMapper;
import com.ruoyi.system.mapper.StockProductTemplateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PurchaseOrderPartMapper;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.service.IPurchaseOrderPartService;

/**
 * 采购订单组件Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-06
 */
@Service
public class PurchaseOrderPartServiceImpl implements IPurchaseOrderPartService 
{
    @Autowired
    private PurchaseOrderPartMapper purchaseOrderPartMapper;


    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    @Autowired
    StockProductTemplateMapper stockProductTemplateMapper;

    /**
     * 查询采购订单组件
     * 
     * @param id 采购订单组件主键
     * @return 采购订单组件
     */
    @Override
    public PurchaseOrderPart selectPurchaseOrderPartById(Long id)
    {
        return purchaseOrderPartMapper.selectPurchaseOrderPartById(id);
    }

    /**
     * 查询采购订单组件列表
     * 
     * @param purchaseOrderPart 采购订单组件
     * @return 采购订单组件
     */
    @Override
    public List<PurchaseOrderPart> selectPurchaseOrderPartList(PurchaseOrderPart purchaseOrderPart)
    {
        return purchaseOrderPartMapper.selectPurchaseOrderPartList(purchaseOrderPart);
    }

    /**
     * 新增采购订单组件
     * 
     * @param purchaseOrderPart 采购订单组件
     * @return 结果
     */
    @Override
    public int insertPurchaseOrderPart(PurchaseOrderPart purchaseOrderPart)
    {
        return purchaseOrderPartMapper.insertPurchaseOrderPart(purchaseOrderPart);
    }

    /**
     * 修改采购订单组件
     * 
     * @param purchaseOrderPart 采购订单组件
     * @return 结果
     */
    @Override
    public int updatePurchaseOrderPart(PurchaseOrderPart purchaseOrderPart)
    {
        return purchaseOrderPartMapper.updatePurchaseOrderPart(purchaseOrderPart);
    }

    /**
     * 批量删除采购订单组件
     * 
     * @param ids 需要删除的采购订单组件主键
     * @return 结果
     */
    @Override
    public int deletePurchaseOrderPartByIds(Long[] ids)
    {
        return purchaseOrderPartMapper.deletePurchaseOrderPartByIds(ids);
    }

    /**
     * 删除采购订单组件信息
     * 
     * @param id 采购订单组件主键
     * @return 结果
     */
    @Override
    public int deletePurchaseOrderPartById(Long id)
    {
        return purchaseOrderPartMapper.deletePurchaseOrderPartById(id);
    }

    /**
     * 订单组件名字纠正
     * @return
     */
    @Override
    public int purchaseOrderPartNamePut(){
        List<PurchaseOrderPart> purchaseOrderParts = purchaseOrderPartMapper.selectPurchaseOrderPartList(new PurchaseOrderPart());
        for (PurchaseOrderPart purchaseOrderPart : purchaseOrderParts) {
            purchaseOrderPart.setPartMessage(purchaseOrderPart.getName());
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(purchaseOrderPart.getProductId());
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
            purchaseOrderPart.setName(stockProductTemplate.getName());
            purchaseOrderPartMapper.updatePurchaseOrderPart(purchaseOrderPart);
        }
        return 1;
    }
}
