package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MrpBomPartMapper;
import com.ruoyi.system.domain.stock.MrpBomPart;
import com.ruoyi.system.service.IMrpBomPartService;

/**
 * 物料清单组件Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Service
public class MrpBomPartServiceImpl implements IMrpBomPartService 
{
    @Autowired
    private MrpBomPartMapper mrpBomPartMapper;

    /**
     * 查询物料清单组件
     * 
     * @param id 物料清单组件主键
     * @return 物料清单组件
     */
    @Override
    public MrpBomPart selectMrpBomPartById(Long id)
    {
        return mrpBomPartMapper.selectMrpBomPartById(id);
    }

    /**
     * 查询物料清单组件列表
     * 
     * @param mrpBomPart 物料清单组件
     * @return 物料清单组件
     */
    @Override
    public List<MrpBomPart> selectMrpBomPartList(MrpBomPart mrpBomPart)
    {
        return mrpBomPartMapper.selectMrpBomPartList(mrpBomPart);
    }

    /**
     * 新增物料清单组件
     * 
     * @param mrpBomPart 物料清单组件
     * @return 结果
     */
    @Override
    public int insertMrpBomPart(MrpBomPart mrpBomPart)
    {
        return mrpBomPartMapper.insertMrpBomPart(mrpBomPart);
    }

    /**
     * 修改物料清单组件
     * 
     * @param mrpBomPart 物料清单组件
     * @return 结果
     */
    @Override
    public int updateMrpBomPart(MrpBomPart mrpBomPart)
    {
        return mrpBomPartMapper.updateMrpBomPart(mrpBomPart);
    }

    /**
     * 批量删除物料清单组件
     * 
     * @param ids 需要删除的物料清单组件主键
     * @return 结果
     */
    @Override
    public int deleteMrpBomPartByIds(Long[] ids)
    {
        return mrpBomPartMapper.deleteMrpBomPartByIds(ids);
    }

    /**
     * 删除物料清单组件信息
     * 
     * @param id 物料清单组件主键
     * @return 结果
     */
    @Override
    public int deleteMrpBomPartById(Long id)
    {
        return mrpBomPartMapper.deleteMrpBomPartById(id);
    }
}
