package com.ruoyi.system.service.impl.stock;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockInventoryLineMapper;
import com.ruoyi.system.domain.stock.StockInventoryLine;
import com.ruoyi.system.service.IStockInventoryLineService;

/**
 * InventoryLineService业务层处理
 * 
 * @author Lin
 * @date 2023-05-31
 */
@Service
public class StockInventoryLineServiceImpl implements IStockInventoryLineService 
{
    @Autowired
    private StockInventoryLineMapper stockInventoryLineMapper;

    /**
     * 查询InventoryLine
     * 
     * @param id InventoryLine主键
     * @return InventoryLine
     */
    @Override
    public StockInventoryLine selectStockInventoryLineById(Long id)
    {
        return stockInventoryLineMapper.selectStockInventoryLineById(id);
    }

    /**
     * 查询InventoryLine列表
     * 
     * @param stockInventoryLine InventoryLine
     * @return InventoryLine
     */
    @Override
    public List<StockInventoryLine> selectStockInventoryLineList(StockInventoryLine stockInventoryLine)
    {
        return stockInventoryLineMapper.selectStockInventoryLineList(stockInventoryLine);
    }

    /**
     * 新增InventoryLine
     * 
     * @param stockInventoryLine InventoryLine
     * @return 结果
     */
    @Override
    public int insertStockInventoryLine(StockInventoryLine stockInventoryLine)
    {
        return stockInventoryLineMapper.insertStockInventoryLine(stockInventoryLine);
    }

    /**
     * 修改InventoryLine
     * 
     * @param stockInventoryLine InventoryLine
     * @return 结果
     */
    @Override
    public int updateStockInventoryLine(StockInventoryLine stockInventoryLine)
    {
        return stockInventoryLineMapper.updateStockInventoryLine(stockInventoryLine);
    }

    /**
     * 批量删除InventoryLine
     * 
     * @param ids 需要删除的InventoryLine主键
     * @return 结果
     */
    @Override
    public int deleteStockInventoryLineByIds(Long[] ids)
    {
        return stockInventoryLineMapper.deleteStockInventoryLineByIds(ids);
    }

    /**
     * 删除InventoryLine信息
     * 
     * @param id InventoryLine主键
     * @return 结果
     */
    @Override
    public int deleteStockInventoryLineById(Long id)
    {
        return stockInventoryLineMapper.deleteStockInventoryLineById(id);
    }
}
