package com.ruoyi.system.service.impl.sale;

import java.util.List;

import com.ruoyi.system.domain.stock.StockProductEntity;
import com.ruoyi.system.domain.stock.StockProductTemplate;
import com.ruoyi.system.mapper.StockProductEntityMapper;
import com.ruoyi.system.mapper.StockProductTemplateMapper;
import com.ruoyi.system.mapper.StockTemplateMessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SaleOrderPartMapper;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.service.ISaleOrderPartService;

/**
 * 订单组件Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Service
public class SaleOrderPartServiceImpl implements ISaleOrderPartService 
{
    @Autowired
    private SaleOrderPartMapper saleOrderPartMapper;

    @Autowired
    private StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    /**
     * 查询订单组件
     * 
     * @param id 订单组件主键
     * @return 订单组件
     */
    @Override
    public SaleOrderPart selectSaleOrderPartById(Long id)
    {
        return saleOrderPartMapper.selectSaleOrderPartById(id);
    }

    /**
     * 查询订单组件列表
     * 
     * @param saleOrderPart 订单组件
     * @return 订单组件
     */
    @Override
    public List<SaleOrderPart> selectSaleOrderPartList(SaleOrderPart saleOrderPart)
    {
        return saleOrderPartMapper.selectSaleOrderPartList(saleOrderPart);
    }

    /**
     * 新增订单组件
     * 
     * @param saleOrderPart 订单组件
     * @return 结果
     */
    @Override
    public int insertSaleOrderPart(SaleOrderPart saleOrderPart)
    {
        return saleOrderPartMapper.insertSaleOrderPart(saleOrderPart);
    }

    /**
     * 修改订单组件
     * 
     * @param saleOrderPart 订单组件
     * @return 结果
     */
    @Override
    public int updateSaleOrderPart(SaleOrderPart saleOrderPart)
    {
        return saleOrderPartMapper.updateSaleOrderPart(saleOrderPart);
    }

    /**
     * 批量删除订单组件
     * 
     * @param ids 需要删除的订单组件主键
     * @return 结果
     */
    @Override
    public int deleteSaleOrderPartByIds(Long[] ids)
    {
        return saleOrderPartMapper.deleteSaleOrderPartByIds(ids);
    }

    /**
     * 删除订单组件信息
     * 
     * @param id 订单组件主键
     * @return 结果
     */
    @Override
    public int deleteSaleOrderPartById(Long id)
    {
        return saleOrderPartMapper.deleteSaleOrderPartById(id);
    }


    /**
     * 订单组件名字纠正
     * @return
     */
    @Override
    public int saleOrderPartNamePut(){
        List<SaleOrderPart> saleOrderParts = saleOrderPartMapper.selectSaleOrderPartList(new SaleOrderPart());
        for (SaleOrderPart saleOrderPart : saleOrderParts) {
            saleOrderPart.setPartMessage(saleOrderPart.getName());
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(saleOrderPart.getProductId());
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
            saleOrderPart.setName(stockProductTemplate.getName());
            saleOrderPartMapper.updateSaleOrderPart(saleOrderPart);
        }
        return 1;
    }
}
