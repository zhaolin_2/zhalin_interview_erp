package com.ruoyi.system.service.impl.stock;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.service.IStockInventoryService;

/**
 * InventoryService业务层处理
 * 
 * @author Lin
 * @date 2023-05-31
 */
@Service
public class StockInventoryServiceImpl implements IStockInventoryService 
{
    @Autowired
    private StockInventoryMapper stockInventoryMapper;

    @Autowired
    private StockInventoryLineMapper stockInventoryLineMapper;

    @Autowired
    private StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    @Autowired
    private StockQuantMapper stockQuantMapper;




    private static Map<Long, StockInventory> INSERT_INVENTORY_TEMP = new HashMap<>();

    private static Map<Long, StockInventory> UPDATE_INVENTORY_TEMP = new HashMap<>();

    /**
     * 查询Inventory
     * 
     * @param id Inventory主键
     * @return Inventory
     */
    @Override
    public StockInventory selectStockInventoryById(Long id)
    {
        StockInventory stockInventory = stockInventoryMapper.selectStockInventoryById(id);
        List<StockInventoryLine> stockInventoryLines = stockInventoryLineMapper.selectStockInventoryLineByInInventoryId(id);
        for (int i = 0; i < stockInventoryLines.size(); i++) {
            StockInventoryLine stockInventoryLine =  stockInventoryLines.get(i);
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityById(stockInventoryLine.getProductId());
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductEntity.getProductTmplId());
            stockInventoryLine.setProductName(stockProductTemplate.getName());
            stockInventoryLine.setProductTemplateId(stockProductTemplate.getId());
            stockInventoryLine.setHandleNum(stockInventoryLine.getProductQty()-stockInventoryLine.getTheoreticalQty());
        }
        stockInventory.setStockInventoryLines(stockInventoryLines);
        return stockInventory;
    }

    /**
     * 查询Inventory列表
     * 
     * @param stockInventory Inventory
     * @return Inventory
     */
    @Override
    public List<StockInventory> selectStockInventoryList(StockInventory stockInventory)
    {
        return stockInventoryMapper.selectStockInventoryList(stockInventory);
    }

    /**
     * 新增Inventory
     * 
     * @param stockInventory Inventory
     * @return 结果
     */
    @Override
    public int insertStockInventory(StockInventory stockInventory)
    {
        return stockInventoryMapper.insertStockInventory(stockInventory);
    }

    /**
     * 修改Inventory
     * 
     * @param stockInventory Inventory
     * @return 结果
     */
    @Override
    public int updateStockInventory(StockInventory stockInventory)
    {
        return stockInventoryMapper.updateStockInventory(stockInventory);
    }

    /**
     * 批量删除Inventory
     * 
     * @param ids 需要删除的Inventory主键
     * @return 结果
     */
    @Override
    public int deleteStockInventoryByIds(Long[] ids)
    {
        return stockInventoryMapper.deleteStockInventoryByIds(ids);
    }

    /**
     * 删除Inventory信息
     * 
     * @param id Inventory主键
     * @return 结果
     */
    @Override
    public int deleteStockInventoryById(Long id)
    {
        return stockInventoryMapper.deleteStockInventoryById(id);
    }

}
