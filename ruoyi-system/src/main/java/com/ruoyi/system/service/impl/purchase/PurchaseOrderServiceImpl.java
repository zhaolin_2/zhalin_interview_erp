package com.ruoyi.system.service.impl.purchase;

import java.math.BigDecimal;
import java.util.*;

import cn.hutool.core.convert.Convert;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.impl.sale.ResPartnerServiceImpl;
import com.ruoyi.system.service.impl.stock.StockPickingServiceImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.service.IPurchaseOrderService;

/**
 * 采购Service业务层处理
 *
 * @author Lin
 * @date 2023-04-06
 */
@Service
public class PurchaseOrderServiceImpl implements IPurchaseOrderService {
    @Autowired
    private PurchaseOrderMapper purchaseOrderMapper;

    @Autowired
    private PurchaseOrderPartMapper purchaseOrderPartMapper;

    @Autowired
    private ResPartnerMapper resPartnerMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    @Autowired
    private StockReorderRuleMapper stockReorderRuleMapper;

    @Autowired
    private StockQuantMapper stockQuantMapper;

    @Autowired
    private StockProductSupplierinfoMapper stockProductSupplierinfoMapper;

    @Autowired
    private StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    private SysUserMapper sysUserMapper;


    private static Map<Long, PurchaseOrder> INSERT_PURCHASE_ORDERS_TEMP = new HashMap<>();

    private static Map<Long, PurchaseOrder> UPDATE_PURCHASE_ORDERS_TEMP = new HashMap<>();


    /**
     * 查询采购订单详细信息（包含订单组件信息）
     *
     * @param id 采购主键
     * @return 采购
     */
    @Override
    public PurchaseOrder selectPurchaseOrderById(Long id) {
        PurchaseOrder purchaseOrder = purchaseOrderMapper.selectPurchaseOrderById(id);
        try {
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrderPartMapper.selectPurchaseOrderPartsByOrderId(purchaseOrder.getId());
            for (int i = 0; i < purchaseOrderParts.size(); i++) {
                PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
                purchaseOrderPart.setProductTemplateId(stockProductEntityMapper.selectStockProductEntityById(purchaseOrderPart.getProductId()).getProductTmplId());
            }
            purchaseOrder.setPurchaseOrderParts(purchaseOrderParts);
        } catch (NullPointerException e) {
            purchaseOrder.setPurchaseOrderParts(null);
        }
        purchaseOrderTranslation(purchaseOrder);
        UPDATE_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), purchaseOrder);
        return purchaseOrder;
    }

    /**
     * 查询采购列表
     *
     * @param purchaseOrder 采购
     * @return 采购
     */
    @Override
    public List<PurchaseOrder> selectPurchaseOrderList(PurchaseOrder purchaseOrder) {
        return purchaseOrderMapper.selectPurchaseOrderList(purchaseOrder);
    }

    /**
     * 新增
     *
     * @param purchaseOrder 采购
     * @return 结果
     */
    @Override
    public int insertPurchaseOrder(PurchaseOrder purchaseOrder) {
        //写入对象到数据库，并且删除暂存在map内的内容
        purchaseOrder.setDateOrder(new Date());
        purchaseOrder.setCurrencyId(8l);
        purchaseOrder.setCreateUid(SecurityUtils.getUserId());
        purchaseOrder.setName("测试");
        purchaseOrder.setCreateDate(new Date());
        purchaseOrder.setState("draft");
        purchaseOrderMapper.insertPurchaseOrder(purchaseOrder);
        List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
        for (int i = 0; i < purchaseOrderParts.size(); i++) {
            PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
            purchaseOrderPart.setOrderId(purchaseOrder.getId());
            StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(purchaseOrderPart.getProductTemplateId());
            purchaseOrderPart.setName(stockProductTemplate.getName());
            purchaseOrderPart.setPartnerId(purchaseOrder.getPartnerId());
            purchaseOrderPart.setProductId(stockProductEntityMapper.selectStockProductEntityByTemplateId(purchaseOrderPart.getProductTemplateId()).getId());
            purchaseOrderPartMapper.insertPurchaseOrderPart(purchaseOrderPart);
        }
        INSERT_PURCHASE_ORDERS_TEMP.remove(SecurityUtils.getUserId());
        return 1;
    }

    /**
     * 修改采购订单
     *
     * @param purchaseOrder 采购
     * @return 结果
     */
    @Override
    public int updatePurchaseOrder(PurchaseOrder purchaseOrder) {
        List<PurchaseOrderPart> purchaseOrderPartsOld = purchaseOrderPartMapper.selectPurchaseOrderPartsByOrderId(purchaseOrder.getId());
        for (int i = 0; i < purchaseOrderPartsOld.size(); i++) {
            PurchaseOrderPart purchaseOrderPart = purchaseOrderPartsOld.get(i);
            purchaseOrderPartMapper.deletePurchaseOrderPartById(purchaseOrderPart.getId());
        }
        purchaseOrder.setWriteUid(SecurityUtils.getUserId());
        purchaseOrder.setWriteDate(new Date());
        purchaseOrderMapper.updatePurchaseOrder(purchaseOrder);
        List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
        int num = 0;
        for (int i = 0; i < purchaseOrderParts.size(); i++) {
            PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
            purchaseOrderPart.setOrderId(purchaseOrder.getId());
            num += purchaseOrderPartMapper.insertPurchaseOrderPart(purchaseOrderPart);
        }

        UPDATE_PURCHASE_ORDERS_TEMP.remove(SecurityUtils.getUserId());
        return num;
    }

    /**
     * 批量删除采购
     *
     * @param ids 需要删除的采购主键
     * @return 结果
     */
    @Override
    public int deletePurchaseOrderByIds(Long[] ids) {
        for (Long id : ids) {
            deletePurchaseOrderById(id);
        }
        return ids.length;
    }

    /**
     * 删除采购信息
     *
     * @param id 采购主键
     * @return 结果
     */
    @Override
    public int deletePurchaseOrderById(Long id) {
        PurchaseOrder purchaseOrder = purchaseOrderMapper.selectPurchaseOrderById(id);
        List<PurchaseOrderPart> purchaseOrderParts = purchaseOrderPartMapper.selectPurchaseOrderPartsByOrderId(purchaseOrder.getId());
        for (int i = 0; i < purchaseOrderParts.size(); i++) {
            PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
            purchaseOrderPartMapper.deletePurchaseOrderPartById(purchaseOrderPart.getId());
        }
        int i = purchaseOrderMapper.deletePurchaseOrderById(id);
        return i;
    }

    /**
     * 详情页：获取采购订单的组件
     *
     * @param orderId
     * @return
     */
    @Override
    public List<PurchaseOrderPart> GetOrderPartMsg(Long orderId) {
        purchaseOrderPartMapper.selectPurchaseOrderPartsByOrderId(orderId);
        return purchaseOrderPartMapper.selectPurchaseOrderPartsByOrderId(orderId);
    }

    /**
     * 根据采购订单编号 获取供应商详情信息
     *
     * @param orderId （purchaseOrderId） 采购订单编号
     * @return
     */
    @Override
    public List getOrderPartnerMsg(Long orderId) {
        //根据订单id  查询订单
        PurchaseOrder purchaseOrder = purchaseOrderMapper.selectPurchaseOrderById(orderId);
        //获取合作伙伴业务类
        ResPartnerServiceImpl bean = SpringUtils.getBean(ResPartnerServiceImpl.class);
        //查询供应商信息
        List partnerMessage = bean.selectPartnerMessageByPartnerId(purchaseOrder.getPartnerId());
        return partnerMessage;
    }

    /**
     * 用于给采购订单组件统一设置序列
     *
     * @return
     */
    @Override
    public String sequenceHandle() {
        List<PurchaseOrder> purchaseOrders = selectPurchaseOrderList(new PurchaseOrder());
        for (int i = 0; i < purchaseOrders.size(); i++) {
            PurchaseOrder purchaseOrder = purchaseOrders.get(i);
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrderPartMapper.selectPurchaseOrderPartsByOrderId(purchaseOrder.getId());
            for (Integer j = 0; j < purchaseOrderParts.size(); j++) {
                PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(j);
                purchaseOrderPart.setSequence(j.longValue());
                purchaseOrderPartMapper.updatePurchaseOrderPart(purchaseOrderPart);
            }
        }
        return "OK";
    }


    /**
     * 根据订单序列（实际上是订单插入数据库前的唯一表示） 删除订单组件
     *
     * @param sequence
     * @return
     */
    @Override
    public PurchaseOrder purchaseOrderPartDelete(Long sequence, String isInsert) {
        if (isInsert.equals("t")) {
            PurchaseOrder purchaseOrder = INSERT_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
            for (PurchaseOrderPart purchaseOrderPart : purchaseOrderParts) {
                if (purchaseOrderPart.getSequence().equals(sequence)) {
                    purchaseOrderParts.remove(purchaseOrderPart);
                    break;
                }
            }
            purchaseOrder.setPurchaseOrderParts(purchaseOrderParts);
            PurchaseOrder purchaseOrderChecked = checkPurchaseOrderPrice(purchaseOrder);
            //TODO 这里别忘了核算总价之后再返回回去
            INSERT_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), purchaseOrderChecked);
            return purchaseOrderChecked;
        }
        if (isInsert.equals("f")) {
            PurchaseOrder purchaseOrderUpdate = UPDATE_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<PurchaseOrderPart> purchaseOrderUpdateParts = purchaseOrderUpdate.getPurchaseOrderParts();
            for (PurchaseOrderPart purchaseOrderPart : purchaseOrderUpdateParts) {
                if (purchaseOrderPart.getSequence().equals(sequence)) {
                    purchaseOrderUpdateParts.remove(purchaseOrderPart);
                    break;
                }
            }
            purchaseOrderUpdate.setPurchaseOrderParts(purchaseOrderUpdateParts);
            PurchaseOrder purchaseOrderChecked = checkPurchaseOrderPrice(purchaseOrderUpdate);
            //TODO 这里别忘了核算总价之后再返回回去
            UPDATE_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), purchaseOrderChecked);
            return purchaseOrderChecked;
        }
        return new PurchaseOrder();
    }

    /**
     * 组件编辑接口
     *
     * @param purchaseOrderPartNew
     * @return
     */
    @Override
    public PurchaseOrder purchaseOrderPartUpdate(PurchaseOrderPart purchaseOrderPartNew) {
        if (purchaseOrderPartNew.getOrderId() == null) {
            PurchaseOrder purchaseOrder = INSERT_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
            for (int i = 0; i < purchaseOrderParts.size(); i++) {
                PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
                if (purchaseOrderPart.getSequence().equals(purchaseOrderPartNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(purchaseOrderPartNew, purchaseOrderPart);
                }
            }
            purchaseOrder.setPurchaseOrderParts(purchaseOrderParts);
            PurchaseOrder purchaseOrderChecked = checkPurchaseOrderPrice(purchaseOrder);
            INSERT_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), purchaseOrderChecked);
            return purchaseOrderChecked;
        }

        if (purchaseOrderPartNew.getOrderId() != null) {
            PurchaseOrder purchaseOrder = UPDATE_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
            for (int i = 0; i < purchaseOrderParts.size(); i++) {
                PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
                if (purchaseOrderPart.getSequence().equals(purchaseOrderPartNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(purchaseOrderPartNew, purchaseOrderPart);
                }
            }
            purchaseOrder.setPurchaseOrderParts(purchaseOrderParts);
            PurchaseOrder purchaseOrderChecked = checkPurchaseOrderPrice(purchaseOrder);
            UPDATE_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), purchaseOrderChecked);
            return purchaseOrderChecked;
        }
        return new PurchaseOrder();
    }

    /**
     * @param stockId 库存产品id
     * @return 该方法用于采购订单的订单行获取
     * 返回内容包含一个带有部分产品信息的订单行对象，给前端渲染后，等待用户添加
     */
    @Override
    public PurchaseOrderPart getPurchaseOrderPartFromStockProductTemplateId(Long stockId) {
        /**
         * 与销售订单逻辑略有不同，因为供应商确定后，采购的内容必须是该供应商旗下的
         */
        //根据库存产品模板id 查询库存模板产品
        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockId);
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        //构造全新的订单组件
        PurchaseOrderPart purchaseOrderPart = new PurchaseOrderPart();
        purchaseOrderPart.setName(stockProductTemplate.getName());
        purchaseOrderPart.setProductQty(1L);
        purchaseOrderPart.setDatePlanned(new Date());
        purchaseOrderPart.setProductUom(stockProductTemplate.getUomId());
        purchaseOrderPart.setPriceUnit(stockProductTemplate.getListPrice());
        purchaseOrderPart.setProductId(stockProductEntity.getId());
        purchaseOrderPart.setPriceSubtotal(purchaseOrderPart.getPriceUnit().multiply(BigDecimal.valueOf(purchaseOrderPart.getProductQty())));
        purchaseOrderPart.setQtyReceived(0L);
        purchaseOrderPart.setWriteDate(new Date());
        purchaseOrderPart.setWriteUid(SecurityUtils.getUserId());
        purchaseOrderPart.setProductTemplateId(stockProductTemplate.getId());
        return purchaseOrderPart;
    }

    /**
     * 采购订单写入
     *
     * @param purchaseOrder
     * @return
     * @throws IllegalAccessException
     */
    @Override
    public PurchaseOrder writePurchaseOrder(PurchaseOrder purchaseOrder) throws IllegalAccessException {
        //传入订单没有id 则为新增
        if (purchaseOrder.getId() == null) {
            /**
             * 点击新增：传入内容为空，且map中的值为空，则为完全新增，返回空值提供给用户填写
             */
            System.out.println(purchaseOrder);
            if (com.ruoyi.common.utils.bean.BeanUtils.checkFieldAllNull(purchaseOrder) && !INSERT_PURCHASE_ORDERS_TEMP.containsKey(SecurityUtils.getUserId())) {
                INSERT_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), new PurchaseOrder());
                return new PurchaseOrder();
            }
            /**
             * 点击新增：传入内容为空，但map中的值不为空，则说明用户上一次中断了填写，返回上次中断前的内容提供给用户填写
             */
            if (com.ruoyi.common.utils.bean.BeanUtils.checkFieldAllNull(purchaseOrder) && INSERT_PURCHASE_ORDERS_TEMP.containsKey(SecurityUtils.getUserId())) {
                System.out.println("进入到这个方法了：");
                System.out.println(INSERT_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId()));
                return INSERT_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            }

            //说明是第一次插入订单行，设置订单组件的序列
            if (INSERT_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId()) == null) {
                //设置订单创建时间
                purchaseOrder.setDateOrder(new Date());
                purchaseOrder.setCreateDate(new Date());
            }
            System.out.println(purchaseOrder);
            PurchaseOrder purchaseOrderChecked = checkPurchaseOrderPrice(purchaseOrder);
            //TODO 在这里进行其他项目的更新
            INSERT_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), purchaseOrderChecked);
            return purchaseOrderChecked;
        }
        if (purchaseOrder.getId() != null) {
            //传入订单包含id 则为修改接口
            PurchaseOrder purchaseOrderUpdateChecked = checkPurchaseOrderPrice(purchaseOrder);
            UPDATE_PURCHASE_ORDERS_TEMP.put(SecurityUtils.getUserId(), purchaseOrderUpdateChecked);
            return purchaseOrderUpdateChecked;
        }
        return new PurchaseOrder();
    }

    /**
     * 根据用户选定的供应商，提供前端可选择的库存产品
     *
     * @return
     */
    @Override
    public List<StockProductTemplate> selectPurchaseOrderStockListAvailable(String isInsert,String name) {
        try {
            if (isInsert.equals("t")) {
                PurchaseOrder purchaseOrder = INSERT_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
                String partnerId = String.valueOf(purchaseOrder.getPartnerId());
                List<StockProductSupplierinfo> stockProductSupplierinfos = stockProductSupplierinfoMapper.selectStockProductSupplierinfoByPartnerId(partnerId);
                List<StockProductTemplate> stocks = new ArrayList();
                for (StockProductSupplierinfo stockProductSupplierinfo : stockProductSupplierinfos) {
                    StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductSupplierinfo.getProductTmplId());
                    if (name.equals("null")||name.equals("")){
                        stocks.add(stockProductTemplate);
                    }else if (!name.equals("null") & stockProductTemplate.getName().contains(name)){
                        stocks.add(stockProductTemplate);
                    }
                }
                return stocks;
            }
            if (isInsert.equals("f")) {
                PurchaseOrder purchaseOrder = UPDATE_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
                String partnerId = String.valueOf(purchaseOrder.getPartnerId());
                List<StockProductSupplierinfo> stockProductSupplierinfos = stockProductSupplierinfoMapper.selectStockProductSupplierinfoByPartnerId(partnerId);
                List<StockProductTemplate> stocks = new ArrayList();
                for (StockProductSupplierinfo stockProductSupplierinfo : stockProductSupplierinfos) {
                    StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockProductSupplierinfo.getProductTmplId());
                    if (name.equals("null")||name.equals("")){
                        stocks.add(stockProductTemplate);
                    }else if (!name.equals("null") & stockProductTemplate.getName().contains(name)){
                        stocks.add(stockProductTemplate);
                    }
                }
                return stocks;
            }
        } catch (NullPointerException e) {
            throw new RuntimeException("没有选定供应商");
        }
        return stockProductTemplateMapper.selectStockProductTemplateList(new StockProductTemplate());
    }


    /**
     * 采购订单处理类：序列与其他自动生成信息
     *
     * @param purchaseOrder
     * @return
     */
    public PurchaseOrder checkPurchaseOrderPrice(@NotNull PurchaseOrder purchaseOrder) {
        if (purchaseOrder.getId() == null) {
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
            BigDecimal price = new BigDecimal(0.00);
            if (purchaseOrderParts != null) {
                for (int i = 0; i < purchaseOrderParts.size(); i++) {
                    PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
                    purchaseOrderPart.setSequence(Long.valueOf(i) + 1);
                    purchaseOrderPart.setPriceSubtotal(purchaseOrderPart.getPriceUnit().multiply(BigDecimal.valueOf(purchaseOrderPart.getProductQty())));
                    price = price.add(purchaseOrderPart.getPriceSubtotal());
                }
                purchaseOrder.setAmountTotal(price);
            } else {
                purchaseOrder.setAmountTotal(price);
            }
            purchaseOrder.setAmountTotalDaxie(Convert.digitToChinese(purchaseOrder.getAmountTotal()));
            purchaseOrder.setWriteDate(new Date());
            purchaseOrder.setWriteUid(SecurityUtils.getUserId());
            return purchaseOrder;
        }
        if (purchaseOrder.getId() != null) {
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
            BigDecimal price = new BigDecimal(0.00);
            if (purchaseOrderParts != null) {
                for (int i = 0; i < purchaseOrderParts.size(); i++) {
                    PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
                    purchaseOrderPart.setSequence(Long.valueOf(i) + 1);
                    purchaseOrderPart.setPriceSubtotal(purchaseOrderPart.getPriceUnit().multiply(BigDecimal.valueOf(purchaseOrderPart.getProductQty())));
                    price = price.add(purchaseOrderPart.getPriceSubtotal());
                    purchaseOrderPart.setOrderId(purchaseOrder.getId());
                }
                purchaseOrder.setAmountTotal(price);
            } else {
                purchaseOrder.setAmountTotal(price);
            }
            purchaseOrder.setAmountTotalDaxie(Convert.digitToChinese(purchaseOrder.getAmountTotal()));
            purchaseOrder.setWriteDate(new Date());
            purchaseOrder.setWriteUid(SecurityUtils.getUserId());
            return purchaseOrder;
        }
        return new PurchaseOrder();

    }


    @Override
    public String purchaseOrderPartUpdateAbandon() {
        INSERT_PURCHASE_ORDERS_TEMP.remove(SecurityUtils.getUserId());
        return "新增已放弃";
    }

    @Override
    public PurchaseOrderPart purchaseOrderPartInfo(Long sequenceId, String isInsert) {
        if (isInsert.equals("t")) {
            /**
             * 当处于更新页面的时候：
             */
            PurchaseOrder purchaseOrder = INSERT_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
            for (PurchaseOrderPart purchaseOrderPart : purchaseOrderParts) {
                if (purchaseOrderPart.getSequence().equals(sequenceId)) {
                    return purchaseOrderPart;
                }
            }
        } else if (isInsert.equals("f")) {
            /**
             * 当处于更新页面的时候：
             */
            PurchaseOrder purchaseOrder = UPDATE_PURCHASE_ORDERS_TEMP.get(SecurityUtils.getUserId());
            List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
            for (PurchaseOrderPart purchaseOrderPart : purchaseOrderParts) {
                if (purchaseOrderPart.getSequence().equals(sequenceId)) {
                    return purchaseOrderPart;
                }
            }
        }
        return new PurchaseOrderPart();
    }

    /**
     * 采购询价单 转化为 采购单
     *
     * @param id
     */
    @Override
    public void draftPurchaseOrderConfirmToPurchaseOrder(Long id) {
        PurchaseOrder purchaseOrder = selectPurchaseOrderById(id);
        if (purchaseOrder.getState().equals("cancel") || purchaseOrder.getState().equals("purchase")) {
            throw new RuntimeException("订单状态处于已取消或已经是采购订单");
        }
        purchaseOrder.setState("purchase");
        purchaseOrder.setWriteDate(new Date());
        purchaseOrderMapper.updatePurchaseOrder(purchaseOrder);
        //TODO:生成收货管理（WH/IN）
        StockPickingServiceImpl bean = SpringUtils.getBean(StockPickingServiceImpl.class);
        bean.stockPickingGenerateBySPurchaseOrder(purchaseOrder);
    }

    /**
     * 报价单取消订单
     *
     * @param id
     */
    @Override
    public void purchaseOrderCancel(Long id) {
        PurchaseOrder purchaseOrder = purchaseOrderMapper.selectPurchaseOrderById(id);
        if (purchaseOrder.getState().equals("cancel") || purchaseOrder.getState().equals("purchase")) {
            throw new RuntimeException("订单状态处于已取消或已经是销售订单");
        }
        purchaseOrder.setState("cancel");
        purchaseOrderMapper.updatePurchaseOrder(purchaseOrder);
    }


    @Override
    public void purchaseOrderTranslation(PurchaseOrder purchaseOrder) {
        SysUser sysUserWrite = sysUserMapper.selectUserById(purchaseOrder.getWriteUid());
        SysUser sysUserCreate = sysUserMapper.selectUserById(purchaseOrder.getCreateUid());
        purchaseOrder.setCreateUserName(sysUserCreate.getNickName());
        purchaseOrder.setWriteUserName(sysUserWrite.getNickName());
        if (purchaseOrder.getInvoiceStatus() != null) {
            if (purchaseOrder.getInvoiceStatus().equals("no")) {
                purchaseOrder.setInvoiceStatus("不需开票");
            } else if (purchaseOrder.getInvoiceStatus().equals("to invoice")) {
                purchaseOrder.setInvoiceStatus("待开票");
            } else if (purchaseOrder.getInvoiceStatus().equals("invoiced")) {
                purchaseOrder.setInvoiceStatus("已开票");
            }
        }
        purchaseOrder.setAmountTotalDaxie(Convert.digitToChinese(purchaseOrder.getAmountTotal()));
        purchaseOrder.setCurrencyName("￥");
        return;
    }

    @Override
    public PurchaseOrder selectPurchaseOrderByName(String purchaseOrderName) {
        PurchaseOrder purchaseOrder = purchaseOrderMapper.selectPurchaseOrderByName(purchaseOrderName);
        return selectPurchaseOrderById(purchaseOrder.getId());
    }

}
