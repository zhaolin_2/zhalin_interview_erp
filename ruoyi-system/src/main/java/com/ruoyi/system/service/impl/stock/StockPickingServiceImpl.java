package com.ruoyi.system.service.impl.stock;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.purchase.PurchaseOrder;
import com.ruoyi.system.domain.purchase.PurchaseOrderPart;
import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.domain.sale.SaleOrder;
import com.ruoyi.system.domain.sale.SaleOrderPart;
import com.ruoyi.system.domain.stock.*;
import com.ruoyi.system.domain.stock.vo.StockDeliveryOrReceiveVo;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.impl.purchase.PurchaseOrderServiceImpl;
import com.ruoyi.system.service.impl.sale.SaleOrderServiceImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.service.IStockPickingService;

import javax.swing.*;

/**
 * 库存拣货Service业务层处理
 *
 * @author Lin
 * @date 2023-05-14
 */
@Service
public class StockPickingServiceImpl implements IStockPickingService {
    @Autowired
    private StockPickingMapper stockPickingMapper;

    @Autowired
    private StockMoveMapper stockMoveMapper;

    @Autowired
    private StockQuantMapper stockQuantMapper;

    @Autowired
    private StockInventoryMapper stockInventoryMapper;

    @Autowired
    private StockInventoryLineMapper stockInventoryLineMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;

    @Autowired
    private StockProductTemplateMapper stockProductTemplateMapper;

    @Autowired
    private ResPartnerMapper resPartnerMapper;

    @Autowired
    private SaleOrderMapper saleOrderMapper;

    @Autowired
    private StockMoveLineMapper stockMoveLineMapper;

    @Autowired
    private SaleOrderPartMapper saleOrderPartMapper;

    @Autowired
    private PurchaseOrderPartMapper purchaseOrderPartMapper;

    @Autowired
    private PurchaseOrderMapper purchaseOrderMapper;


    private static Map<Long, StockPicking> INSERT_PICKING_TEMP = new HashMap<>();

    private static Map<Long, StockPicking> UPDATE_PICKING_TEMP = new HashMap<>();

    /**
     * 查询库存出入库管理
     *
     * @param id 库存拣货主键
     * @return 库存拣货
     */
    @Override
    public StockPicking selectStockPickingById(Long id) {
        StockPicking stockPicking = stockPickingMapper.selectStockPickingById(id);
        if (stockPicking.getPickingTypeId()==5L){
            SaleOrder saleOrder = saleOrderMapper.selectSaleOrderBySaleOrderName(stockPicking.getOrigin());
            stockPicking.setSaleOrderId(saleOrder.getId());
        }
        try {
            List<StockMove> stockMoves = stockMoveMapper.selectStockMoveByPickingId(stockPicking.getId());
            for (int i = 0; i < stockMoves.size(); i++) {
                StockMove stockMove =  stockMoves.get(i);
                stockMove.setProductTemplateId(stockProductEntityMapper.selectStockProductEntityById(stockMove.getProductId()).getProductTmplId());
            }
            stockPicking.setStockMoveList(stockMoves);
        } catch (NullPointerException e) {
            stockPicking.setStockMoveList(null);
        }
        UPDATE_PICKING_TEMP.put(SecurityUtils.getUserId(), stockPicking);
        return stockPicking;
    }

    /**
     * 查询库存出入库管理列表
     *
     * @param stockPicking 库存出入库
     * @return 库存拣货
     */
    @Override
    public List<StockPicking> selectStockPickingList(StockPicking stockPicking) {
        return stockPickingMapper.selectStockPickingList(stockPicking);
    }

    /**
     * 新增库存拣货
     *
     * @param stockPicking 库存拣货
     * @return 结果
     */
    @Override
    public int insertStockPicking(StockPicking stockPicking) {
        //插入记录同时改变库存
        stockPicking.setCreateDate(new Date());
        stockPicking.setPickingTypeId(16L);
        stockPicking.setPartnerName(resPartnerMapper.selectResPartnerById(stockPicking.getPartnerId()).getName());
        stockPickingMapper.insertStockPicking(stockPicking);
        List<StockMove> stockMoveList = stockPicking.getStockMoveList();
        for (StockMove stockMove : stockMoveList) {
            stockMove.setPickingId(stockPicking.getId());
            stockMoveMapper.insertStockMove(stockMove);
            //TODO 改变库存内容
        }
        INSERT_PICKING_TEMP.remove(SecurityUtils.getUserId());
        return stockPicking.getId().intValue();
    }

    /**
     * 修改库存拣货
     *
     * @param stockPicking 库存拣货
     * @return 结果
     */
    @Override
    public int updateStockPicking(StockPicking stockPicking) {
        List<StockMove> stockMovesOld = stockMoveMapper.selectStockMoveByPickingId(stockPicking.getId());
        for (int i = 0; i < stockMovesOld.size(); i++) {
            StockMove stockMove = stockMovesOld.get(i);
            stockMoveMapper.deleteStockMoveById(stockMove.getId());
        }
        stockPicking.setWriteDate(new Date());
        stockPicking.setWriteUid(SecurityUtils.getUserId());
        stockPickingMapper.updateStockPicking(stockPicking);
        List<StockMove> stockMoveList = stockPicking.getStockMoveList();
        int num = 0;
        for (int i = 0; i < stockMoveList.size(); i++) {
            StockMove stockMove = stockMoveList.get(i);
            stockMove.setPickingId(stockPicking.getId());
            num += stockMoveMapper.insertStockMove(stockMove);
        }
        UPDATE_PICKING_TEMP.remove(SecurityUtils.getUserId());
        return num;
    }

    /**
     * 批量删除库存拣货
     *
     * @param ids 需要删除的库存拣货主键
     * @return 结果
     */
    @Override
    public int deleteStockPickingByIds(Long[] ids) {
        for (Long id : ids) {
            stockPickingMapper.deleteStockPickingById(id);
        }
        return ids.length;
    }

    /**
     * 删除库存拣货信息
     *
     * @param id 库存拣货主键
     * @return 结果
     */
    @Override
    public int deleteStockPickingById(Long id) {
        List<StockMove> stockMoves = stockMoveMapper.selectStockMoveByPickingId(id);
        for (int i = 0; i < stockMoves.size(); i++) {
            StockMove stockMove = stockMoves.get(i);
            stockMoveMapper.deleteStockMoveById(stockMove.getId());
        }
        return stockPickingMapper.deleteStockPickingById(id);
    }

    /**
     * 待修改组件返回
     *
     * @param sequence
     * @param isInsert
     * @return
     */
    @Override
    public StockMove movePartInfo(Long sequence, String isInsert) {
        if (isInsert.equals("t")) {
            /**
             * 当处于更新页面的时候：
             */
            StockPicking stockPicking = INSERT_PICKING_TEMP.get(SecurityUtils.getUserId());
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (StockMove stockMove : stockMoveList) {
                if (stockMove.getSequence().equals(sequence)) {
                    return stockMove;
                }
            }
        } else if (isInsert.equals("f")) {
            /**
             * 当处于更新页面的时候：
             */
            StockPicking stockPicking = UPDATE_PICKING_TEMP.get(SecurityUtils.getUserId());
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (StockMove stockMove : stockMoveList) {
                if (stockMove.getSequence().equals(sequence)) {
                    return stockMove;
                }
            }
        }
        return new StockMove();
    }

    /**
     * 序列设置
     *
     * @param stockPicking
     * @return StockPicking
     */
    public StockPicking checkStockPicking(@NotNull StockPicking stockPicking) {
        if (stockPicking.getId() == null) {
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (int i = 0; i < stockMoveList.size(); i++) {
                StockMove stockMove = stockMoveList.get(i);
                stockMove.setSequence(Long.valueOf(i) + 1);
            }
            stockPicking.setStockMoveList(stockMoveList);
            return stockPicking;
        }

        if (stockPicking.getId() != null) {
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (int i = 0; i < stockMoveList.size(); i++) {
                StockMove stockMove = stockMoveList.get(i);
                stockMove.setSequence(Long.valueOf(i) + 1);
                stockMove.setPickingId(stockPicking.getId());
            }
            return stockPicking;
        }
        return new StockPicking();
    }


    @Override
    public List<StockMove> getMovePartsInfoByPickingId(Long pickingId) {
        return stockMoveMapper.selectStockMoveByPickingId(pickingId);
    }

    /**
     * 根据销售订单生成对应的picking发货管理记录（）
     *
     * @param saleOrder 发货管理的来源的销售订单
     */
    @Override
    public void stockPickingGenerateBySaleOrder(SaleOrder saleOrder) {
        //构造全新的出货管理：设置值，写入数据库
        StockPicking stockPicking = new StockPicking();
        stockPicking.setName(saleOrder.getName() + "发货管理");
        stockPicking.setOrigin(saleOrder.getName());
        stockPicking.setMoveType("out");
        stockPicking.setState("assigned");
        stockPicking.setScheduledDate(saleOrder.getValidityDate());
        stockPicking.setDate(new Date());
        stockPicking.setLocationId(15l);
        stockPicking.setPartnerId(saleOrder.getPartnerId().longValue());
        stockPicking.setPartnerName(resPartnerMapper.selectResPartnerById(saleOrder.getPartnerId().longValue()).getName());
        stockPicking.setPartnerId(saleOrder.getPartnerId().longValue());
        stockPicking.setCreateDate(new Date());
        stockPicking.setPickingTypeId(5l);
        stockPicking.setCreateUid(1l);
        stockPicking.setSaleId(saleOrder.getUserId().longValue());
        stockPickingMapper.insertStockPicking(stockPicking);
        //取出销售订单中的销售组件，并且转化为pick出货管理的子项（stockMove），即单项产品的发货记录
        List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
        for (int i = 0; i < saleOrderParts.size(); i++) {
            SaleOrderPart saleOrderPart = saleOrderParts.get(i);
            StockMove stockMove = new StockMove();
            stockMove.setName(saleOrderPart.getName());
            stockMove.setSequence(Long.valueOf(i) + 1);
            stockMove.setCreateDate(new Date());
            stockMove.setDate(new Date());
            stockMove.setDateExpected(saleOrder.getValidityDate());
            stockMove.setProductId(saleOrderPart.getProductId());
            stockMove.setOrderedQty(saleOrderPart.getProductUomQty());
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(saleOrderPart.getProductId());
            if (stockQuant != null) {
                stockMove.setProductQty(stockQuant.getQuantity());
            } else {
                stockMove.setProductQty(0l);
            }
            stockMove.setProductUom(2l);
            stockMove.setLocationId(15l);
            stockMove.setLocationDestId(8l);
            stockMove.setPickingId(stockPicking.getId());
            stockMove.setState("assigned");
            stockMove.setOrigin(saleOrder.getName());
            stockMove.setReference(stockPicking.getName());
            stockMove.setCreateDate(new Date());
            stockMove.setCreateUid(1l);
            stockMove.setIsDone("f");
            stockMove.setToRefund("f");
            stockMove.setSaleLineId(saleOrderPart.getId());
            stockMove.setActive("t");
            stockMoveMapper.insertStockMove(stockMove);
        }
    }

    /**
     * 根据采购订单生成出货管理（stock_pick）
     *
     * @param purchaseOrder
     */
    @Override
    public void stockPickingGenerateBySPurchaseOrder(PurchaseOrder purchaseOrder) {
        //构造全新的出货管理：设置值，写入数据库
        StockPicking stockPicking = new StockPicking();
        stockPicking.setName(purchaseOrder.getName() + "收货管理");
        stockPicking.setOrigin(purchaseOrder.getName());
        stockPicking.setMoveType("warehousing");
        stockPicking.setState("assigned");
        stockPicking.setScheduledDate(purchaseOrder.getDateOrder());
        stockPicking.setDate(new Date());
        stockPicking.setDateDone(null);
        stockPicking.setLocationId(15l);
        stockPicking.setPartnerId(purchaseOrder.getPartnerId().longValue());
        stockPicking.setPrinted("f");
        stockPicking.setIsLocked("f");
        stockPicking.setPickingTypeId(3l);
        stockPicking.setCreateDate(new Date());
        stockPicking.setCreateUid(1l);
        stockPicking.setWriteDate(new Date());
        stockPicking.setWriteUid(1l);
        stockPicking.setSaleId(purchaseOrder.getCreateUid());
        stockPickingMapper.insertStockPicking(stockPicking);
        //取出销售订单中的销售组件，并且转化为pick出货管理的子项（stockMove），即单项产品的发货记录
        List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();
        for (int i = 0; i < purchaseOrderParts.size(); i++) {
            PurchaseOrderPart purchaseOrderPart = purchaseOrderParts.get(i);
            StockMove stockMove = new StockMove();
            stockMove.setName(purchaseOrderPart.getName());
            stockMove.setSequence(Long.valueOf(i) + 1);
            stockMove.setCreateDate(new Date());
            stockMove.setDate(new Date());
            stockMove.setDateExpected(new Date());
            stockMove.setProductId(purchaseOrderPart.getProductId());
            stockMove.setOrderedQty(purchaseOrderPart.getProductQty());
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(purchaseOrderPart.getProductId());
            if (stockQuant==null){
                stockMove.setProductQty(0l);
            }else{
                stockMove.setProductQty(stockQuant.getQuantity());
            }
            stockMove.setProductUom(2l);
            stockMove.setLocationId(8l);
            stockMove.setLocationDestId(15l);
            stockMove.setPickingId(stockPicking.getId());
            stockMove.setState("assigned");
            stockMove.setOrigin(purchaseOrder.getName());
            stockMove.setReference(stockPicking.getName());
            stockMove.setCreateDate(new Date());
            stockMove.setCreateUid(1l);
            stockMove.setWriteDate(new Date());
            stockMove.setWriteUid(1l);
            stockMove.setIsDone("f");
            stockMove.setToRefund("f");
            stockMove.setSaleLineId(purchaseOrder.getId());
            stockMove.setActive("t");
            stockMoveMapper.insertStockMove(stockMove);
        }
    }

    /**
     * 发货 管理
     *
     * @param
     * @return
     */
    @Override
    public String stockDelivery(StockPicking stockPicking) {
        //拿到Picking之后 对库存进行整理 对销售订单的发货数量进行更新
        //查询订单  更新订单的交货数量
        //更新库存数量 新增一条发货记录
        SaleOrderServiceImpl bean = SpringUtils.getBean(SaleOrderServiceImpl.class);
        SaleOrder saleOrder = bean.selectSaleOrderBySaleOrderName(stockPicking.getOrigin());
        List<SaleOrderPart> saleOrderParts = saleOrder.getSaleOrderParts();
        List<StockMove> stockMoveList = stockPicking.getStockMoveList();
        //该订单是否全部发货完毕，处于完结状态
        boolean isDone = true;
        for (int i = 0; i < stockMoveList.size(); i++) {
            StockMove stockMove =  stockMoveList.get(i);
            //发货数量
            Long deliveryNum = stockMove.getNum();
            //扣减库存
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockMove.getProductId());
            stockQuant.setQuantity(stockQuant.getQuantity() - deliveryNum);
            stockQuant.setWriteUid(SecurityUtils.getUserId());
            stockQuant.setWriteDate(new Date());
            stockQuantMapper.updateStockQuant(stockQuant);
            for (SaleOrderPart saleOrderPart : saleOrderParts) {
                if (saleOrderPart.getProductId().equals(stockMove.getProductId())){
                    saleOrderPart.setQtyDelivered(stockMove.getNum());
                    if (stockMove.getNum()<saleOrderPart.getProductUomQty()){
                        isDone = false;
                    }
                    //将已发货的数量更新回数据库
                    saleOrderPartMapper.updateSaleOrderPart(saleOrderPart);
                }
            }
        }
        if (isDone){
            saleOrder.setState("done");
            saleOrder.setWriteUid(SecurityUtils.getUserId());
            saleOrder.setWriteDate(new Date());
            saleOrderMapper.updateSaleOrder(saleOrder);
        }
        stockMoveLineGenerator(stockPicking);
        System.out.println(stockPicking);
        return "OK";
    }

    /**
     * 收货 管理
     *
     * @param
     * @return
     */
    @Override
    public String stockReceive(StockPicking stockPicking) {
        //拿到Picking之后 对库存进行整理 对销售订单的发货数量进行更新
        //查询订单  更新订单的交货数量
        //更新库存数量 新增一条发货记录
        PurchaseOrderServiceImpl bean = SpringUtils.getBean(PurchaseOrderServiceImpl.class);
        PurchaseOrder purchaseOrder = bean.selectPurchaseOrderByName(stockPicking.getOrigin());
        List<PurchaseOrderPart> purchaseOrderParts = purchaseOrder.getPurchaseOrderParts();

        List<StockMove> stockMoveList = stockPicking.getStockMoveList();
        //该订单是否全部发货完毕，处于完结状态
        boolean isDone = true;
        for (int i = 0; i < stockMoveList.size(); i++) {
            StockMove stockMove =  stockMoveList.get(i);
            //发货数量
            Long receiveNum = stockMove.getNum();
            //扣减库存
            StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockMove.getProductId());
            stockQuant.setQuantity(stockQuant.getQuantity() + receiveNum);
            stockQuant.setWriteUid(SecurityUtils.getUserId());
            stockQuant.setWriteDate(new Date());
            stockQuantMapper.updateStockQuant(stockQuant);
            for (PurchaseOrderPart purchaseOrderPart : purchaseOrderParts) {
                if (purchaseOrderPart.getProductId().equals(stockMove.getProductId())){
                    purchaseOrderPart.setQtyReceived(stockMove.getNum());
                    if (stockMove.getNum()<purchaseOrderPart.getProductQty()){
                        isDone = false;
                    }
                    //将已发货的数量更新回数据库
                    purchaseOrderPartMapper.updatePurchaseOrderPart(purchaseOrderPart);
                }
            }
        }
        if (isDone){
            purchaseOrder.setState("done");
            purchaseOrder.setWriteUid(SecurityUtils.getUserId());
            purchaseOrder.setWriteDate(new Date());
            purchaseOrderMapper.updatePurchaseOrder(purchaseOrder);
        }
        stockMoveLineGenerator(stockPicking);
        return "OK";
    }


    /**
     * 出入库 操作编辑
     *
     * @param stockPicking
     * @return
     */
    @Override
    public StockPicking writePicking(StockPicking stockPicking) throws IllegalAccessException {
        //传入订单没有id 则为新增
        if (stockPicking.getId() == null) {
            /**
             * 点击新增：传入内容为空，且map中的值为空，则为完全新增，返回空值提供给用户填写
             */
            if (com.ruoyi.common.utils.bean.BeanUtils.checkFieldAllNull(stockPicking) && !INSERT_PICKING_TEMP.containsKey(SecurityUtils.getUserId())) {
                return new StockPicking();
            }
            /**
             * 点击新增：传入内容为空，但map中的值不为空，则说明用户上一次中断了填写，返回上次中断前的内容提供给用户填写
             */
            if (com.ruoyi.common.utils.bean.BeanUtils.checkFieldAllNull(stockPicking) && INSERT_PICKING_TEMP.containsKey(SecurityUtils.getUserId())) {
                return INSERT_PICKING_TEMP.get(SecurityUtils.getUserId());
            }
            /**
             * 插入订单行确认按钮
             *传入数据不为空（），则说明用户是通过页面内提交订单组件按钮请求接口，保存用户传入的数据，并且返回给前端 提供用户编写
             * 第一次插入 map内什么都没有 直接写入处理后写入mao
             * 新增则追加
             * 修改应该如何处理（需要注意修改不要影响到新增）：
             * 现在map里是最新的 传回去的也是最新的 点击修改订单行：
             * 请求修改接口：传入序列 我将该序列对应的订单组件回传
             * 用户编辑
             * 点击这个write接口：
             * 前端
             */
            //说明是第一次插入订单行，设置订单组件的序列
            if (INSERT_PICKING_TEMP.get(SecurityUtils.getUserId()) == null) {
                //设置订单创建时间
                stockPicking.setState("assigned");
                stockPicking.setPickingTypeId(16L);
                stockPicking.setScheduledDate(new Date());
                stockPicking.setDate(new Date());
                stockPicking.setCreateUid(SecurityUtils.getUserId());
                stockPicking.setCreateDate(new Date());
                stockPicking.setWriteUid(SecurityUtils.getUserId());
                stockPicking.setWriteDate(new Date());
            }
            StockPicking stockPickingChecked = checkStockPicking(stockPicking);
            INSERT_PICKING_TEMP.put(SecurityUtils.getUserId(), stockPickingChecked);
            return stockPickingChecked;
        }
        if (stockPicking.getId() != null) {
            StockPicking stockPickingChecked = checkStockPicking(stockPicking);
            UPDATE_PICKING_TEMP.put(SecurityUtils.getUserId(), stockPickingChecked);
            return stockPickingChecked;
        }
        return new StockPicking();
    }

    @Override
    public StockMove getMovePartFromStockProductTemplateId(Long stockId) {
        //根据库存产品模板id 查询库存模板产品
        StockProductTemplate stockProductTemplate = stockProductTemplateMapper.selectStockProductTemplateById(stockId);
        StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockId);
        //构造全新的订单组件
        StockMove stockMove = new StockMove();
        stockMove.setName(stockProductTemplate.getName());
        stockMove.setDate(new Date());
        stockMove.setDateExpected(new Date());
        stockMove.setProductId(stockProductEntity.getId());
        stockMove.setOrderedQty(0L);
        StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockProductEntity.getId());
        if (stockQuant != null){
            stockMove.setProductQty(stockQuant.getQuantity());
        }else {
            stockMove.setProductQty(0L);
        }
        stockMove.setProductUomQty(0L);
        stockMove.setState("assigned");
        stockMove.setPickingTypeId(3L);
        stockMove.setCreateUid(SecurityUtils.getUserId());
        stockMove.setWriteUid(SecurityUtils.getUserId());
        stockMove.setIsDone("f");
        return stockMove;
    }

    /**
     * 根据订单序列（实际上是订单插入数据库前的唯一表示） 删除订单组件
     *
     * @param sequence
     * @return
     */
    @Override
    public StockPicking stockMovePartDelete(Long sequence, String isInsert) {
        if (isInsert.equals("t")){
            StockPicking stockPicking = INSERT_PICKING_TEMP.get(SecurityUtils.getUserId());
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (StockMove stockMove : stockMoveList) {
                if (stockMove.getSequence().equals(sequence)) {
                    stockMoveList.remove(stockMove);
                    break;
                }
            }
            stockPicking.setStockMoveList(stockMoveList);
            StockPicking stockPickingChecked = checkStockPicking(stockPicking);
            INSERT_PICKING_TEMP.put(SecurityUtils.getUserId(), stockPickingChecked);
            return stockPickingChecked;
        }
        if (isInsert.equals("f")){
            StockPicking stockPicking = UPDATE_PICKING_TEMP.get(SecurityUtils.getUserId());
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (StockMove stockMove : stockMoveList) {
                if (stockMove.getSequence().equals(sequence)) {
                    stockMoveList.remove(stockMove);
                    break;
                }
            }
            stockPicking.setStockMoveList(stockMoveList);
            StockPicking stockPickingChecked = checkStockPicking(stockPicking);
            UPDATE_PICKING_TEMP.put(SecurityUtils.getUserId(), stockPickingChecked);
            return stockPickingChecked;
        }
        return new StockPicking();
    }


    /**
     * 出入库管理 出入库组件编辑
     *
     * @param stockMoveNew 修改的组件
     *                         查询序列一致的，将新的组件替换旧组件，并且核算其他内容，将最新的订单存入map 并且返回
     * @return 修改后的出入库管理
     */
    @Override
    public StockPicking stockMovePartUpdate(StockMove stockMoveNew) {
        //如果传入的订单组件没有orderId，说明改订单没有插入过数据库内，从INSERT_SALE_ORDERS_TEMP 中取值即可
        if (stockMoveNew.getPickingId() == null){
            StockPicking stockPicking = INSERT_PICKING_TEMP.get(SecurityUtils.getUserId());
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (int i = 0; i < stockMoveList.size(); i++) {
                StockMove stockMove = stockMoveList.get(i);
                if (stockMove.getSequence().equals(stockMoveNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(stockMoveNew, stockMove);
                }
            }
            stockPicking.setStockMoveList(stockMoveList);
            StockPicking stockPickingChecked = checkStockPicking(stockPicking);
            INSERT_PICKING_TEMP.put(SecurityUtils.getUserId(), stockPickingChecked);
            return stockPickingChecked;
        }
        if (stockMoveNew.getPickingId() !=null){
            StockPicking stockPicking = UPDATE_PICKING_TEMP.get(SecurityUtils.getUserId());
            List<StockMove> stockMoveList = stockPicking.getStockMoveList();
            for (int i = 0; i < stockMoveList.size(); i++) {
                StockMove stockMove = stockMoveList.get(i);
                if (stockMove.getSequence().equals(stockMoveNew.getSequence())) {
                    //判断序列相同：
                    BeanUtils.copyProperties(stockMoveNew, stockMove);
                }
            }
            stockPicking.setStockMoveList(stockMoveList);
            StockPicking stockPickingChecked = checkStockPicking(stockPicking);
            UPDATE_PICKING_TEMP.put(SecurityUtils.getUserId(), stockPickingChecked);
            return stockPickingChecked;
        }
        return new StockPicking();
    }

    /**
     * 放弃出入库管理的编辑
     * @return
     */
    @Override
    public String stockPickingUpdateAbandon() {
        INSERT_PICKING_TEMP.remove(SecurityUtils.getUserId());
        return "新增已放弃";
    }

    /**
     * 出库确认
     * @param id
     * @return
     */
    @Override
    public String stockPickingConfirm(Long id) {
        StockPickingServiceImpl bean = SpringUtils.getBean(StockPickingServiceImpl.class);
        StockPicking stockPicking = bean.selectStockPickingById(id);
        List<StockMove> stockMoveList = stockPicking.getStockMoveList();
        if (stockMoveList ==null || stockMoveList.size()==0){
            throw new RuntimeException("没有可操作的库存，请编辑后确认。");
        }
        //出库操作
        if (stockPicking.getMoveType().equals("outbound")){
            for (StockMove stockMove : stockMoveList) {
                StockQuant stockQuant = stockQuantMapper.selectStockPhysicalInventoryByProductId(stockMove.getProductId());
                    stockQuant.setQuantity(stockQuant.getQuantity()-stockMove.getOrderedQty());
                    stockQuant.setWriteDate(new Date());
                    stockQuant.setWriteUid(SecurityUtils.getUserId());
                    stockQuantMapper.insertStockQuant(stockQuant);
                }
        }
        stockPicking.setState("done");
        stockPicking.setWriteUid(SecurityUtils.getUserId());
        stockPicking.setWriteDate(new Date());
        stockPicking.setDateDone(new Date());
        stockPickingMapper.updateStockPicking(stockPicking);
        return "库存数量已更新";
    }

    @Override
    public void setPartnerNameByPid(){
        List<StockPicking> stockPickings = selectStockPickingList(new StockPicking());
        for (int i = 0; i < stockPickings.size(); i++) {
            StockPicking stockPicking =  stockPickings.get(i);
            if (stockPicking.getPartnerId()!=null) {
                ResPartner resPartner = resPartnerMapper.selectResPartnerById(stockPicking.getPartnerId());
                stockPicking.setPartnerName(resPartner.getName());
            }
            stockPickingMapper.updateStockPicking(stockPicking);
        }
    }


    /**
     * 出库或发货 记录
     * 传入picking对象，取出其组件，根据组件的出库或发货数量生成记录
     * @param stockPicking
     */
    public void stockMoveLineGenerator(StockPicking stockPicking){
        List<StockMove> stockMoveList = stockPicking.getStockMoveList();
        for (int i = 0; i < stockMoveList.size(); i++) {
            StockMove stockMove = stockMoveList.get(i);
            //取出stockMove后，针对每一个move生成对应的出库记录（moveLine，同时也是发货的记录）
            StockMoveLine stockMoveLine = new StockMoveLine();
            stockMoveLine.setPickingId(stockPicking.getId());
            stockMoveLine.setMoveId(stockMove.getId());
            stockMoveLine.setProductId(stockMove.getProductId());
            stockMoveLine.setProductUomId(2l);
            stockMoveLine.setQtyDone(stockMove.getNum());
            stockMoveLine.setDate(new Date());
            stockMoveLine.setState("done");
            stockMoveLine.setReference(stockPicking.getName());
            stockMoveLine.setCreateDate(new Date());
            stockMoveLine.setCreateUid(SecurityUtils.getUserId());
            stockMoveLineMapper.insertStockMoveLine(stockMoveLine);
        }
    }



}
