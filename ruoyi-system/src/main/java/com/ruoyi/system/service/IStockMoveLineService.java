package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.StockMoveLine;

/**
 * 库存移动记录Service接口
 * 
 * @author Lin
 * @date 2023-06-05
 */
public interface IStockMoveLineService 
{
    /**
     * 查询库存移动记录
     * 
     * @param id 库存移动记录主键
     * @return 库存移动记录
     */
    public StockMoveLine selectStockMoveLineById(Long id);

    /**
     * 查询库存移动记录列表
     * 
     * @param stockMoveLine 库存移动记录
     * @return 库存移动记录集合
     */
    public List<StockMoveLine> selectStockMoveLineList(StockMoveLine stockMoveLine);

    /**
     * 新增库存移动记录
     * 
     * @param stockMoveLine 库存移动记录
     * @return 结果
     */
    public int insertStockMoveLine(StockMoveLine stockMoveLine);

    /**
     * 修改库存移动记录
     * 
     * @param stockMoveLine 库存移动记录
     * @return 结果
     */
    public int updateStockMoveLine(StockMoveLine stockMoveLine);

    /**
     * 批量删除库存移动记录
     * 
     * @param ids 需要删除的库存移动记录主键集合
     * @return 结果
     */
    public int deleteStockMoveLineByIds(Long[] ids);

    /**
     * 删除库存移动记录信息
     * 
     * @param id 库存移动记录主键
     * @return 结果
     */
    public int deleteStockMoveLineById(Long id);
}
