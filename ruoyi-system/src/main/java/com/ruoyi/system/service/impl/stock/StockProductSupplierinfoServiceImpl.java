package com.ruoyi.system.service.impl.stock;

import java.util.List;

import com.ruoyi.system.domain.sale.ResPartner;
import com.ruoyi.system.domain.stock.StockProductEntity;
import com.ruoyi.system.mapper.ResPartnerMapper;
import com.ruoyi.system.mapper.StockProductEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StockProductSupplierinfoMapper;
import com.ruoyi.system.domain.stock.StockProductSupplierinfo;
import com.ruoyi.system.service.IStockProductSupplierinfoService;

/**
 * 库存供应商信息Service业务层处理
 * 
 * @author Lin
 * @date 2023-04-17
 */
@Service
public class StockProductSupplierinfoServiceImpl implements IStockProductSupplierinfoService 
{
    @Autowired
    private StockProductSupplierinfoMapper stockProductSupplierinfoMapper;

    @Autowired
    private ResPartnerMapper resPartnerMapper;

    @Autowired
    private StockProductEntityMapper stockProductEntityMapper;



    /**
     * 查询库存供应商信息
     * 
     * @param id 库存供应商信息主键
     * @return 库存供应商信息
     */
    @Override
    public StockProductSupplierinfo selectStockProductSupplierinfoById(Long id)
    {
        return stockProductSupplierinfoMapper.selectStockProductSupplierinfoById(id);
    }

    /**
     * 查询库存供应商信息列表
     * 
     * @param stockProductSupplierinfo 库存供应商信息
     * @return 库存供应商信息
     */
    @Override
    public List<StockProductSupplierinfo> selectStockProductSupplierinfoList(StockProductSupplierinfo stockProductSupplierinfo)
    {
        return stockProductSupplierinfoMapper.selectStockProductSupplierinfoList(stockProductSupplierinfo);
    }

    /**
     * 新增库存供应商信息
     * 
     * @param stockProductSupplierinfo 库存供应商信息
     * @return 结果
     */
    @Override
    public int insertStockProductSupplierinfo(StockProductSupplierinfo stockProductSupplierinfo)
    {
        return stockProductSupplierinfoMapper.insertStockProductSupplierinfo(stockProductSupplierinfo);
    }

    /**
     * 修改库存供应商信息
     * 
     * @param stockProductSupplierinfo 库存供应商信息
     * @return 结果
     */
    @Override
    public int updateStockProductSupplierinfo(StockProductSupplierinfo stockProductSupplierinfo)
    {
        return stockProductSupplierinfoMapper.updateStockProductSupplierinfo(stockProductSupplierinfo);
    }

    /**
     * 批量删除库存供应商信息
     * 
     * @param ids 需要删除的库存供应商信息主键
     * @return 结果
     */
    @Override
    public int deleteStockProductSupplierinfoByIds(Long[] ids)
    {
        return stockProductSupplierinfoMapper.deleteStockProductSupplierinfoByIds(ids);
    }

    /**
     * 删除库存供应商信息信息
     * 
     * @param id 库存供应商信息主键
     * @return 结果
     */
    @Override
    public int deleteStockProductSupplierinfoById(Long id)
    {
        return stockProductSupplierinfoMapper.deleteStockProductSupplierinfoById(id);
    }

    /**
     * 根据库存产品模板id 查询库存产品供应商信息
     * @param stockId 库存产品的id（stock_product_template 的 id）
     * @return
     */
    @Override
    public List<StockProductSupplierinfo> selectStockProductSupplierinfoByStockTmplId(Long stockId) {
        List<StockProductSupplierinfo> stockProductSupplierinfos = stockProductSupplierinfoMapper.selectStockProductSupplierinfoByStockTmplId(stockId);
        for (int i = 0; i < stockProductSupplierinfos.size(); i++) {
            StockProductSupplierinfo stockProductSupplierinfo =  stockProductSupplierinfos.get(i);
            ResPartner resPartner = resPartnerMapper.selectResPartnerById(stockProductSupplierinfo.getName());
            stockProductSupplierinfo.setDisplayName(resPartner.getName());
            stockProductSupplierinfo.setPartnerId(stockProductSupplierinfo.getName());
        }
        return stockProductSupplierinfos;
    }

    //TODO
    @Override
    public void setProductIdByTmplId(){
        List<StockProductSupplierinfo> stockProductSupplierinfos = stockProductSupplierinfoMapper.selectStockProductSupplierinfoList(new StockProductSupplierinfo());
        for (int i = 0; i < stockProductSupplierinfos.size(); i++) {
            StockProductSupplierinfo stockProductSupplierinfo =  stockProductSupplierinfos.get(i);
            StockProductEntity stockProductEntity = stockProductEntityMapper.selectStockProductEntityByTemplateId(stockProductSupplierinfo.getProductTmplId());
            stockProductSupplierinfo.setProductId(stockProductEntity.getId());
            stockProductSupplierinfoMapper.updateStockProductSupplierinfo(stockProductSupplierinfo);
        }
    }
}
