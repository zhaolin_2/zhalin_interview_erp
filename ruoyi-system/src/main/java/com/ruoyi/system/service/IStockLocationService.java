package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.stock.StockLocation;

/**
 * 库存位置Service接口
 * 
 * @author Lin
 * @date 2023-04-14
 */
public interface IStockLocationService 
{
    /**
     * 查询库存位置
     * 
     * @param id 库存位置主键
     * @return 库存位置
     */
    public StockLocation selectStockLocationById(Long id);

    /**
     * 查询库存位置列表
     * 
     * @param stockLocation 库存位置
     * @return 库存位置集合
     */
    public List<StockLocation> selectStockLocationList(StockLocation stockLocation);

    /**
     * 新增库存位置
     * 
     * @param stockLocation 库存位置
     * @return 结果
     */
    public int insertStockLocation(StockLocation stockLocation);

    /**
     * 修改库存位置
     * 
     * @param stockLocation 库存位置
     * @return 结果
     */
    public int updateStockLocation(StockLocation stockLocation);

    /**
     * 批量删除库存位置
     * 
     * @param ids 需要删除的库存位置主键集合
     * @return 结果
     */
    public int deleteStockLocationByIds(Long[] ids);

    /**
     * 删除库存位置信息
     * 
     * @param id 库存位置主键
     * @return 结果
     */
    public int deleteStockLocationById(Long id);
}
