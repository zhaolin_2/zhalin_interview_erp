package com.ruoyi.system.domain.stock;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 单位名称对象 product_uom
 *
 * @author Lin
 * @date 2023-06-09
 */
public class ProductUom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** Category */
    @Excel(name = "Category")
    private Long categoryId;

    /** Ratio */
    @Excel(name = "Ratio")
    private BigDecimal factor;

    /** Rounding Precision */
    @Excel(name = "Rounding Precision")
    private BigDecimal rounding;

    /** Active */
    @Excel(name = "Active")
    private String active;

    /** Type */
    @Excel(name = "Type")
    private String uomType;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }
    public void setFactor(BigDecimal factor)
    {
        this.factor = factor;
    }

    public BigDecimal getFactor()
    {
        return factor;
    }
    public void setRounding(BigDecimal rounding)
    {
        this.rounding = rounding;
    }

    public BigDecimal getRounding()
    {
        return rounding;
    }
    public void setActive(String active)
    {
        this.active = active;
    }

    public String getActive()
    {
        return active;
    }
    public void setUomType(String uomType)
    {
        this.uomType = uomType;
    }

    public String getUomType()
    {
        return uomType;
    }
    public void setCreateUid(Long createUid)
    {
        this.createUid = createUid;
    }

    public Long getCreateUid()
    {
        return createUid;
    }
    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getCreateDate()
    {
        return createDate;
    }
    public void setWriteUid(Long writeUid)
    {
        this.writeUid = writeUid;
    }

    public Long getWriteUid()
    {
        return writeUid;
    }
    public void setWriteDate(Date writeDate)
    {
        this.writeDate = writeDate;
    }

    public Date getWriteDate()
    {
        return writeDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("categoryId", getCategoryId())
                .append("factor", getFactor())
                .append("rounding", getRounding())
                .append("active", getActive())
                .append("uomType", getUomType())
                .append("createUid", getCreateUid())
                .append("createDate", getCreateDate())
                .append("writeUid", getWriteUid())
                .append("writeDate", getWriteDate())
                .toString();
    }
}
