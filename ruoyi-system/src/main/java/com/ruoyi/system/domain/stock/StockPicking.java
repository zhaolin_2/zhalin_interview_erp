package com.ruoyi.system.domain.stock;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存拣货对象 stock_picking
 * 
 * @author Lin
 * @date 2023-05-14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockPicking extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** Reference */
    @Excel(name = "Reference")
    private String name;

    /** Source Document */
    @Excel(name = "Source Document")
    private String origin;

    /** Notes */
    @Excel(name = "Notes")
    private String note;

    /** Back Order of */
    @Excel(name = "Back Order of")
    private Long backorderId;

    /** Shipping Policy */
    @Excel(name = "Shipping Policy")
    private String moveType;

    /** Status */
    @Excel(name = "Status")
    private String state;

    /** Procurement Group */
    @Excel(name = "Procurement Group")
    private Long groupId;

    /** Priority */
    @Excel(name = "Priority")
    private String priority;

    /** Scheduled Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Scheduled Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date scheduledDate;

    /** Creation Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Creation Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** Date of Transfer */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Date of Transfer", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateDone;

    /** Source Location */
    @Excel(name = "Source Location")
    private Long locationId;

    /** Destination Location */
    @Excel(name = "Destination Location")
    private Long locationDestId;

    /** Operation Type */
    @Excel(name = "Operation Type")
    private Long pickingTypeId;

    /** Partner */
    @Excel(name = "Partner")
    private Long partnerId;

    /** Partner */
    private String partnerName;

    /** Company */
    @Excel(name = "Company")
    private Long companyId;

    /** Owner */
    @Excel(name = "Owner")
    private Long ownerId;

    /** Printed */
    @Excel(name = "Printed")
    private String printed;

    /** Is Locked */
    @Excel(name = "Is Locked")
    private String isLocked;

    /** Last Message Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Message Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date messageLastPost;

    /** Next Activity Deadline */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Next Activity Deadline", width = 30, dateFormat = "yyyy-MM-dd")
    private Date activityDateDeadline;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** Sales Order */
    @Excel(name = "Sales Order")
    private Long saleId;

    private Long saleOrderId;

    private Long purchaseOrderId;

    /**
     * picking 的子项
     */
    private List<StockMove> stockMoveList;

}
