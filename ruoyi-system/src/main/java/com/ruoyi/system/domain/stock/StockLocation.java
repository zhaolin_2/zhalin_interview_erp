package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存位置对象 stock_location
 * 
 * @author Lin
 * @date 2023-04-14
 */
public class StockLocation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** parent_left */
    @Excel(name = "parent_left")
    private Long parentLeft;

    /** parent_right */
    @Excel(name = "parent_right")
    private Long parentRight;

    /** 位置名称 */
    @Excel(name = "位置名称")
    private String name;

    /** 完整名称 */
    @Excel(name = "完整名称")
    private String completeName;

    /** Active */
    @Excel(name = "Active")
    private String active;

    /** Location Type */
    @Excel(name = "Location Type")
    private String usage;

    /** Parent Location */
    @Excel(name = "Parent Location")
    private Long locationId;

    /** Owner */
    @Excel(name = "Owner")
    private Long partnerId;

    /** Additional Information */
    @Excel(name = "Additional Information")
    private String comment;

    /** Corridor (X) */
    @Excel(name = "Corridor (X)")
    private Long posx;

    /** Shelves (Y) */
    @Excel(name = "Shelves (Y)")
    private Long posy;

    /** Height (Z) */
    @Excel(name = "Height (Z)")
    private Long posz;

    /** Company */
    @Excel(name = "Company")
    private Long companyId;

    /** Is a Scrap Location? */
    @Excel(name = "Is a Scrap Location?")
    private String scrapLocation;

    /** Is a Return Location? */
    @Excel(name = "Is a Return Location?")
    private String returnLocation;

    /** Removal Strategy */
    @Excel(name = "Removal Strategy")
    private Long removalStrategyId;

    /** Put Away Strategy */
    @Excel(name = "Put Away Strategy")
    private Long putawayStrategyId;

    /** Barcode */
    @Excel(name = "Barcode")
    private String barcode;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** Stock Valuation Account (Incoming) */
    @Excel(name = "Stock Valuation Account (Incoming)")
    private Long valuationInAccountId;

    /** Stock Valuation Account (Outgoing) */
    @Excel(name = "Stock Valuation Account (Outgoing)")
    private Long valuationOutAccountId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setParentLeft(Long parentLeft) 
    {
        this.parentLeft = parentLeft;
    }

    public Long getParentLeft() 
    {
        return parentLeft;
    }
    public void setParentRight(Long parentRight) 
    {
        this.parentRight = parentRight;
    }

    public Long getParentRight() 
    {
        return parentRight;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCompleteName(String completeName) 
    {
        this.completeName = completeName;
    }

    public String getCompleteName() 
    {
        return completeName;
    }
    public void setActive(String active) 
    {
        this.active = active;
    }

    public String getActive() 
    {
        return active;
    }
    public void setUsage(String usage) 
    {
        this.usage = usage;
    }

    public String getUsage() 
    {
        return usage;
    }
    public void setLocationId(Long locationId) 
    {
        this.locationId = locationId;
    }

    public Long getLocationId() 
    {
        return locationId;
    }
    public void setPartnerId(Long partnerId) 
    {
        this.partnerId = partnerId;
    }

    public Long getPartnerId() 
    {
        return partnerId;
    }
    public void setComment(String comment) 
    {
        this.comment = comment;
    }

    public String getComment() 
    {
        return comment;
    }
    public void setPosx(Long posx) 
    {
        this.posx = posx;
    }

    public Long getPosx() 
    {
        return posx;
    }
    public void setPosy(Long posy) 
    {
        this.posy = posy;
    }

    public Long getPosy() 
    {
        return posy;
    }
    public void setPosz(Long posz) 
    {
        this.posz = posz;
    }

    public Long getPosz() 
    {
        return posz;
    }
    public void setCompanyId(Long companyId) 
    {
        this.companyId = companyId;
    }

    public Long getCompanyId() 
    {
        return companyId;
    }
    public void setScrapLocation(String scrapLocation) 
    {
        this.scrapLocation = scrapLocation;
    }

    public String getScrapLocation() 
    {
        return scrapLocation;
    }
    public void setReturnLocation(String returnLocation) 
    {
        this.returnLocation = returnLocation;
    }

    public String getReturnLocation() 
    {
        return returnLocation;
    }
    public void setRemovalStrategyId(Long removalStrategyId) 
    {
        this.removalStrategyId = removalStrategyId;
    }

    public Long getRemovalStrategyId() 
    {
        return removalStrategyId;
    }
    public void setPutawayStrategyId(Long putawayStrategyId) 
    {
        this.putawayStrategyId = putawayStrategyId;
    }

    public Long getPutawayStrategyId() 
    {
        return putawayStrategyId;
    }
    public void setBarcode(String barcode) 
    {
        this.barcode = barcode;
    }

    public String getBarcode() 
    {
        return barcode;
    }
    public void setCreateUid(Long createUid) 
    {
        this.createUid = createUid;
    }

    public Long getCreateUid() 
    {
        return createUid;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setWriteUid(Long writeUid) 
    {
        this.writeUid = writeUid;
    }

    public Long getWriteUid() 
    {
        return writeUid;
    }
    public void setWriteDate(Date writeDate) 
    {
        this.writeDate = writeDate;
    }

    public Date getWriteDate() 
    {
        return writeDate;
    }
    public void setValuationInAccountId(Long valuationInAccountId) 
    {
        this.valuationInAccountId = valuationInAccountId;
    }

    public Long getValuationInAccountId() 
    {
        return valuationInAccountId;
    }
    public void setValuationOutAccountId(Long valuationOutAccountId) 
    {
        this.valuationOutAccountId = valuationOutAccountId;
    }

    public Long getValuationOutAccountId() 
    {
        return valuationOutAccountId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentLeft", getParentLeft())
            .append("parentRight", getParentRight())
            .append("name", getName())
            .append("completeName", getCompleteName())
            .append("active", getActive())
            .append("usage", getUsage())
            .append("locationId", getLocationId())
            .append("partnerId", getPartnerId())
            .append("comment", getComment())
            .append("posx", getPosx())
            .append("posy", getPosy())
            .append("posz", getPosz())
            .append("companyId", getCompanyId())
            .append("scrapLocation", getScrapLocation())
            .append("returnLocation", getReturnLocation())
            .append("removalStrategyId", getRemovalStrategyId())
            .append("putawayStrategyId", getPutawayStrategyId())
            .append("barcode", getBarcode())
            .append("createUid", getCreateUid())
            .append("createDate", getCreateDate())
            .append("writeUid", getWriteUid())
            .append("writeDate", getWriteDate())
            .append("valuationInAccountId", getValuationInAccountId())
            .append("valuationOutAccountId", getValuationOutAccountId())
            .toString();
    }
}
