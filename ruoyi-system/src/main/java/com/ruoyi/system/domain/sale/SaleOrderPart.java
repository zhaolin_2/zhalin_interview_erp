package com.ruoyi.system.domain.sale;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单组件对象 sale_order_part
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleOrderPart extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 订单名称 */
    private String orderDisplayName;

    /** 库存存的是备注内容 */
    @Excel(name = "名称")
    private String name;

    /** 产品名称 */
    private String productName;

    /** 优先级 */
    @Excel(name = "优先级")
    private Long sequence;

    /** 发票状态 */
    @Excel(name = "发票状态")
    private String invoiceStatus;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal priceUnit;

    /** 小计 */
    @Excel(name = "小计")
    private BigDecimal priceSubtotal;

    /** 税？ */
    @Excel(name = "税？")
    private Double priceTax;

    /** 总计 */
    @Excel(name = "总计")
    private BigDecimal priceTotal;

    /** Price Reduce */
    @Excel(name = "Price Reduce")
    private BigDecimal priceReduce;

    /** Price Reduce Tax inc */
    @Excel(name = "Price Reduce Tax inc")
    private BigDecimal priceReduceTaxinc;

    /** Price Reduce Tax excl */
    @Excel(name = "Price Reduce Tax excl")
    private BigDecimal priceReduceTaxexcl;

    /** 折扣 */
    @Excel(name = "折扣")
    private BigDecimal discount;

    /** 产品id */
    @Excel(name = "产品id")
    private Long productId;

    private Long productTemplateId;

    /** 数量 */
    @Excel(name = "数量")
    private Long productUomQty;

    /** 单位 */
    @Excel(name = "单位")
    private Long productUom;

    /** 已经交付数量 */
    @Excel(name = "Delivered")
    private Long qtyDelivered;

    /** To Invoice */
    @Excel(name = "To Invoice")
    private Long qtyToInvoice;

    /** Invoiced */
    @Excel(name = "Invoiced")
    private BigDecimal qtyInvoiced;

    //实际库存量
    private Long stockNum;

    /** 销售员id */
    @Excel(name = "销售员id")
    private Long salesmanId;

    /** Currency */
    @Excel(name = "Currency")
    private Long currencyId;

    /** Customer */
    @Excel(name = "Customer")
    private Long orderPartnerId;

    /** 供应商展示名称 */
    private String orderPartnerDisplayName;

    /** Is a down payment */
    @Excel(name = "Is a down payment")
    private String isDownpayment;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String state;

    /** 交付期限 */
    @Excel(name = "交付期限")
    private Long customerLead;

    /** Amount To Invoice */
    @Excel(name = "Amount To Invoice")
    private BigDecimal amtToInvoice;

    /** Amount Invoiced */
    @Excel(name = "Amount Invoiced")
    private BigDecimal amtInvoiced;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUid;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 最后更新者id */
    @Excel(name = "最后更新者id")
    private Long writeUid;

    /** 最后更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** 是否为服务 */
    @Excel(name = "是否为服务")
    private String isService;

    /** 表面处理 */
    @Excel(name = "表面处理")
    private String surfaceTreatment;

    /** 税 */
    @Excel(name = "税")
    private Long worldchampTaxId;

    /** 打印 */
    @Excel(name = "打印")
    private String isPrint;

    /** 单位 */
    @Excel(name = "单位")
    private String worldchampUom;

    /** 币种 */
    @Excel(name = "币种")
    private String worldchampCurrency;

    /**产品备注 */
    private String partMessage;

}
