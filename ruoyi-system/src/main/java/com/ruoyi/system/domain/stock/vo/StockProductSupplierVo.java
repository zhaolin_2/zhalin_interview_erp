package com.ruoyi.system.domain.stock.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存对象 stock_product
 * 
 * @author Lin
 * @date 2023-03-30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockProductSupplierVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 供应商id */
    private Integer partnerId;

    /** 最低采购数量*/
    private Integer minQty;

    /** 供应商报价 */
    private BigDecimal price;



}
