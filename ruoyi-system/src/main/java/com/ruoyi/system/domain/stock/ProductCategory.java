package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品内部类别对象 product_category
 * 
 * @author Lin
 * @date 2023-06-09
 */
public class ProductCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long parentLeft;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long parentRight;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 全程 */
    @Excel(name = "全程")
    private String completeName;

    /** Parent Category */
    @Excel(name = "Parent Category")
    private Long parentId;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** 所属公司 */
    @Excel(name = "所属公司")
    private Long companyId;

    /** Force Removal Strategy */
    @Excel(name = "Force Removal Strategy")
    private Long removalStrategyId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setParentLeft(Long parentLeft) 
    {
        this.parentLeft = parentLeft;
    }

    public Long getParentLeft() 
    {
        return parentLeft;
    }
    public void setParentRight(Long parentRight) 
    {
        this.parentRight = parentRight;
    }

    public Long getParentRight() 
    {
        return parentRight;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCompleteName(String completeName) 
    {
        this.completeName = completeName;
    }

    public String getCompleteName() 
    {
        return completeName;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setCreateUid(Long createUid) 
    {
        this.createUid = createUid;
    }

    public Long getCreateUid() 
    {
        return createUid;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setWriteUid(Long writeUid) 
    {
        this.writeUid = writeUid;
    }

    public Long getWriteUid() 
    {
        return writeUid;
    }
    public void setWriteDate(Date writeDate) 
    {
        this.writeDate = writeDate;
    }

    public Date getWriteDate() 
    {
        return writeDate;
    }
    public void setCompanyId(Long companyId) 
    {
        this.companyId = companyId;
    }

    public Long getCompanyId() 
    {
        return companyId;
    }
    public void setRemovalStrategyId(Long removalStrategyId) 
    {
        this.removalStrategyId = removalStrategyId;
    }

    public Long getRemovalStrategyId() 
    {
        return removalStrategyId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentLeft", getParentLeft())
            .append("parentRight", getParentRight())
            .append("name", getName())
            .append("completeName", getCompleteName())
            .append("parentId", getParentId())
            .append("createUid", getCreateUid())
            .append("createDate", getCreateDate())
            .append("writeUid", getWriteUid())
            .append("writeDate", getWriteDate())
            .append("companyId", getCompanyId())
            .append("removalStrategyId", getRemovalStrategyId())
            .toString();
    }
}
