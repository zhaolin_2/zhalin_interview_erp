package com.ruoyi.system.domain.purchase;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购订单组件对象 purchase_order_part
 * 
 * @author Lin
 * @date 2023-04-06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrderPart extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 序列 */
    @Excel(name = "序列")
    private Long sequence;

    /** 产品数量 */
    @Excel(name = "产品数量")
    private Long productQty;

    /** 计划日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date datePlanned;

    /** 产品单位 */
    @Excel(name = "产品单位")
    private Long productUom;

    /** 产品id */
    @Excel(name = "产品id")
    private Long productId;


    private Long productTemplateId;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal priceUnit;

    /** 部分总计 */
    @Excel(name = "部分总计")
    private BigDecimal priceSubtotal;

    /** 总计 */
    @Excel(name = "总计")
    private BigDecimal priceTotal;

    /** 税额 */
    @Excel(name = "税额")
    private Double priceTax;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 订单展示名称 */
    private String orderDisplayName;


    /** 状态 */
    @Excel(name = "状态")
    private String state;

    /** 开单数量 */
    @Excel(name = "开单数量")
    private Long qtyInvoiced;

    /** 已接收数量 */
    @Excel(name = "已接收数量")
    private Long qtyReceived;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long partnerId;

    /** 供应商展示名称 */
    private String partnerDisplayName;


    /** Currency */
    @Excel(name = "Currency")
    private Long currencyId;

    /** 订购点 */
    @Excel(name = "订购点")
    private Long orderpointId;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long createUid;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 最后更新者 */
    @Excel(name = "最后更新者")
    private Long writeUid;

    /** 最后更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

        /** 订单组件备注*/
    private String partMessage;



}
