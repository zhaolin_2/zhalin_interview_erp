package com.ruoyi.system.domain.sale;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 银行管理对象 res_bank
 * 
 * @author Lin
 * @date 2023-04-03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResBank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** Active */
    private String active;

    /** 银行识别代码 */
    @Excel(name = "银行识别代码")
    private String bic;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long writeUid;

    /** 最后更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

}
