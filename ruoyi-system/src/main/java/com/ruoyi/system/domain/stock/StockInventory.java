package com.ruoyi.system.domain.stock;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * Inventory对象 stock_inventory
 * 
 * @author Lin
 * @date 2023-05-31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockInventory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    /** 公司id */
    @Excel(name = "公司id")
    private Long companyId;

    /** 位置id */
    @Excel(name = "位置id")
    private Long locationId;

    /** productId */
    @Excel(name = "productId")
    private Long productId;

    /** 包装id */
    @Excel(name = "包装id")
    private Long packageId;

    /** 合作伙伴id */
    @Excel(name = "合作伙伴id")
    private Long partnerId;

    /** Inventoried Lot/Serial Number */
    @Excel(name = "Inventoried Lot/Serial Number")
    private Long lotId;

    /** Inventory of */
    @Excel(name = "Inventory of")
    private String filter;

    /** Inventoried Category */
    @Excel(name = "Inventoried Category")
    private Long categoryId;

    /** Include Exhausted Products */
    @Excel(name = "Include Exhausted Products")
    private String exhausted;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUid;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long writeUid;

    /** 更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** Force Accounting Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Force Accounting Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date accountingDate;

    /** Origin */
    @Excel(name = "Origin")
    private String origin;

    /** 到期日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date arrivalDate;

    /** Shipment Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Shipment Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date shipmentDate;

    /** Package Num */
    @Excel(name = "Package Num")
    private String packageNum;

    /** 销售订单 */
    @Excel(name = "销售订单")
    private Long saleOrderId;

    /**
     * stockInventoryLines 库存出入库的子项
     */
    private List<StockInventoryLine> stockInventoryLines;

}
