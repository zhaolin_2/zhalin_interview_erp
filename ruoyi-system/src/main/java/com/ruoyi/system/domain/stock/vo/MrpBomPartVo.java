package com.ruoyi.system.domain.stock.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 物料清单组件对象 mrp_bom_part
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MrpBomPartVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 库存产品的id */
    private Long stockId;

    /** 该库存产品的bom 组件的 stockId */
    private Long productTemplateId;

    /** Product Quantity */
    private Long productQty;

}
