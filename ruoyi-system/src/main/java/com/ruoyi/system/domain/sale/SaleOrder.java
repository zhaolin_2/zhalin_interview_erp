package com.ruoyi.system.domain.sale;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Anonymous;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 销售订单对象 sale_order
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单名称 */
    @Excel(name = "订单名称")
    private String name;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String state;

    /** 订单日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateOrder;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validityDate;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 确认日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "确认日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date confirmationDate;

    /** 销售员id */
    @Excel(name = "销售员id")
    private Integer userId;

    private String userName;
    private String partnerName;

    private String writeUserName;

    /** 客户id */
    @Excel(name = "客户id")
    private Integer partnerId;

    /** 发票地址 */
    @Excel(name = "发票地址")
    private Integer partnerInvoiceId;

    private String partnerInvoiceAddress;

    /** 送货地址 */
    @Excel(name = "送货地址")
    private Integer partnerShippingId;

    private String partnerShippingAddress;

    /** 价格表 */
    @Excel(name = "价格表")
    private Integer pricelistId;

    /** 发票状态 */
    @Excel(name = "发票状态")
    private String invoiceStatus;


    /** 条款和条件 */
    @Excel(name = "条款和条件")
    private String note;

    /** 不含税金额 */
    @Excel(name = "不含税金额")
    private Double amountUntaxed;

    /** 含税金额 */
    @Excel(name = "含税金额")
    private Double amountTax;

    /** 总计金额 */
    @Excel(name = "总计金额")
    private BigDecimal amountTotal;

    /** 付款条件 */
    @Excel(name = "付款条件")
    private Integer paymentTermId;


    /** 销售频道 */
    @Excel(name = "销售频道")
    private Integer teamId;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long createUid;

    /** 最后更新者 */
    @Excel(name = "最后更新者")
    private Long writeUid;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** 本地/海外 */
    @Excel(name = "本地/海外")
    private Long warehouseId;

    /** Procurement Group */
    @Excel(name = "采购组")
    private Long procurementGroupId;

    /** 交货条件 */
    @Excel(name = "交货条件")
    private String deliveryConditions;

    /** 指令单号 */
    @Excel(name = "指令单号")
    private String instructionOrder;

    /** 合同号 */
    @Excel(name = "合同号")
    private String contractNo;

    /** 运输方式 */
    @Excel(name = "运输方式")
    private String transportType;

    /** 提供增值发票 */
    @Excel(name = "提供增值发票")
    private String isProvideInvoice;

    /** 承担运费 */
    @Excel(name = "承担运费")
    private String isProvideFreight;

    /** 合同签订日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同签订日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date contractDate;

    /** 交货条件 */
    @Excel(name = "交货条件")
    private Long deliveryConditionsId;

    private String deliveryConditionsName;

    /** 含税 */
    @Excel(name = "含税")
    private String isTax;

    /** 银行 */
    @Excel(name = "银行")
    private Long worldchampBankId;

    /** 总金额大写 */
    @Excel(name = "总金额大写")
    private String amountTotalDaxie;

    /** 扣减 */
    @Excel(name = "扣减")
    private String isInventory;

    /** 订单行*/
    private List<SaleOrderPart> saleOrderParts;

    //搜索使用
    private String partName;




}
