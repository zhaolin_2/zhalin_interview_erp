package com.ruoyi.system.domain.stock;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物料清单对象 mrp_bom
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Data
public class MrpBom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** bom码 */
    @Excel(name = "bom码")
    private String code;

    /** 启用状态 */
    @Excel(name = "启用状态")
    private String active;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** Product */
    @Excel(name = "Product")
    private Long productTmplId;

    /** Product Variant */
    @Excel(name = "Product Variant")
    private Long productId;

    /** 库内产品数量 */
    @Excel(name = "库内产品数量")
    private Long productQty;

    /** Product Unit of Measure */
    @Excel(name = "Product Unit of Measure")
    private Long productUomId;

    /** Sequence */
    @Excel(name = "Sequence")
    private Long sequence;

    /** 路线 */
    @Excel(name = "路线")
    private Long routingId;

    /** Manufacturing Readiness */
    @Excel(name = "Manufacturing Readiness")
    private String readyToProduce;

    /** Operation Type */
    @Excel(name = "Operation Type")
    private Long pickingTypeId;

    /** Company */
    @Excel(name = "Company")
    private Long companyId;

    /** Last Message Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Message Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date messageLastPost;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /**
     * 订单组件数组
     */
    private List<MrpBomPart> mrpBomParts;


}
