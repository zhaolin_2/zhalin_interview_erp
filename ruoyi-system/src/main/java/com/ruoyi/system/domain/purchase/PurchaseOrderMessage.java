package com.ruoyi.system.domain.purchase;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购订单日志对象 purchase_order_message
 * 
 * @author Lin
 * @date 2023-04-06
 */
public class PurchaseOrderMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售订单id */
    @Excel(name = "销售订单id")
    private Long purchaseOrderId;

    /** 销售订单信息 */
    @Excel(name = "销售订单信息")
    private String purchaseOrderMsg;

    /** 处理类型 */
    @Excel(name = "处理类型")
    private String handleType;

    /** 操作人 */
    @Excel(name = "操作人")
    private String handleMan;

    /** 操作数量 */
    @Excel(name = "操作数量")
    private Long handleNum;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPurchaseOrderId(Long purchaseOrderId) 
    {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Long getPurchaseOrderId() 
    {
        return purchaseOrderId;
    }
    public void setPurchaseOrderMsg(String purchaseOrderMsg) 
    {
        this.purchaseOrderMsg = purchaseOrderMsg;
    }

    public String getPurchaseOrderMsg() 
    {
        return purchaseOrderMsg;
    }
    public void setHandleType(String handleType) 
    {
        this.handleType = handleType;
    }

    public String getHandleType() 
    {
        return handleType;
    }
    public void setHandleMan(String handleMan) 
    {
        this.handleMan = handleMan;
    }

    public String getHandleMan() 
    {
        return handleMan;
    }
    public void setHandleNum(Long handleNum) 
    {
        this.handleNum = handleNum;
    }

    public Long getHandleNum() 
    {
        return handleNum;
    }
    public void setRecordTime(Date recordTime) 
    {
        this.recordTime = recordTime;
    }

    public Date getRecordTime() 
    {
        return recordTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("purchaseOrderId", getPurchaseOrderId())
            .append("purchaseOrderMsg", getPurchaseOrderMsg())
            .append("handleType", getHandleType())
            .append("handleMan", getHandleMan())
            .append("handleNum", getHandleNum())
            .append("recordTime", getRecordTime())
            .toString();
    }
}
