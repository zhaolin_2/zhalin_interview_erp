package com.ruoyi.system.domain.purchase;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购对象 purchase_order
 * 
 * @author Lin
 * @date 2023-04-06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 来源 */
    @Excel(name = "来源")
    private String origin;

    /** 订单日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateOrder;

    /** 核准日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "核准日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateApprove;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long partnerId;

    private String partnerName;

    /** Currency */
    @Excel(name = "Currency")
    private Long currencyId;

    /** Currency */
    private String currencyName;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    /** 采购信息 */
    @Excel(name = "采购信息")
    private String notes;

    /** 发票数量 */
    @Excel(name = "发票数量")
    private Long invoiceCount;

    /** 发票状态 */
    @Excel(name = "发票状态")
    private String invoiceStatus;

    /** Receptions */
    @Excel(name = "Receptions")
    private Long pickingCount;

    /** 计划日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date datePlanned;

    /** 不含税总额 */
    @Excel(name = "不含税总额")
    private BigDecimal amountUntaxed;

    /** 税额 */
    @Excel(name = "税额")
    private BigDecimal amountTax;

    /** 订单总额 */
    @Excel(name = "订单总额")
    private BigDecimal amountTotal;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long createUid;

    /** 创建者 */
    private String createUserName;

    /** Deliver To */
    @Excel(name = "Deliver To")
    private Long pickingTypeId;

    /** 组id */
    @Excel(name = "组id")
    private Long groupId;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 最后更新人 */
    @Excel(name = "最后更新人")
    private Long writeUid;

    private String writeUserName;

    /** 最后更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** 采购单号 */
    @Excel(name = "采购单号")
    private String purchaseNum;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNum;

    /** 公司电话 */
    @Excel(name = "公司电话")
    private Long telId;

    /** 公司邮箱 */
    @Excel(name = "公司邮箱")
    private Long emailId;

    /** 编制 */
    @Excel(name = "编制")
    private String createMen;

    /**
     * 采购订单组件对象数组
     */
    List<PurchaseOrderPart> purchaseOrderParts;

    /** 金额大写 */
    private String amountTotalDaxie;

    //搜索使用
    private String partName;
    
}
