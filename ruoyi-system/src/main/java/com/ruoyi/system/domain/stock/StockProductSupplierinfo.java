package com.ruoyi.system.domain.stock;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存供应商信息对象 stock_product_supplierinfo
 * 
 * @author Lin
 * @date 2023-04-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockProductSupplierinfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 供应商id(partnerId) */
    @Excel(name = "供应商id(partnerId)")
    private Long name;

    private  Long partnerId;

    private String displayName;


    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** Vendor Product Code */
    @Excel(name = "Vendor Product Code")
    private String productCode;

    /** Sequence */
    @Excel(name = "Sequence")
    private Long sequence;

    /** 最低采购数量 */
    @Excel(name = "最低采购数量")
    private Long minQty;

    /** 供应商报价 */
    @Excel(name = "供应商报价")
    private BigDecimal price;

    /** Company */
    @Excel(name = "Company")
    private Long companyId;

    /** Currency */
    @Excel(name = "Currency")
    private Long currencyId;

    /** Start Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Start Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateStart;

    /** End Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "End Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateEnd;

    /** Product Variant */
    @Excel(name = "Product Variant")
    private Long productId;

    /** Product Template */
    @Excel(name = "Product Template")
    private Long productTmplId;

    /** Delivery Lead Time */
    @Excel(name = "Delivery Lead Time")
    private Long delay;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

}
