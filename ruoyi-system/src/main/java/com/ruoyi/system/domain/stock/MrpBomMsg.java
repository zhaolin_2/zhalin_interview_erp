package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * bom操作信息对象 mrp_bom_msg
 * 
 * @author Lin
 * @date 2023-04-18
 */
public class MrpBomMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** bom id */
    @Excel(name = "bom id")
    private Long bomId;

    /** bom操作信息 */
    @Excel(name = "bom操作信息")
    private String bomMsg;

    /** 处理类型 */
    @Excel(name = "处理类型")
    private String handleType;

    /** 操作人 */
    @Excel(name = "操作人")
    private String handleMan;

    /** 操作数量 */
    @Excel(name = "操作数量")
    private Long handleNum;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBomId(Long bomId) 
    {
        this.bomId = bomId;
    }

    public Long getBomId() 
    {
        return bomId;
    }
    public void setBomMsg(String bomMsg) 
    {
        this.bomMsg = bomMsg;
    }

    public String getBomMsg() 
    {
        return bomMsg;
    }
    public void setHandleType(String handleType) 
    {
        this.handleType = handleType;
    }

    public String getHandleType() 
    {
        return handleType;
    }
    public void setHandleMan(String handleMan) 
    {
        this.handleMan = handleMan;
    }

    public String getHandleMan() 
    {
        return handleMan;
    }
    public void setHandleNum(Long handleNum) 
    {
        this.handleNum = handleNum;
    }

    public Long getHandleNum() 
    {
        return handleNum;
    }
    public void setRecordTime(Date recordTime) 
    {
        this.recordTime = recordTime;
    }

    public Date getRecordTime() 
    {
        return recordTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("bomId", getBomId())
            .append("bomMsg", getBomMsg())
            .append("handleType", getHandleType())
            .append("handleMan", getHandleMan())
            .append("handleNum", getHandleNum())
            .append("recordTime", getRecordTime())
            .toString();
    }
}
