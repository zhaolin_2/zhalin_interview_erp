package com.ruoyi.system.domain.stock.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存对象 stock_product
 * 
 * @author Lin
 * @date 2023-03-30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockProductTemplateVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 序列 */
    @Excel(name = "序列")
    private Long sequence;

    /** 库存描述 */
    @Excel(name = "库存描述")
    private String description;

    /** 采购描述 */
    @Excel(name = "采购描述")
    private String descriptionPurchase;

    /** 销售描述 */
    @Excel(name = "销售描述")
    private String descriptionSale;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 内部类别 */
    @Excel(name = "内部类别")
    private Long categId;

    /** 售价 */
    @Excel(name = "售价")
    private Double listPrice;

    /** 体积 */
    @Excel(name = "体积")
    private Double volume;

    /** 重量 */
    @Excel(name = "重量")
    private Double weight;

    /** 可用于销售 */
    @Excel(name = "可用于销售")
    private String saleOk;

    /** 可用于购买 */
    @Excel(name = "可用于购买")
    private String purchaseOk;

    /** Unit of Measure */
    @Excel(name = "Unit of Measure")
    private Long uomId;

    /** Active */
    @Excel(name = "Active")
    private String active;

    /** 颜色 */
    @Excel(name = "颜色")
    private Long color;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUid;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 最后更新者id */
    @Excel(name = "最后更新者id")
    private Long writeUid;

    /** 最后更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** 规格 */
    @Excel(name = "规格")
    private String specification;

    /** 可用于费用 */
    @Excel(name = "可用于费用")
    private String canBeExpensed;

    /** 仓库(海内外) */
    @Excel(name = "仓库(海内外)")
    private String warehouse;

    /** 库存不足预警数量 */
    private Integer productMinQty;

    /** 补货数量推荐数量 */
    private Integer productMaxQty;

    /** 供应商id */
    private Integer partnerId;

    /** 最低采购数量*/
    private Integer purchaseMinQty;

    /** 供应商报价 */
    private BigDecimal price;

    /** 初始库存 */
    private Integer quantyWhenInsert;


}
