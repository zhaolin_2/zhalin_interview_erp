package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存移动记录对象 stock_move_line
 * 
 * @author Lin
 * @date 2023-06-05
 */
public class StockMoveLine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** Stock Picking */
    @Excel(name = "Stock Picking")
    private Long pickingId;

    /** Stock Move */
    @Excel(name = "Stock Move")
    private Long moveId;

    /** Product */
    @Excel(name = "Product")
    private Long productId;

    /** Unit of Measure */
    @Excel(name = "Unit of Measure")
    private Long productUomId;

    /** Real Reserved Quantity */
    @Excel(name = "Real Reserved Quantity")
    private Long productQty;

    /** Reserved */
    @Excel(name = "Reserved")
    private Long productUomQty;

    /** Ordered Quantity */
    @Excel(name = "Ordered Quantity")
    private Long orderedQty;

    /** Done */
    @Excel(name = "Done")
    private Long qtyDone;

    /** Source Package */
    @Excel(name = "Source Package")
    private Long packageId;

    /** Lot */
    @Excel(name = "Lot")
    private Long lotId;

    /** Lot/Serial Number */
    @Excel(name = "Lot/Serial Number")
    private String lotName;

    /** Destination Package */
    @Excel(name = "Destination Package")
    private Long resultPackageId;

    /** Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** Owner */
    @Excel(name = "Owner")
    private Long ownerId;

    /** From */
    @Excel(name = "From")
    private Long locationId;

    /** To */
    @Excel(name = "To")
    private Long locationDestId;

    /** Status */
    @Excel(name = "Status")
    private String state;

    /** Reference */
    @Excel(name = "Reference")
    private String reference;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** Work Order */
    @Excel(name = "Work Order")
    private Long workorderId;

    /** Production Order */
    @Excel(name = "Production Order")
    private Long productionId;

    /** Finished Lot */
    @Excel(name = "Finished Lot")
    private Long lotProducedId;

    /** Quantity Finished Product */
    @Excel(name = "Quantity Finished Product")
    private Long lotProducedQty;

    /** Done for Work Order */
    @Excel(name = "Done for Work Order")
    private String doneWo;

    /** Move Done */
    @Excel(name = "Move Done")
    private String doneMove;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPickingId(Long pickingId) 
    {
        this.pickingId = pickingId;
    }

    public Long getPickingId() 
    {
        return pickingId;
    }
    public void setMoveId(Long moveId) 
    {
        this.moveId = moveId;
    }

    public Long getMoveId() 
    {
        return moveId;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setProductUomId(Long productUomId) 
    {
        this.productUomId = productUomId;
    }

    public Long getProductUomId() 
    {
        return productUomId;
    }
    public void setProductQty(Long productQty) 
    {
        this.productQty = productQty;
    }

    public Long getProductQty() 
    {
        return productQty;
    }
    public void setProductUomQty(Long productUomQty) 
    {
        this.productUomQty = productUomQty;
    }

    public Long getProductUomQty() 
    {
        return productUomQty;
    }
    public void setOrderedQty(Long orderedQty) 
    {
        this.orderedQty = orderedQty;
    }

    public Long getOrderedQty() 
    {
        return orderedQty;
    }
    public void setQtyDone(Long qtyDone) 
    {
        this.qtyDone = qtyDone;
    }

    public Long getQtyDone() 
    {
        return qtyDone;
    }
    public void setPackageId(Long packageId) 
    {
        this.packageId = packageId;
    }

    public Long getPackageId() 
    {
        return packageId;
    }
    public void setLotId(Long lotId) 
    {
        this.lotId = lotId;
    }

    public Long getLotId() 
    {
        return lotId;
    }
    public void setLotName(String lotName) 
    {
        this.lotName = lotName;
    }

    public String getLotName() 
    {
        return lotName;
    }
    public void setResultPackageId(Long resultPackageId) 
    {
        this.resultPackageId = resultPackageId;
    }

    public Long getResultPackageId() 
    {
        return resultPackageId;
    }
    public void setDate(Date date) 
    {
        this.date = date;
    }

    public Date getDate() 
    {
        return date;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setLocationId(Long locationId) 
    {
        this.locationId = locationId;
    }

    public Long getLocationId() 
    {
        return locationId;
    }
    public void setLocationDestId(Long locationDestId) 
    {
        this.locationDestId = locationDestId;
    }

    public Long getLocationDestId() 
    {
        return locationDestId;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setReference(String reference) 
    {
        this.reference = reference;
    }

    public String getReference() 
    {
        return reference;
    }
    public void setCreateUid(Long createUid) 
    {
        this.createUid = createUid;
    }

    public Long getCreateUid() 
    {
        return createUid;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setWriteUid(Long writeUid) 
    {
        this.writeUid = writeUid;
    }

    public Long getWriteUid() 
    {
        return writeUid;
    }
    public void setWriteDate(Date writeDate) 
    {
        this.writeDate = writeDate;
    }

    public Date getWriteDate() 
    {
        return writeDate;
    }
    public void setWorkorderId(Long workorderId) 
    {
        this.workorderId = workorderId;
    }

    public Long getWorkorderId() 
    {
        return workorderId;
    }
    public void setProductionId(Long productionId) 
    {
        this.productionId = productionId;
    }

    public Long getProductionId() 
    {
        return productionId;
    }
    public void setLotProducedId(Long lotProducedId) 
    {
        this.lotProducedId = lotProducedId;
    }

    public Long getLotProducedId() 
    {
        return lotProducedId;
    }
    public void setLotProducedQty(Long lotProducedQty) 
    {
        this.lotProducedQty = lotProducedQty;
    }

    public Long getLotProducedQty() 
    {
        return lotProducedQty;
    }
    public void setDoneWo(String doneWo) 
    {
        this.doneWo = doneWo;
    }

    public String getDoneWo() 
    {
        return doneWo;
    }
    public void setDoneMove(String doneMove) 
    {
        this.doneMove = doneMove;
    }

    public String getDoneMove() 
    {
        return doneMove;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pickingId", getPickingId())
            .append("moveId", getMoveId())
            .append("productId", getProductId())
            .append("productUomId", getProductUomId())
            .append("productQty", getProductQty())
            .append("productUomQty", getProductUomQty())
            .append("orderedQty", getOrderedQty())
            .append("qtyDone", getQtyDone())
            .append("packageId", getPackageId())
            .append("lotId", getLotId())
            .append("lotName", getLotName())
            .append("resultPackageId", getResultPackageId())
            .append("date", getDate())
            .append("ownerId", getOwnerId())
            .append("locationId", getLocationId())
            .append("locationDestId", getLocationDestId())
            .append("state", getState())
            .append("reference", getReference())
            .append("createUid", getCreateUid())
            .append("createDate", getCreateDate())
            .append("writeUid", getWriteUid())
            .append("writeDate", getWriteDate())
            .append("workorderId", getWorkorderId())
            .append("productionId", getProductionId())
            .append("lotProducedId", getLotProducedId())
            .append("lotProducedQty", getLotProducedQty())
            .append("doneWo", getDoneWo())
            .append("doneMove", getDoneMove())
            .toString();
    }
}
