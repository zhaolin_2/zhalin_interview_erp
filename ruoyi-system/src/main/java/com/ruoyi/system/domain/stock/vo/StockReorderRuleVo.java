package com.ruoyi.system.domain.stock.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 重订货对象vo stock_reorder_rule
 * 用于接受前端传参使用，讲
 * @author Lin
 * @date 2023-04-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockReorderRuleVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 库存产品模板id */
    private Long stockId;

    /** Minimum Quantity */
    private Long productMinQty;

    /** Maximum Quantity */
    private Long productMaxQty;




}
