package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存数量对象 stock_quant
 * 
 * @author Lin
 * @date 2023-04-04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockQuant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** Product */
    @Excel(name = "Product")
    private Long productId;

    /** Location */
    @Excel(name = "Location")
    private Long locationId;

    /** Quantity */
    @Excel(name = "Quantity")
    private Long quantity;

    /** 预留数量 Quantity */
    @Excel(name = "Reserved Quantity")
    private Long reservedQuantity;


    /** 可操作数量 Quantity */
    private Long operationalQuantity;

    /** 预测数量 Quantity */
    private Long predictiveQuantity;

    /** Incoming Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Incoming Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inDate;



    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

}
