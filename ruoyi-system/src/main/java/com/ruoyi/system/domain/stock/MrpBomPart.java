package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物料清单组件对象 mrp_bom_part
 * 
 * @author Lin
 * @date 2023-04-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MrpBomPart extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** Product */
    @Excel(name = "Product")
    private Long productId;

    /** 库存产品名称 */
    private String displayName;

    /** Product Quantity */
    @Excel(name = "Product Quantity")
    private Long productQty;

    /** Product Unit of Measure */
    @Excel(name = "Product Unit of Measure")
    private Long productUomId;

    /** Product Unit of Measure */
    private String productUomName;

    /** 库内剩余存量 */
    private Long stockNum;

    /** Sequence */
    @Excel(name = "Sequence")
    private Long sequence;

    /** Routing */
    @Excel(name = "Routing")
    private Long routingId;

    /** Parent BoM */
    @Excel(name = "Parent BoM")
    private Long bomId;

    /** Consumed in Operation */
    @Excel(name = "Consumed in Operation")
    private Long operationId;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    private Long productTemplateId;
}
