package com.ruoyi.system.domain.stock.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Author Lin
 * @Date 2023 04 20 14
 * 该实体类用于库存产品 中 像用户展示该产品用于何种bom
 * 除bom包含的信息外，该类还包含上述产品在该bom中的使用数量以及库存数量
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MrpBomInStockVo {
    /** id */
    private Long id;

    /** Product */
    private Long productTmplId;

    /** 该bom 对应的库存产品模板名称(通过productTmplId查询得到)*/
    private String productTmplName;

    /** 库内产品数量 */
    private Long productQty;

    /** 单位 */
    private Long productUomId;

    private String productUomName;

    /**用户选择的组件用于其bom的数量 通俗地说就是用户查询的这个组件在它所在的bom里的构成数量是多少 */
    private Long bomPartNumInBom;




}
