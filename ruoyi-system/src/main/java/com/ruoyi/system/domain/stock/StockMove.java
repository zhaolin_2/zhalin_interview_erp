package com.ruoyi.system.domain.stock;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存收发对象 stock_move
 *
 * @author Lin
 * @date 2023-05-14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockMove extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    private Long productTemplateId;

    /**
     * Description
     */
    @Excel(name = "Description")
    private String name;

    /**
     * Sequence
     */
    @Excel(name = "Sequence")
    private Long sequence;

    /**
     * Priority
     */
    @Excel(name = "Priority")
    private String priority;

    /**
     * Creation Date
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Creation Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /**
     * Date
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /**
     * Company
     */
    @Excel(name = "Company")
    private Long companyId;

    /**
     * Expected Date
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Expected Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateExpected;

    /**
     * Product
     */
    @Excel(name = "Product")
    private Long productId;

    /**
     * 订单内数量
     */
    @Excel(name = "Ordered Quantity")
    private Long orderedQty;

    /**
     * 实际库存
     */
    @Excel(name = "Real Quantity")
    private Long productQty;

    /**
     * Initial Demand
     */
    @Excel(name = "Initial Demand")
    private Long productUomQty;

    /**
     * Unit of Measure
     */
    @Excel(name = "Unit of Measure")
    private Long productUom;

    /**
     * Preferred Packaging
     */
    @Excel(name = "Preferred Packaging")
    private Long productPackaging;

    /**
     * Source Location
     */
    @Excel(name = "Source Location")
    private Long locationId;

    /**
     * Destination Location
     */
    @Excel(name = "Destination Location")
    private Long locationDestId;

    /**
     * Destination Address
     */
    @Excel(name = "Destination Address ")
    private Long partnerId;

    /**
     * Transfer Reference
     */
    @Excel(name = "Transfer Reference")
    private Long pickingId;

    /**
     * 备注
     */
    @Excel(name = "Notes")
    private String note;

    /**
     * Status
     */
    @Excel(name = "Status")
    private String state;

    /**
     * Unit Price
     */
    @Excel(name = "Unit Price")
    private BigDecimal priceUnit;

    /**
     * Source Document
     */
    @Excel(name = "Source Document")
    private String origin;

    /**
     * Supply Method
     */
    @Excel(name = "Supply Method")
    private String procureMethod;

    /**
     * Scrapped
     */
    @Excel(name = "Scrapped")
    private String scrapped;

    /**
     * Procurement Group
     */
    @Excel(name = "Procurement Group")
    private Long groupId;

    /**
     * Procurement Rule
     */
    @Excel(name = "Procurement Rule")
    private Long ruleId;

    /**
     * Push Rule
     */
    @Excel(name = "Push Rule")
    private Long pushRuleId;

    /**
     * Propagate cancel and split
     */
    @Excel(name = "Propagate cancel and split")
    private String propagate;

    /**
     * Operation Type
     */
    @Excel(name = "Operation Type")
    private Long pickingTypeId;

    /**
     * Inventory
     */
    @Excel(name = "Inventory")
    private Long inventoryId;

    /**
     * Origin return move
     */
    @Excel(name = "Origin return move")
    private Long originReturnedMoveId;

    /**
     * Owner
     */
    @Excel(name = "Owner ")
    private Long restrictPartnerId;

    /**
     * Warehouse
     */
    @Excel(name = "Warehouse")
    private Long warehouseId;

    /**
     * Whether the move was added after the picking's confirmation
     */
    @Excel(name = "Whether the move was added after the picking's confirmation")
    private String additional;

    /**
     * Reference
     */
    @Excel(name = "Reference")
    private String reference;

    /**
     * Created by
     */
    @Excel(name = "Created by")
    private Long createUid;

    /**
     * Last Updated by
     */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /**
     * Last Updated on
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /**
     * Created Production Order
     */
    @Excel(name = "Created Production Order")
    private Long createdProductionId;

    /**
     * Production Order for finished products
     */
    @Excel(name = "Production Order for finished products")
    private Long productionId;

    /**
     * Production Order for raw materials
     */
    @Excel(name = "Production Order for raw materials")
    private Long rawMaterialProductionId;

    /**
     * Disassembly Order
     */
    @Excel(name = "Disassembly Order")
    private Long unbuildId;

    /**
     * Consumed Disassembly Order
     */
    @Excel(name = "Consumed Disassembly Order")
    private Long consumeUnbuildId;

    /**
     * Operation To Consume
     */
    @Excel(name = "Operation To Consume")
    private Long operationId;

    /**
     * Work Order To Consume
     */
    @Excel(name = "Work Order To Consume")
    private Long workorderId;

    /**
     * BoM Line
     */
    @Excel(name = "BoM Line")
    private Long bomLineId;

    /**
     * Unit Factor
     */
    @Excel(name = "Unit Factor")
    private Double unitFactor;

    /**
     * Done
     */
    @Excel(name = "Done")
    private String isDone;

    /**
     * To Refund (update SO/PO)
     */
    @Excel(name = "To Refund (update SO/PO)")
    private String toRefund;

    /**
     * Value
     */
    @Excel(name = "Value")
    private Long value;

    /**
     * Remaining Qty
     */
    @Excel(name = "Remaining Qty")
    private Long remainingQty;

    /**
     * Remaining Value
     */
    @Excel(name = "Remaining Value")
    private Long remainingValue;

    /**
     * Purchase Order Line
     */
    @Excel(name = "Purchase Order Line")
    private Long purchaseLineId;

    /**
     * Created Purchase Order Line
     */
    @Excel(name = "Created Purchase Order Line")
    private Long createdPurchaseLineId;

    /**
     * Repair
     */
    @Excel(name = "Repair")
    private Long repairId;

    /**
     * Sale Line
     */
    @Excel(name = "Sale Line")
    private Long saleLineId;

    /**
     * 有效
     */
    @Excel(name = "有效")
    private String active;

    /**
     * Shipping Method
     */
    @Excel(name = "Shipping Method")
    private String shippingMethod;

    /**
     * Type
     */
    @Excel(name = "Type")
    private String overseasType;

    //用于接收数量的收发货数量的参数
    private Long num;
}
