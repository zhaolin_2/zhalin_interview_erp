package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品实体对象 stock_product_entity
 * 
 * @author Lin
 * @date 2023-04-04
 */
public class StockProductEntity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** Internal Reference */
    @Excel(name = "Internal Reference")
    private String defaultCode;

    /** Active */
    @Excel(name = "Active")
    private String active;

    /** Product Template */
    @Excel(name = "Product Template")
    private Long productTmplId;

    /** Barcode */
    @Excel(name = "Barcode")
    private String barcode;

    /** Volume */
    @Excel(name = "Volume")
    private Double volume;

    /** Weight */
    @Excel(name = "Weight")
    private Double weight;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String customerProductNum;

    /** 国内/海外 */
    @Excel(name = "国内/海外")
    private String warehouse;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDefaultCode(String defaultCode) 
    {
        this.defaultCode = defaultCode;
    }

    public String getDefaultCode() 
    {
        return defaultCode;
    }
    public void setActive(String active) 
    {
        this.active = active;
    }

    public String getActive() 
    {
        return active;
    }
    public void setProductTmplId(Long productTmplId) 
    {
        this.productTmplId = productTmplId;
    }

    public Long getProductTmplId() 
    {
        return productTmplId;
    }
    public void setBarcode(String barcode) 
    {
        this.barcode = barcode;
    }

    public String getBarcode() 
    {
        return barcode;
    }
    public void setVolume(Double volume) 
    {
        this.volume = volume;
    }

    public Double getVolume() 
    {
        return volume;
    }
    public void setWeight(Double weight) 
    {
        this.weight = weight;
    }

    public Double getWeight() 
    {
        return weight;
    }
    public void setCreateUid(Long createUid) 
    {
        this.createUid = createUid;
    }

    public Long getCreateUid() 
    {
        return createUid;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setWriteUid(Long writeUid) 
    {
        this.writeUid = writeUid;
    }

    public Long getWriteUid() 
    {
        return writeUid;
    }
    public void setWriteDate(Date writeDate) 
    {
        this.writeDate = writeDate;
    }

    public Date getWriteDate() 
    {
        return writeDate;
    }
    public void setCustomerProductNum(String customerProductNum) 
    {
        this.customerProductNum = customerProductNum;
    }

    public String getCustomerProductNum() 
    {
        return customerProductNum;
    }
    public void setWarehouse(String warehouse) 
    {
        this.warehouse = warehouse;
    }

    public String getWarehouse() 
    {
        return warehouse;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("defaultCode", getDefaultCode())
            .append("active", getActive())
            .append("productTmplId", getProductTmplId())
            .append("barcode", getBarcode())
            .append("volume", getVolume())
            .append("weight", getWeight())
            .append("createUid", getCreateUid())
            .append("createDate", getCreateDate())
            .append("writeUid", getWriteUid())
            .append("writeDate", getWriteDate())
            .append("customerProductNum", getCustomerProductNum())
            .append("warehouse", getWarehouse())
            .toString();
    }

}
