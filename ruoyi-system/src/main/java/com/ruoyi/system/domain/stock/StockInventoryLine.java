package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * InventoryLine对象 stock_inventory_line
 * 
 * @author Lin
 * @date 2023-05-31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockInventoryLine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** InventoryId */
    @Excel(name = "InventoryId")
    private Long inventoryId;

    /** 合作伙伴id */
    @Excel(name = "合作伙伴id")
    private Long partnerId;

    /** 产品id */
    @Excel(name = "产品id")
    private Long productId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品编码 */
    @Excel(name = "产品编码")
    private String productCode;

    /** 产品单位 */
    @Excel(name = "产品单位")
    private Long productUomId;

    /** 产品数量 */
    @Excel(name = "产品数量")
    private Long productQty;

    /** 位置 */
    @Excel(name = "位置")
    private Long locationId;

    /** 位置名称 */
    @Excel(name = "位置名称")
    private String locationName;

    /** 包装id */
    @Excel(name = "包装id")
    private Long packageId;

    /** Lot/Serial Number */
    @Excel(name = "Lot/Serial Number")
    private Long prodLotId;

    /** Serial Number Name */
    @Excel(name = "Serial Number Name")
    private String prodlotName;

    /** 公司id */
    @Excel(name = "公司id")
    private Long companyId;

    /** 理论数量 */
    @Excel(name = "理论数量")
    private Long theoreticalQty;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long createUid;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long writeUid;

    /** 更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** Scheduled Receipt */
    @Excel(name = "Scheduled Receipt")
    private Long scheduledReceipt;

    /** 到达日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到达日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date arrivalDate;

    /** Shipment Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Shipment Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date shipmentDate;

    /** Package Num */
    @Excel(name = "Package Num")
    private String packageNum;

    /** Remind Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Remind Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date remindDate;

    /**
     * 调整数量
     */
    private Long handleNum;

    private Long productTemplateId;

}
