package com.ruoyi.system.domain.sale;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 合作伙伴对象 res_partner 和账户信息bank集合
 * 
 * @author Lin
 * @date 2023-04-03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResPartnerAndAct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String name;

    /** 展示名称 */
    @Excel(name = "展示名称")
    private String displayName;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** 父id */
    private Long parentId;

    /** 税务登记证号码 */
    @Excel(name = "税务登记证号码")
    private String vat;

    /** 官网地址 */
    @Excel(name = "官网地址")
    private String website;

    /** 摘要 */
    @Excel(name = "摘要")
    private String comment;

    /** 信用额度 */
    @Excel(name = "信用额度")
    private Long creditLimit;

    /** Active */
    @Excel(name = "Active")
    private String active;

    /** 是否为客户 */
    @Excel(name = "是否为客户")
    private String customer;

    /** 是否为供应商 */
    @Excel(name = "是否为供应商")
    private String supplier;

    /** 为否为雇员 */
    @Excel(name = "为否为雇员")
    private String employee;

    /** Address Type */
    @Excel(name = "Address Type")
    private String type;

    /** 街道 */
    @Excel(name = "街道")
    private String street;

    /** 所属区 */
    @Excel(name = "所属区")
    private String zip;

    /** 城市 */
    @Excel(name = "城市")
    private String city;

    /** State */
    @Excel(name = "State")
    private Long stateId;

    /** 国家id */
    @Excel(name = "国家id")
    private Long countryId;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phone;

    /** 移动电话 */
    @Excel(name = "移动电话")
    private String mobile;

    /** 公司/个人 */
    @Excel(name = "公司/个人")
    private String isCompany;

    /** Share Partner */
    @Excel(name = "Share Partner")
    private String partnerShare;

    /** Commercial Entity */
    @Excel(name = "Commercial Entity")
    private Long commercialPartnerId;

    /** Company Name Entity */
    @Excel(name = "Company Name Entity")
    private String commercialCompanyName;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long writeUid;

    /** 记录日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** 客户编码 */
    @Excel(name = "客户编码")
    private String customerCode;

    /** 定作方地址 */
    @Excel(name = "定作方地址")
    private String deliveryAddress;


}
