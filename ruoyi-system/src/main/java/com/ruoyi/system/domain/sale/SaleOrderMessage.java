package com.ruoyi.system.domain.sale;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购订单操作日志对象 sale_order_message
 * 
 * @author Lin
 * @date 2023-04-06
 */
public class SaleOrderMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售订单id */
    @Excel(name = "销售订单id")
    private Long saleOrderId;

    /** 销售订单信息 */
    @Excel(name = "销售订单信息")
    private String saleOrderMsg;

    /** 处理类型 */
    @Excel(name = "处理类型")
    private String handleType;

    /** 操作人 */
    @Excel(name = "操作人")
    private String handleMan;

    /** 操作数量 */
    @Excel(name = "操作数量")
    private Long handleNum;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSaleOrderId(Long saleOrderId) 
    {
        this.saleOrderId = saleOrderId;
    }

    public Long getSaleOrderId() 
    {
        return saleOrderId;
    }
    public void setSaleOrderMsg(String saleOrderMsg) 
    {
        this.saleOrderMsg = saleOrderMsg;
    }

    public String getSaleOrderMsg() 
    {
        return saleOrderMsg;
    }
    public void setHandleType(String handleType) 
    {
        this.handleType = handleType;
    }

    public String getHandleType() 
    {
        return handleType;
    }
    public void setHandleMan(String handleMan) 
    {
        this.handleMan = handleMan;
    }

    public String getHandleMan() 
    {
        return handleMan;
    }
    public void setHandleNum(Long handleNum) 
    {
        this.handleNum = handleNum;
    }

    public Long getHandleNum() 
    {
        return handleNum;
    }
    public void setRecordTime(Date recordTime) 
    {
        this.recordTime = recordTime;
    }

    public Date getRecordTime() 
    {
        return recordTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("saleOrderId", getSaleOrderId())
            .append("saleOrderMsg", getSaleOrderMsg())
            .append("handleType", getHandleType())
            .append("handleMan", getHandleMan())
            .append("handleNum", getHandleNum())
            .append("recordTime", getRecordTime())
            .toString();
    }
}
