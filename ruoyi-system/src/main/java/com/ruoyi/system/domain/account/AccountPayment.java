package com.ruoyi.system.domain.account;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 付款对象 account_payment
 * 
 * @author Lin
 * @date 2023-05-31
 */
public class AccountPayment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** Company */
    @Excel(name = "Company")
    private Long companyId;

    /** Name */
    @Excel(name = "Name")
    private String name;

    /** Status */
    @Excel(name = "Status")
    private String state;

    /** Payment Type */
    @Excel(name = "Payment Type")
    private String paymentType;

    /** Payment Reference */
    @Excel(name = "Payment Reference")
    private String paymentReference;

    /** Journal Entry Name */
    @Excel(name = "Journal Entry Name")
    private String moveName;

    /** Transfer To */
    @Excel(name = "Transfer To")
    private Long destinationJournalId;

    /** Payment Difference */
    @Excel(name = "Payment Difference")
    private String paymentDifferenceHandling;

    /** Difference Account */
    @Excel(name = "Difference Account")
    private Long writeoffAccountId;

    /** Journal Item Label */
    @Excel(name = "Journal Item Label")
    private String writeoffLabel;

    /** Last Message Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Message Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date messageLastPost;

    /** Payment Method Type */
    @Excel(name = "Payment Method Type")
    private Long paymentMethodId;

    /** Partner Type */
    @Excel(name = "Partner Type")
    private String partnerType;

    /** Partner */
    @Excel(name = "Partner")
    private Long partnerId;

    /** Payment Amount */
    @Excel(name = "Payment Amount")
    private BigDecimal amount;

    /** Currency */
    @Excel(name = "Currency")
    private Long currencyId;

    /** Payment Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Payment Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentDate;

    /** Memo */
    @Excel(name = "Memo")
    private String communication;

    /** Payment Journal */
    @Excel(name = "Payment Journal")
    private Long journalId;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    /** Payment Transaction */
    @Excel(name = "Payment Transaction")
    private Long paymentTransactionId;

    /** Saved payment token */
    @Excel(name = "Saved payment token")
    private Long paymentTokenId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCompanyId(Long companyId) 
    {
        this.companyId = companyId;
    }

    public Long getCompanyId() 
    {
        return companyId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setPaymentType(String paymentType) 
    {
        this.paymentType = paymentType;
    }

    public String getPaymentType() 
    {
        return paymentType;
    }
    public void setPaymentReference(String paymentReference) 
    {
        this.paymentReference = paymentReference;
    }

    public String getPaymentReference() 
    {
        return paymentReference;
    }
    public void setMoveName(String moveName) 
    {
        this.moveName = moveName;
    }

    public String getMoveName() 
    {
        return moveName;
    }
    public void setDestinationJournalId(Long destinationJournalId) 
    {
        this.destinationJournalId = destinationJournalId;
    }

    public Long getDestinationJournalId() 
    {
        return destinationJournalId;
    }
    public void setPaymentDifferenceHandling(String paymentDifferenceHandling) 
    {
        this.paymentDifferenceHandling = paymentDifferenceHandling;
    }

    public String getPaymentDifferenceHandling() 
    {
        return paymentDifferenceHandling;
    }
    public void setWriteoffAccountId(Long writeoffAccountId) 
    {
        this.writeoffAccountId = writeoffAccountId;
    }

    public Long getWriteoffAccountId() 
    {
        return writeoffAccountId;
    }
    public void setWriteoffLabel(String writeoffLabel) 
    {
        this.writeoffLabel = writeoffLabel;
    }

    public String getWriteoffLabel() 
    {
        return writeoffLabel;
    }
    public void setMessageLastPost(Date messageLastPost) 
    {
        this.messageLastPost = messageLastPost;
    }

    public Date getMessageLastPost() 
    {
        return messageLastPost;
    }
    public void setPaymentMethodId(Long paymentMethodId) 
    {
        this.paymentMethodId = paymentMethodId;
    }

    public Long getPaymentMethodId() 
    {
        return paymentMethodId;
    }
    public void setPartnerType(String partnerType) 
    {
        this.partnerType = partnerType;
    }

    public String getPartnerType() 
    {
        return partnerType;
    }
    public void setPartnerId(Long partnerId) 
    {
        this.partnerId = partnerId;
    }

    public Long getPartnerId() 
    {
        return partnerId;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setCurrencyId(Long currencyId) 
    {
        this.currencyId = currencyId;
    }

    public Long getCurrencyId() 
    {
        return currencyId;
    }
    public void setPaymentDate(Date paymentDate) 
    {
        this.paymentDate = paymentDate;
    }

    public Date getPaymentDate() 
    {
        return paymentDate;
    }
    public void setCommunication(String communication) 
    {
        this.communication = communication;
    }

    public String getCommunication() 
    {
        return communication;
    }
    public void setJournalId(Long journalId) 
    {
        this.journalId = journalId;
    }

    public Long getJournalId() 
    {
        return journalId;
    }
    public void setCreateUid(Long createUid) 
    {
        this.createUid = createUid;
    }

    public Long getCreateUid() 
    {
        return createUid;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setWriteUid(Long writeUid) 
    {
        this.writeUid = writeUid;
    }

    public Long getWriteUid() 
    {
        return writeUid;
    }
    public void setWriteDate(Date writeDate) 
    {
        this.writeDate = writeDate;
    }

    public Date getWriteDate() 
    {
        return writeDate;
    }
    public void setPaymentTransactionId(Long paymentTransactionId) 
    {
        this.paymentTransactionId = paymentTransactionId;
    }

    public Long getPaymentTransactionId() 
    {
        return paymentTransactionId;
    }
    public void setPaymentTokenId(Long paymentTokenId) 
    {
        this.paymentTokenId = paymentTokenId;
    }

    public Long getPaymentTokenId() 
    {
        return paymentTokenId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("companyId", getCompanyId())
            .append("name", getName())
            .append("state", getState())
            .append("paymentType", getPaymentType())
            .append("paymentReference", getPaymentReference())
            .append("moveName", getMoveName())
            .append("destinationJournalId", getDestinationJournalId())
            .append("paymentDifferenceHandling", getPaymentDifferenceHandling())
            .append("writeoffAccountId", getWriteoffAccountId())
            .append("writeoffLabel", getWriteoffLabel())
            .append("messageLastPost", getMessageLastPost())
            .append("paymentMethodId", getPaymentMethodId())
            .append("partnerType", getPartnerType())
            .append("partnerId", getPartnerId())
            .append("amount", getAmount())
            .append("currencyId", getCurrencyId())
            .append("paymentDate", getPaymentDate())
            .append("communication", getCommunication())
            .append("journalId", getJournalId())
            .append("createUid", getCreateUid())
            .append("createDate", getCreateDate())
            .append("writeUid", getWriteUid())
            .append("writeDate", getWriteDate())
            .append("paymentTransactionId", getPaymentTransactionId())
            .append("paymentTokenId", getPaymentTokenId())
            .toString();
    }
}
