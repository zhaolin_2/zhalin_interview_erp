package com.ruoyi.system.domain.stock.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Lin
 * @Date 2023 06 05 10
 * 用于发货 或 收货 的数据接收
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockDeliveryOrReceiveVo {

    //操作的订单组件 或者 采购组件 的 id
    private Long id;

    //发货或者收货数量
    private Long num;

}
