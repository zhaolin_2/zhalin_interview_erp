package com.ruoyi.system.domain.sale;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 账户信息对象 res_partner_account
 * 
 * @author lin
 * @date 2023-04-03
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class ResPartnerAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** Sanitized Account Number */
    @Excel(name = "Sanitized Account Number")
    private String accNumber;

    /** Account Holder */
    @Excel(name = "Account Holder")
    private Long partnerId;

    private String bankName;

    /** Bank */
    @Excel(name = "Bank")
    private Long bankId;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

}
