package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 重订货对象 stock_reorder_rule
 * 
 * @author Lin
 * @date 2023-04-17
 */
public class StockReorderRule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** Name */
    @Excel(name = "Name")
    private String name;

    /** Active */
    @Excel(name = "Active")
    private String active;

    /** Warehouse */
    @Excel(name = "Warehouse")
    private Long warehouseId;

    /** Location */
    @Excel(name = "Location")
    private Long locationId;

    /** Product */
    @Excel(name = "Product")
    private Long productId;

    /** Minimum Quantity */
    @Excel(name = "Minimum Quantity")
    private Long productMinQty;

    /** Maximum Quantity */
    @Excel(name = "Maximum Quantity")
    private Long productMaxQty;

    /** Qty Multiple */
    @Excel(name = "Qty Multiple")
    private Long qtyMultiple;

    /** Procurement Group */
    @Excel(name = "Procurement Group")
    private Long groupId;

    /** Company */
    @Excel(name = "Company")
    private Long companyId;

    /** Lead Time */
    @Excel(name = "Lead Time")
    private Long leadDays;

    /** Lead Type */
    @Excel(name = "Lead Type")
    private String leadType;

    /** Created by */
    @Excel(name = "Created by")
    private Long createUid;

    /** Created on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Created on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** Last Updated by */
    @Excel(name = "Last Updated by")
    private Long writeUid;

    /** Last Updated on */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Last Updated on", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setActive(String active) 
    {
        this.active = active;
    }

    public String getActive() 
    {
        return active;
    }
    public void setWarehouseId(Long warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId() 
    {
        return warehouseId;
    }
    public void setLocationId(Long locationId) 
    {
        this.locationId = locationId;
    }

    public Long getLocationId() 
    {
        return locationId;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setProductMinQty(Long productMinQty) 
    {
        this.productMinQty = productMinQty;
    }

    public Long getProductMinQty() 
    {
        return productMinQty;
    }
    public void setProductMaxQty(Long productMaxQty) 
    {
        this.productMaxQty = productMaxQty;
    }

    public Long getProductMaxQty() 
    {
        return productMaxQty;
    }
    public void setQtyMultiple(Long qtyMultiple) 
    {
        this.qtyMultiple = qtyMultiple;
    }

    public Long getQtyMultiple() 
    {
        return qtyMultiple;
    }
    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setCompanyId(Long companyId) 
    {
        this.companyId = companyId;
    }

    public Long getCompanyId() 
    {
        return companyId;
    }
    public void setLeadDays(Long leadDays) 
    {
        this.leadDays = leadDays;
    }

    public Long getLeadDays() 
    {
        return leadDays;
    }
    public void setLeadType(String leadType) 
    {
        this.leadType = leadType;
    }

    public String getLeadType() 
    {
        return leadType;
    }
    public void setCreateUid(Long createUid) 
    {
        this.createUid = createUid;
    }

    public Long getCreateUid() 
    {
        return createUid;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setWriteUid(Long writeUid) 
    {
        this.writeUid = writeUid;
    }

    public Long getWriteUid() 
    {
        return writeUid;
    }
    public void setWriteDate(Date writeDate) 
    {
        this.writeDate = writeDate;
    }

    public Date getWriteDate() 
    {
        return writeDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("active", getActive())
            .append("warehouseId", getWarehouseId())
            .append("locationId", getLocationId())
            .append("productId", getProductId())
            .append("productMinQty", getProductMinQty())
            .append("productMaxQty", getProductMaxQty())
            .append("qtyMultiple", getQtyMultiple())
            .append("groupId", getGroupId())
            .append("companyId", getCompanyId())
            .append("leadDays", getLeadDays())
            .append("leadType", getLeadType())
            .append("createUid", getCreateUid())
            .append("createDate", getCreateDate())
            .append("writeUid", getWriteUid())
            .append("writeDate", getWriteDate())
            .toString();
    }
}
