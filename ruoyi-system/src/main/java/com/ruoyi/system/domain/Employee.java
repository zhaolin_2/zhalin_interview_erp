package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author Lin
 * @Date 2023 06 07 10
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private String name;
    private String sex;
    private String telephone;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private String payment;
}
