package com.ruoyi.system.domain.stock;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存详情对象 stock_message
 * 
 * @author Lin
 * @date 2023-04-03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockTemplateMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 库存id */
    @Excel(name = "库存id")
    private Long stockId;

    /** 处理详情 */
    @Excel(name = "处理详情")
    private String stockMsg;

    /** 处理类型 */
    @Excel(name = "处理类型")
    private String handleType;

    /** 处理人 */
    @Excel(name = "处理人")
    private String handleMan;

    /** 操作数量 */
    @Excel(name = "操作数量")
    private Long handleNum;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

}
